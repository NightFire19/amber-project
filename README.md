https://gitlab.example.com/NightFire19/amber-project/badges/main/release.svg


# Amber Project

Working with Amber Project API endpoints. 


## Project status

### Completed
- Home Screen
- Weapon Screen
- Character Screen
- Complete Search Screen

### In Progress 
- Monster/Enemy Screen

### To Do
- Food, Materials, Furnishing, Namecard, Books, Elements Screens
- Specific Search Screens
- Firebase Implementation for Crashlytics and Advertisments.
- Profile and Crafting Features
- Navigation Overhaul (reducing recomposition)
- Code Cleanup/Refactoring
