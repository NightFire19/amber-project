plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("com.google.dagger.hilt.android")
    id("com.google.gms.google-services")
    id("com.google.firebase.crashlytics")
    kotlin("kapt")
    id("com.google.devtools.ksp") version "1.8.10-1.0.9"
}

android {
    compileSdk = 33

    defaultConfig {
        applicationId = "com.xuan.projectamber"
        minSdk = 26
        targetSdk = 33
        versionCode = 6
        versionName = "0.3"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    composeOptions {
        kotlinCompilerExtensionVersion = "1.4.4"
    }

    compileOptions {
        sourceCompatibility  = JavaVersion.VERSION_17

        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = "17"
    }


    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    buildFeatures {
        compose = true
    }
    namespace = "com.xuan.projectamber"
    applicationVariants.all {
        kotlin.sourceSets {
            getByName(name) {
                kotlin.srcDir("build/generated/ksp/$name/kotlin")
            }
        }
    }

    dependencies {

        implementation("androidx.core:core-ktx:1.10.0")
        implementation("androidx.appcompat:appcompat:1.6.1")
        implementation("com.google.android.material:material:1.9.0")
        implementation("androidx.constraintlayout:constraintlayout:2.1.4")
        testImplementation("junit:junit:4.13.2")
        androidTestImplementation("androidx.test.ext:junit:1.1.5")
        androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")

        // co-routines
        implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.6.4")
        implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.6.1")
        implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.6.1")


        // COMPOSE!!!
        implementation("androidx.compose.ui:ui:1.4.3")
        implementation("androidx.compose.ui:ui-tooling:1.4.3")
        implementation("androidx.compose.foundation:foundation:1.4.3")
        implementation("androidx.compose.material:material-icons-core:1.4.3")
        implementation("androidx.compose.material:material-icons-extended:1.4.3")
        implementation("androidx.compose.material3:material3:1.1.0-rc01")
        implementation("androidx.activity:activity-compose:1.7.1")
        androidTestImplementation("androidx.compose.ui:ui-test-junit4:1.4.3")
        implementation("androidx.hilt:hilt-navigation-compose:1.0.0")
        implementation("androidx.paging:paging-compose:1.0.0-alpha19")
        implementation("androidx.compose.runtime:runtime-livedata:1.5.0-alpha03")

        //coil
        implementation("io.coil-kt:coil-compose:2.1.0")

        // Dagger - Hilt
        implementation("com.google.dagger:hilt-android:2.45")
        kapt("com.google.dagger:hilt-android-compiler:2.45")
        implementation("androidx.hilt:hilt-navigation-compose:1.0.0")
        implementation("androidx.navigation:navigation-compose:2.5.3")


        // Retrofit
        implementation ("com.squareup.retrofit2:retrofit:2.9.0")
        implementation ("com.squareup.retrofit2:converter-gson:2.9.0")

        // Room Components
        implementation ("androidx.datastore:datastore-preferences:1.0.0")
        implementation ("androidx.room:room-ktx:2.5.1")
        implementation ("androidx.room:room-runtime:2.5.1")
        kapt ("androidx.room:room-compiler:2.5.1")
        androidTestImplementation ("androidx.room:room-testing:2.5.1")

        implementation ("com.google.android.gms:play-services-ads:22.0.0")

        //Firebase BoM
        implementation(platform("com.google.firebase:firebase-bom:31.4.0"))

        //Firebase analytics
        implementation ("com.google.firebase:firebase-analytics-ktx")

        //Firebase crashlytics
        implementation ("com.google.firebase:firebase-analytics-ktx")

        implementation("de.charlex.compose:html-text:1.3.1")
    }
}
