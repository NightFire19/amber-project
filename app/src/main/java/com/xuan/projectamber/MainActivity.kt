package com.xuan.projectamber

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.xuan.projectamber.ui.theme.ProjectAmberTheme
import com.xuan.projectamber.ui.AppScaffold
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ProjectAmberTheme {
                AppScaffold()
            }
        }
    }
}