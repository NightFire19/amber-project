package com.xuan.projectamber

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ProjectAmber : Application() {
}