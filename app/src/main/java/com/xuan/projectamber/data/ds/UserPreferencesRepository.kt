package com.xuan.projectamber.data.ds

import android.content.Context
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.core.content.edit

private const val USER_PREFERENCES_NAME = "user_preferences"
private const val LANGUAGE_KEY = "language"

class UserPreferencesRepository constructor(context: Context) {

    private val sharedPreferences =
        context.applicationContext.getSharedPreferences(USER_PREFERENCES_NAME, Context.MODE_PRIVATE)

    var languageFlow : String by mutableStateOf(language)
        private set

    private val language: String
        get() {
            val language = sharedPreferences.getString(LANGUAGE_KEY, "English")
            return language ?: "English"
        }

    fun setLanguage(lang: String) {
        sharedPreferences.edit {
            putString(LANGUAGE_KEY, lang)
        }
        languageFlow = lang
    }
}