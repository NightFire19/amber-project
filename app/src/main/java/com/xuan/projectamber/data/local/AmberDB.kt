package com.xuan.projectamber.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.xuan.projectamber.domain.Converters
import com.xuan.projectamber.domain.models.*
import com.xuan.projectamber.domain.models.FoodEntity
import com.xuan.projectamber.domain.models.FurnitureEntity

@Database(
    entities =
    [
        EventEntity::class,
        DailyDungeonEntity::class,
        UpgradeEntity::class,
        CharacterEntity::class,
        CharacterStoryEntity::class,
        WeaponEntity::class,
        WeaponStoryEntity::class,
        SearchResultEntity::class,
        MonsterEntity::class,
        MaterialEntity::class,
        CategorySearchResultEntity::class,
        FoodEntity::class,
        FurnitureEntity::class,
        NamecardEntity::class,
        ReliquaryEntity::class,
        BookEntity::class,
        ReadableEntity::class,
        QuestEntity::class,
        QuestDetailEntity::class,
    TCGSummaryEntity::class,
    TCGEntity::class,
    ],
    exportSchema = false,
    version = 46
)

@TypeConverters(Converters::class)
abstract class AmberDB : RoomDatabase() {
    abstract fun getDao(): AmberDao
}