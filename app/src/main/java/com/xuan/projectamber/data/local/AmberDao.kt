package com.xuan.projectamber.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.OnConflictStrategy
import com.xuan.projectamber.domain.models.*
import com.xuan.projectamber.domain.models.FoodEntity
import com.xuan.projectamber.domain.models.FurnitureEntity
import kotlinx.coroutines.flow.Flow


@Dao
interface AmberDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertEvents(event: List<EventEntity>)

    @Query("select * from dailyDungeon_db")
    fun getDailyDungeons(): Flow<DailyDungeonEntity>

    @Query("DELETE FROM event_db")
    fun deleteEvents()

    @Query("select * from event_db where description like '%' || :search  || '%' ")
    fun getLikeEvents(search: String?): Flow<List<EventEntity>>

    @Query("select * from event_db where nameFull not like '%' || :search  || '%' ")
    fun getNotLikeEvents(search: String?): Flow<List<EventEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDailyDungeons(dailyDungeonEntity: DailyDungeonEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUpgrades(upgradeEntity: UpgradeEntity)

    @Query("select * from upgrade_db")
    fun getUpgrades(): Flow<UpgradeEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCharacter(characterEntity: CharacterEntity)

    @Query("select * from character_db where id=:id")
    fun getCharacter(id: String): Flow<CharacterEntity>

    @Query("SELECT EXISTS(SELECT * FROM character_db WHERE id = :id)")
    fun isRowIsExist(id : String) : Boolean

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCharacterStory(characterStoryEntity: CharacterStoryEntity)

    @Query("select * from story_db where id=:id")
    fun getCharacterStory(id: String): Flow<CharacterStoryEntity>

    @Query("SELECT EXISTS(SELECT * FROM story_db WHERE id = :id)")
    fun isStoryRowIsExist(id : String) : Boolean

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertWeapon(weaponEntity: WeaponEntity)

    @Query("SELECT EXISTS(SELECT * FROM weapon_db WHERE id = :id)")
    fun isWeaponRowIsExist(id : String) : Boolean

    @Query("select * from weapon_db where id=:id")
    fun getWeapon(id: String) : Flow<WeaponEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertWeaponStory(weaponStoryEntity: WeaponStoryEntity)

    @Query("SELECT EXISTS(SELECT * FROM weaponStory_db WHERE id = :id)")
    fun isWeaponStoryRowIsExist(id : String) : Boolean

    @Query("select * from weaponStory_db where id=:id")
    fun getWeaponStory(id: String) : Flow<WeaponStoryEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSearchResult(searchResultEntity: SearchResultEntity)
    @Query("SELECT COUNT(id) FROM search_db")
    fun getSearchResultCount() : Int

    @Query("SELECT * FROM search_db WHERE name LIKE '%' || :query || '%' LIMIT 10")
    fun getSearchResults(query: String) : Flow<List<SearchResultEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMonster(monsterEntity: MonsterEntity)

    @Query("SELECT EXISTS(SELECT * FROM monster_db WHERE id = :id)")
    fun doesMonsterRowExist(id: String) : Boolean

    @Query("SELECT * FROM monster_db WHERE id = :id")
    fun getMonster(id: String) : Flow<MonsterEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMaterial(materialEntity: MaterialEntity)

    @Query("SELECT EXISTS(SELECT * FROM material_db WHERE id = :id)")
    fun doesMaterialRowExist(id: String) : Boolean

    @Query("SELECT * FROM material_db WHERE id = :id")
    fun getMaterial(id: String) : Flow<MaterialEntity>

    @Query("SELECT COUNT(id) FROM categoryResult_db WHERE category = :category")
    fun getCategorySearchCount(category: String) : Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCategorySummary(categorySearchResultEntity: CategorySearchResultEntity)

    @Query("SELECT * FROM categoryResult_db WHERE name LIKE '%' || :query || '%' AND category = :category LIMIT :limit")
    fun getCategorySearchResults(query: String, category: String, limit: Int) : Flow<List<CategorySearchResultEntity>>

    @Query("SELECT * FROM categoryResult_db WHERE id IN (:ids) AND category = :category")
    fun getCategorySearchResults(ids: List<String>, category: String) : Flow<List<CategorySearchResultEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFood(foodEntity: FoodEntity)

    @Query("SELECT EXISTS(SELECT * FROM food_db WHERE id = :id)")
    fun doesFoodRowExist(id: String) : Boolean

    @Query("SELECT * FROM food_db WHERE id = :id")
    fun getFood(id: String) : Flow<FoodEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFurniture(furnitureEntity: FurnitureEntity)

    @Query("SELECT EXISTS(SELECT * FROM furniture_db WHERE id = :id)")
    fun doesFurnitureRowExist(id: String) : Boolean

    @Query("SELECT * FROM furniture_db WHERE id = :id")
    fun getFurniture(id: String) : Flow<FurnitureEntity>
    
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNamecard(namecardEntity: NamecardEntity)

    @Query("SELECT EXISTS(SELECT * FROM namecard_db WHERE id = :id)")
    fun doesNamecardRowExist(id: String) : Boolean

    @Query("SELECT * FROM namecard_db WHERE id = :id")
    fun getNamecard(id: String) : Flow<NamecardEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertReliquary(reliquaryEntity: ReliquaryEntity)

    @Query("SELECT EXISTS(SELECT * FROM reliquary_db WHERE id = :id)")
    fun doesReliquaryRowExist(id: String) : Boolean

    @Query("SELECT * FROM reliquary_db WHERE id = :id")
    fun getReliquary(id: String) : Flow<ReliquaryEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBook(bookEntity: BookEntity)

    @Query("SELECT EXISTS(SELECT * FROM book_db WHERE id = :id)")
    fun doesBookRowExist(id: String) : Boolean

    @Query("SELECT * FROM book_db WHERE id = :id")
    fun getBook(id: String) : Flow<BookEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertReadable(readableEntity: ReadableEntity)

    @Query("SELECT EXISTS(SELECT * FROM readable_db WHERE id = :id)")
    fun doesReadableRowExist(id: String) : Boolean

    @Query("SELECT * FROM readable_db WHERE id IN (:ids)")
    fun getReadable(ids: List<String>) : Flow<List<ReadableEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertQuests(questEntities: List<QuestEntity>)

    @Query("SELECT COUNT(id) FROM quest_db")
    fun getQuestCount() : Int

    @Query("SELECT * FROM quest_db WHERE chapterTitle LIKE '%' || :query || '%' AND type = :type")
    fun getQuests(query: String, type: String) : Flow<List<QuestEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertQuest(questDetailEntity: QuestDetailEntity)

    @Query("SELECT EXISTS(SELECT * FROM questDetails_db WHERE id = :id)")
    fun doesQuestDetailRowExist(id: String): Boolean

    @Query("SELECT * FROM questDetails_db WHERE id = :id")
    fun getQuestDetail(id: String) : Flow<QuestDetailEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTCGSummaries(tcgSummaries: List<TCGSummaryEntity>)

    @Query("SELECT COUNT(id) FROM tcgSummary_db")
    fun getTCGListCount() : Int

    @Query("SELECT * FROM tcgSummary_db WHERE name LIKE '%' || :query || '%'")
    fun getTCGList(query: String): Flow<List<TCGSummaryEntity>>

    @Query("SELECT * FROM tcgSummary_db WHERE id IN (:ids)")
    fun getTCGList(ids: List<String>): Flow<List<TCGSummaryEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTCG(tcgEntity: TCGEntity)
    @Query("SELECT EXISTS(SELECT * FROM tcg_db WHERE id = :id)")
    fun doesTCGDetailRowExist(id: String): Boolean

    @Query("SELECT * FROM tcg_db WHERE id = :id")
    fun getTCGDetail(id: String) : Flow<TCGEntity>

}