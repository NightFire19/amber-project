package com.xuan.projectamber.data.remote

import com.xuan.projectamber.domain.models.*
import com.xuan.projectamber.domain.models.BookListResponseEntity
import com.xuan.projectamber.domain.models.FoodResponseEntity
import com.xuan.projectamber.domain.models.FurnitureResponseEntity
import com.xuan.projectamber.domain.models.NamecardListResponseEntity
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {
    @GET("assets/data/event.json")
    suspend fun getAllEvents(): Response<Map<String, EventEntity>>

    @GET("v2/{lang}/dailyDungeon")
    suspend fun getDungeons(@Path("lang") lang : String, ): Response<DailyDungeonEntity>

    @GET("v2/static/upgrade")
    suspend fun getUpgrades(): Response<UpgradeEntity>

    @GET("v2/{lang}/avatar/{id}")
    suspend fun getCharacter(@Path("lang") lang : String, @Path("id") id : String): Response<CharacterResponseEntity>

    @GET("v2/{lang}/avatarFetter/{id}")
    suspend fun getCharacterStory(@Path("lang") lang : String, @Path("id") id : String): Response<CharacterStoryResponseEntity>

    @GET("v2/{lang}/weapon/{id}")
    suspend fun getWeapon(@Path("lang") lang : String, @Path("id") id : String): Response<WeaponResponseEntity>

    @GET("v2/{lang}/readable/Weapon{id}")
    suspend fun getWeaponStory(@Path("lang") lang : String, @Path("id") id : String): Response<WeaponStoryResponseEntity>

    @GET("v2/{lang}/everything")
    suspend fun getSearchResults(@Path("lang") lang : String,): Response<SearchResultResponseEntity>

    @GET("v2/{lang}/monster/{id}")
    suspend fun getMonster(@Path("lang") lang : String, @Path("id") id : String): Response<MonsterResponseEntity>

    @GET("v2/{lang}/material/{id}")
    suspend fun getMaterial(@Path("lang") lang : String, @Path("id") id : String): Response<MaterialResponseEntity>

    @GET("v2/{lang}/avatar")
    suspend fun getCharacters(@Path("lang") lang : String, ): Response<CharacterListResponseEntity>

    @GET("v2/{lang}/weapon")
    suspend fun getWeapons(@Path("lang") lang : String, ): Response<WeaponListResponseEntity>

    @GET("v2/{lang}/monster")
    suspend fun getMonsters(@Path("lang") lang : String, ): Response<MonsterListResponseEntity>

    @GET("v2/{lang}/food")
    suspend fun getFoods(@Path("lang") lang : String, ): Response<FoodListResponseEntity>

    @GET("v2/{lang}/material")
    suspend fun getMaterials(@Path("lang") lang : String, ): Response<MaterialListResponseEntity>

    @GET("v2/{lang}/furniture")
    suspend fun getFurniture(@Path("lang") lang : String, ): Response<FurnitureListResponseEntity>

    @GET("v2/{lang}/reliquary")
    suspend fun getArtifacts(@Path("lang") lang : String, ): Response<ArtifactListResponseEntity>

    @GET("v2/{lang}/namecard")
    suspend fun getNamecards(@Path("lang") lang : String, ): Response<NamecardListResponseEntity>

    @GET("v2/{lang}/book")
    suspend fun getBooks(@Path("lang") lang : String, ): Response<BookListResponseEntity>

    @GET("v2/{lang}/food/{id}")
    suspend fun getFood(@Path("lang") lang : String, @Path("id") id : String): Response<FoodResponseEntity>

    @GET("v2/{lang}/furniture/{id}")
    suspend fun getFurniture(@Path("lang") lang : String, @Path("id") id : String): Response<FurnitureResponseEntity>

    @GET("v2/{lang}/namecard/{id}")
    suspend fun getNamecard(@Path("lang") lang : String, @Path("id") id : String): Response<NamecardResponseEntity>

    @GET("v2/{lang}/reliquary/{id}")
    suspend fun getArtifact(@Path("lang") lang : String, @Path("id") id : String): Response<ReliquaryResponseEntity>

    @GET("v2/{lang}/book/{id}")
    suspend fun getBook(@Path("lang") lang : String, @Path("id") id : String): Response<BookResponseEntity>

    @GET("v2/{lang}/readable/Book{id}")
    suspend fun getReadable(@Path("lang") lang : String, @Path("id") id : String): Response<ReadableResponseEntity>

   @GET( "v2/static/changelog")
   suspend fun getChangelog(): Response<ChangelogResponseEntity>

    @GET("v2/{lang}/quest")
    suspend fun getQuests(@Path("lang") lang : String, ): Response<QuestListResponseEntity>

    @GET("v2/{lang}/quest/{id}")
    suspend fun getQuest(@Path("lang") lang : String, @Path("id") id : String): Response<QuestResponseEntity>

    @GET("v2/{lang}/elements")
    suspend fun getElements(@Path("lang") lang : String, ): Response<ElementResponseEntity>

    @GET("v2/{lang}/gcg")
    suspend fun getTCGList(@Path("lang") lang : String, ): Response<TCGListResponseEntity>

    @GET("v2/{lang}/gcg/{id}")
    suspend fun getTCG(@Path("lang") lang : String, @Path("id") id : String): Response<TCGResponseEntity>
}