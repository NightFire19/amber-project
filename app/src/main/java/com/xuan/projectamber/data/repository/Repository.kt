package com.xuan.projectamber.data.repository

import com.xuan.projectamber.domain.models.*
import com.xuan.projectamber.domain.models.FoodEntity
import com.xuan.projectamber.domain.models.FurnitureEntity
import kotlinx.coroutines.flow.Flow
import retrofit2.Response

interface Repository {
    suspend fun insertEvents(events : List<EventEntity>)
    suspend fun getLikeEvents(search : String) : Flow<List<EventEntity>>
    suspend fun getNotLikeEvents(search : String) : Flow<List<EventEntity>>
    suspend fun fetchEvents()

    suspend fun fetchDailyDungeons()
    suspend fun insertDailyDungeons(dailyDungeonEntity: DailyDungeonEntity)
    suspend fun getDailyDungeons() : Flow<DailyDungeonEntity>

    suspend fun fetchUpgrades()
    suspend fun getUpgrades(): Flow<UpgradeEntity>
    suspend fun fetchCharacter(id: String)
    suspend fun getCharacter(id: String): Flow<CharacterEntity>
    suspend fun fetchCharacterStory(id: String)
    suspend fun getCharacterStory(id: String): Flow<CharacterStoryEntity>
    suspend fun fetchWeapon(id: String)
    suspend fun getWeapon(id: String): Flow<WeaponEntity>
    suspend fun fetchWeaponStory(id: String)
    suspend fun getWeaponStory(id: String): Flow<WeaponStoryEntity>
    suspend fun fetchSearchResults()
    suspend fun getSearchResults(query: String): Flow<List<SearchResultEntity>>
    suspend fun fetchMonster(id: String)
    suspend fun getMonster(id: String): Flow<MonsterEntity>
    suspend fun fetchMaterial(id: String)
    suspend fun getMaterial(id: String): Flow<MaterialEntity>
    suspend fun fetchResultList(category: String)
    suspend fun fetchFood(id: String)
    suspend fun getFood(id: String): Flow<FoodEntity>
    suspend fun fetchFurniture(id: String)
    suspend fun getFurniture(id: String): Flow<FurnitureEntity>
    suspend fun getNamecard(id: String): Flow<NamecardEntity>
    suspend fun fetchNamecard(id: String)
    suspend fun fetchArtifact(id: String)
    suspend fun getArtifact(id: String): Flow<ReliquaryEntity>
    suspend fun fetchBook(id: String)
    suspend fun getBook(id: String): Flow<BookEntity>
    suspend fun fetchReadable(id: String)
    suspend fun getReadables(ids: List<String>): Flow<List<ReadableEntity>>
    suspend fun getChangelog(): Map<String, ChangelogEntity>
    suspend fun getResultList(
        ids: List<String>,
        category: String
    ): Flow<List<CategorySearchResultEntity>>

    suspend fun getQuestsByType(query: String, type: String): Flow<List<QuestEntity>>
    suspend fun fetchQuest(id: String)
    suspend fun getQuest(id: String): Flow<QuestDetailEntity>
    suspend fun fetchQuests()
    suspend fun clearAllTables()
    suspend fun getElements(): Response<ElementResponseEntity>?
    suspend fun fetchTCG(id: String)
    suspend fun getTCG(id: String): Flow<TCGEntity>

    suspend fun getTCGList(query: String): Flow<List<TCGSummaryEntity>>
    suspend fun getTCGList(ids: List<String>): Flow<List<TCGSummaryEntity>>
    suspend fun getResultList(
        query: String,
        category: String,
        limit: Int
    ): Flow<List<CategorySearchResultEntity>>
}