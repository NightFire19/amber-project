package com.xuan.projectamber.di

import android.content.Context
import androidx.room.Room
import com.google.gson.Gson
import com.xuan.projectamber.data.ds.UserPreferencesRepository
import com.xuan.projectamber.data.local.AmberDB
import com.xuan.projectamber.data.local.AmberDao
import com.xuan.projectamber.data.remote.ApiService
import com.xuan.projectamber.data.repository.Repository
import com.xuan.projectamber.domain.Converters
import com.xuan.projectamber.domain.repository.RepositoryImpl
import com.xuan.projectamber.util.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    @Provides
    @Singleton
    fun provideApiService(): ApiService {
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiService::class.java)
    }

    @Provides
    @Singleton
    fun provideAmberDao(amberDB: AmberDB): AmberDao {
        return amberDB.getDao()
    }

    @Provides
    @Singleton
    fun provideMyDatabase(@ApplicationContext context: Context): AmberDB {
        return Room.databaseBuilder(context, AmberDB::class.java, "amber_db")
                .addTypeConverter(Converters(Gson()))
                .fallbackToDestructiveMigration()
                .build()
    }

    @Provides
    @Singleton
    fun provideRepository(amberDB: AmberDB, apiService: ApiService, amberDao: AmberDao, userPreferencesRepository: UserPreferencesRepository) : Repository {
        return RepositoryImpl(amberDB, apiService, amberDao, userPreferencesRepository)
    }

    @Provides
    @Singleton
    fun provideUserPreferences(@ApplicationContext context: Context) : UserPreferencesRepository {
        return UserPreferencesRepository(context)
    }
}