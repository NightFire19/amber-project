package com.xuan.projectamber.domain

import androidx.room.ProvidedTypeConverter
import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.xuan.projectamber.domain.models.*
import com.xuan.projectamber.domain.models.FurnitureRecipeEntity
import com.xuan.projectamber.domain.models.RecipeEntity
import java.lang.reflect.Type


@ProvidedTypeConverter
class Converters (
    private val gson: Gson
){
    @TypeConverter
    fun toNameJson(name: Name): String {
        return gson.toJson(name)
    }

    @TypeConverter
    fun fromNameJson(jsonString: String): Name {
        return gson.fromJson(jsonString, Name::class.java)
    }

    @TypeConverter
    fun toNameFullJson(nameFull: NameFull): String {
        return gson.toJson(nameFull)
    }

    @TypeConverter
    fun fromNameFullJson(jsonString: String): NameFull {
        return gson.fromJson(jsonString, NameFull::class.java)
    }

    @TypeConverter
    fun toDescriptionJson(description: Description): String {
        return gson.toJson(description)
    }

    @TypeConverter
    fun fromDescriptionJson(jsonString: String): Description {
        return gson.fromJson(jsonString, Description::class.java)
    }

    @TypeConverter
    fun toBannerJson(banner: Banner): String {
        return gson.toJson(banner)
    }

    @TypeConverter
    fun fromBannerJson(jsonString: String): Banner {
        return gson.fromJson(jsonString, Banner::class.java)
    }

    @TypeConverter
    fun toDaysOfWeekJson(daysOfWeek: DaysOfWeek): String {
        return gson.toJson(daysOfWeek)
    }

    @TypeConverter
    fun fromDaysOfWeekJson(jsonString: String): DaysOfWeek {
        return gson.fromJson(jsonString, DaysOfWeek::class.java)
    }

    @TypeConverter
    fun toDataUpgradeJson(dataUpgrade: DataUpgrade): String {
        return gson.toJson(dataUpgrade)
    }

    @TypeConverter
    fun fromDataUpgradeJson(jsonString: String): DataUpgrade {
        return gson.fromJson(jsonString, DataUpgrade::class.java)
    }

    @TypeConverter
    fun toBirthdayJson(birthday: List<Int>): String {
        return gson.toJson(birthday)
    }

    @TypeConverter
    fun fromBirthdayJson(jsonString: String): List<Int> {
        val listType: Type = object : TypeToken<List<Int?>?>() {}.type
        return gson.fromJson(jsonString,listType)
    }

    @TypeConverter
    fun toFetterJson(fetterEntity: FetterEntity): String {
        return gson.toJson(fetterEntity)
    }

    @TypeConverter
    fun fromFetterJson(jsonString: String): FetterEntity {
        return gson.fromJson(jsonString, FetterEntity::class.java)
    }

    @TypeConverter
    fun toCharacterUpgradeJson(CharacterUpgradeEntity: CharacterUpgradeEntity): String {
        return gson.toJson(CharacterUpgradeEntity)
    }

    @TypeConverter
    fun fromCharacterUpgradeJson(jsonString: String): CharacterUpgradeEntity {
        return gson.fromJson(jsonString, CharacterUpgradeEntity::class.java)
    }

    @TypeConverter
    fun fromConstellationMap(map: Map<String?, ConstellationEntity?>?): String? {
        val gson = Gson()
        return gson.toJson(map)
    }

    @TypeConverter
    fun fromConstellationString(value: String?): Map<String?, ConstellationEntity?>? {
        val mapType = object : TypeToken<Map<String?, ConstellationEntity?>?>() {}.type
        return Gson().fromJson(value, mapType)
    }

    @TypeConverter
    fun fromTalentMap(map: Map<String?, TalentEntity?>?): String? {
        val gson = Gson()
        return gson.toJson(map)
    }

    @TypeConverter
    fun fromTalentString(value: String?): Map<String?, TalentEntity?>? {
        val mapType = object : TypeToken<Map<String?, TalentEntity?>?>() {}.type
        return Gson().fromJson(value, mapType)
    }

    @TypeConverter
    fun toOtherJson(otherEntity: OtherEntity?): String {
        return if (otherEntity == null) {
            ""
        } else {
            gson.toJson(otherEntity)
        }
    }

    @TypeConverter
    fun fromOtherJson(jsonString: String): OtherEntity? {
        return if (jsonString == "") {
            null
        } else {
            gson.fromJson(jsonString, OtherEntity::class.java)
        }
    }
    
    @TypeConverter
    fun fromStoryMap(map: Map<String?, StoryEntity?>?): String? {
        val gson = Gson()
        return gson.toJson(map)
    }

    @TypeConverter
    fun fromStoryString(value: String?): Map<String?, StoryEntity?>? {
        val mapType = object : TypeToken<Map<String?, StoryEntity?>?>() {}.type
        return Gson().fromJson(value, mapType)
    }

    @TypeConverter
    fun fromQuoteMap(map: Map<String?, QuoteEntity?>?): String? {
        val gson = Gson()
        return gson.toJson(map)
    }

    @TypeConverter
    fun fromQuoteString(value: String?): Map<String?, QuoteEntity?>? {
        val mapType = object : TypeToken<Map<String?, QuoteEntity?>?>() {}.type
        return Gson().fromJson(value, mapType)
    }
    
    @TypeConverter
    fun fromAffixMap(map: Map<String?, AffixEntity?>?): String? {
        val gson = Gson()
        return gson.toJson(map)
    }

    @TypeConverter
    fun fromAffixString(value: String?): Map<String?, AffixEntity?>? {
        val mapType = object : TypeToken<Map<String?, AffixEntity?>?>() {}.type
        return Gson().fromJson(value, mapType)
    }
    
    @TypeConverter
    fun toWeaponUpgradeJson(WeaponUpgradeEntity: WeaponUpgradeEntity?): String {
        return if (WeaponUpgradeEntity == null) {
            ""
        } else {
            gson.toJson(WeaponUpgradeEntity)
        }
    }

    @TypeConverter
    fun fromWeaponUpgradeJson(jsonString: String): WeaponUpgradeEntity? {
        return if (jsonString == "") {
            null
        } else {
            gson.fromJson(jsonString, WeaponUpgradeEntity::class.java)
        }
    }

    @TypeConverter
    fun fromAscensionMap(map: Map<String?, Int?>?): String? {
        val gson = Gson()
        return gson.toJson(map)
    }

    @TypeConverter
    fun fromAscensionString(value: String?): Map<String?, Int?>? {
        val mapType = object : TypeToken<Map<String?, Int?>?>() {}.type
        return Gson().fromJson(value, mapType)
    }

    @TypeConverter
    fun fromEntriesMap(map: Map<String?, EntryEntity?>): String {
        val gson = Gson()
        return gson.toJson(map)
    }

    @TypeConverter
    fun fromEntriesString(value: String?): Map<String?, EntryEntity?> {
        val mapType = object : TypeToken<Map<String?, EntryEntity?>?>() {}.type
        return Gson().fromJson(value, mapType)
    }

    @TypeConverter
    fun fromTipMap(map: Map<String?, TipEntity?>?): String {
        val gson = Gson()
        return gson.toJson(map)
    }

    @TypeConverter
    fun fromTipString(value: String?): Map<String?, TipEntity?>? {
        val mapType = object : TypeToken<Map<String?, TipEntity?>?>() {}.type
        return Gson().fromJson(value, mapType)
    }

    @TypeConverter
    fun toSourceJson(source: List<SourceEntity>): String {
        return gson.toJson(source)
    }

    @TypeConverter
    fun fromSourceJson(jsonString: String): List<SourceEntity> {
        val listType: Type = object : TypeToken<List<SourceEntity?>?>() {}.type
        return gson.fromJson(jsonString,listType)
    }

    @TypeConverter
    fun toRecipeJson(RecipeEntity: RecipeEntity?): String {
        return if (RecipeEntity == null) {
            ""
        } else {
            gson.toJson(RecipeEntity)
        }
    }

    @TypeConverter
    fun fromRecipeJson(jsonString: String): RecipeEntity? {
        return if (jsonString == "") {
            null
        } else {
            gson.fromJson(jsonString, RecipeEntity::class.java)
        }
    }

    @TypeConverter
    fun toStringListJson(source: List<String>): String {
        return gson.toJson(source)
    }

    @TypeConverter
    fun fromStringListJson(jsonString:String): List<String> {
        val listType: Type = object : TypeToken<List<String?>?>() {}.type
        return gson.fromJson(jsonString,listType)
    }

    @TypeConverter
    fun toFurnitureRecipeJson(source: FurnitureRecipeEntity?): String {
        return if (source == null) {
            ""
        } else {
            gson.toJson(source)
        }
    }

    @TypeConverter
    fun fromFurnitureRecipeJson(jsonString: String): FurnitureRecipeEntity? {
        return gson.fromJson(jsonString, FurnitureRecipeEntity::class.java)
    }

    @TypeConverter
    fun tostringStringMapJson(map: Map<String?, String?>?): String? {
        val gson = Gson()
        return gson.toJson(map)
    }

    @TypeConverter
    fun fromStringStringMapJson(value: String?): Map<String?, String?>? {
        val mapType = object : TypeToken<Map<String?, String?>?>() {}.type
        return Gson().fromJson(value, mapType)
    }

    @TypeConverter
    fun fromSuitMap(map: Map<String?, SuitEntity?>?): String? {
        val gson = Gson()
        return gson.toJson(map)
    }

    @TypeConverter
    fun fromSuitString(value: String?): Map<String?, SuitEntity?>? {
        val mapType = object : TypeToken<Map<String?, SuitEntity?>?>() {}.type
        return Gson().fromJson(value, mapType)
    }

    @TypeConverter
    fun toVolumeJson(source: List<VolumeEntity>): String {
        return gson.toJson(source)
    }

    @TypeConverter
    fun fromVolumeJson(jsonString: String): List<VolumeEntity> {
        val listType: Type = object : TypeToken<List<VolumeEntity?>?>() {}.type
        return gson.fromJson(jsonString,listType)
    }

    @TypeConverter
    fun toQuestInfoJson(QuestInfoEntity: QuestInfoEntity?): String {
        return if (QuestInfoEntity == null) {
            ""
        } else {
            gson.toJson(QuestInfoEntity)
        }
    }

    @TypeConverter
    fun fromQuestInfoJson(jsonString: String): QuestInfoEntity? {
        return if (jsonString == "") {
            null
        } else {
            gson.fromJson(jsonString, QuestInfoEntity::class.java)
        }
    }

    @TypeConverter
    fun fromStoryListMap(map: Map<String?, StoryListEntity?>?): String? {
        val gson = Gson()
        return gson.toJson(map)
    }

    @TypeConverter
    fun fromStoryListString(value: String?): Map<String?, StoryListEntity?>? {
        val mapType = object : TypeToken<Map<String?, StoryListEntity?>?>() {}.type
        return Gson().fromJson(value, mapType)
    }

    

    @TypeConverter
    fun fromDictionaryMap(map: Map<String?, DictionaryEntity?>?): String? {
        val gson = Gson()
        return gson.toJson(map)
    }

    @TypeConverter
    fun fromDictionaryString(value: String?): Map<String?, DictionaryEntity?>? {
        val mapType = object : TypeToken<Map<String?, DictionaryEntity?>?>() {}.type
        return Gson().fromJson(value, mapType)
    }

    @TypeConverter
    fun fromTCGTalentMap(map: Map<String?, TCGTalentEntity?>?): String? {
        val gson = Gson()
        return gson.toJson(map)
    }

    @TypeConverter
    fun fromTCGTalentString(value: String?): Map<String?, TCGTalentEntity?>? {
        val mapType = object : TypeToken<Map<String?, TCGTalentEntity?>?>() {}.type
        return Gson().fromJson(value, mapType)
    }

    @TypeConverter
    fun toTCGRelatedJson(source: List<TCGRelatedEntity>?): String {
        if (source == null) {
            return ""
        }
        return gson.toJson(source)
    }

    @TypeConverter
    fun fromTCGRelatedJson(jsonString: String): List<TCGRelatedEntity>? {
        if (jsonString == "") {
            return null
        }
        val listType: Type = object : TypeToken<List<TCGRelatedEntity?>?>() {}.type
        return gson.fromJson(jsonString,listType)
    }

    //Do not put new type converters below this.
    @TypeConverter
    fun fromRecipeMap(recipe: Any) : String {
        val gson = Gson()
        return gson.toJson(recipe)
    }

    @TypeConverter
    fun fromRecipeString(jsonString: String?): Any {
        return gson.fromJson(jsonString, Any::class.java)
    }
}



