package com.xuan.projectamber.domain.models

import com.google.gson.annotations.SerializedName

data class ArtifactListResponseEntity(
    @SerializedName("data")
    val data: ArtifactListEntity
)

data class ArtifactListEntity(
    @SerializedName("items")
    val items: Map<String, ArtifactSummaryEntity>
)

data class ArtifactSummaryEntity(
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name:String,
    @SerializedName("levelList")
    val levelList: List<Int>,
    @SerializedName("affixList")
    val affixList: Map<Int,String>,
    @SerializedName("icon")
    val icon: String,
    @SerializedName("route")
    val route: String,
    @SerializedName("sortOrder")
    val sortOrder: Int,
)