package com.xuan.projectamber.domain.models

import com.google.gson.annotations.SerializedName

data class BookListResponseEntity(
    @SerializedName("data")
    val data: BookListEntity
)

data class BookListEntity(
    @SerializedName("items")
    val items: Map<String, BookSummaryEntity>
)

data class BookSummaryEntity(
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name:String,
    @SerializedName("rank")
    val rank: Int,
    @SerializedName("icon")
    val icon: String,
    @SerializedName("route")
    val route: String,
)
