package com.xuan.projectamber.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class BookResponseEntity(
    @SerializedName("data")
    val data: BookEntity
)
@Entity(tableName = "book_db")
data class BookEntity(
    @SerializedName("id")
    @PrimaryKey(autoGenerate = false)
    val id: String = "",
    @SerializedName("name")
    val name: String = "",
    @SerializedName("rank")
    val rank: Int = 0,
    @SerializedName("icon")
    val icon: String = "",
    @SerializedName("volume")
    val volume: List<VolumeEntity>? = listOf()
)

data class VolumeEntity(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("storyId")
    val storyId: String,
)
