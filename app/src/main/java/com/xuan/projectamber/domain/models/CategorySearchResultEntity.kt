package com.xuan.projectamber.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "categoryResult_db")
data class CategorySearchResultEntity(
    @PrimaryKey(autoGenerate = false)
    val id: String,
    val category: String,
    val name: String? = "",
    val icon: String? = "",
    val rank: Int? = 0,
    val primaryType: String? = "",
    val secondaryType: String? = "",
    val buffIcon: String? = "",
)
