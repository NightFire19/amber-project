package com.xuan.projectamber.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class ChangelogResponseEntity(
    @SerializedName("response")
    val response: Int,
    @SerializedName("data")
    val data: Map<String,ChangelogEntity>
)

data class ChangelogEntity(
    @SerializedName("version")
    val version: String = "0.0",
    @SerializedName("beta")
    val beta: String? = null,
    @SerializedName("items")
    val items: Map<String,List<String>> = mapOf()
)
