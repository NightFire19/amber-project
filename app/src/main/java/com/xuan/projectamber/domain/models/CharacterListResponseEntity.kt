package com.xuan.projectamber.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class CharacterListResponseEntity(
    @SerializedName("response")
    val response: Int,
    @SerializedName("data")
    val data: CharacterListEntity,
)

data class CharacterListEntity (
    @SerializedName("items")
    val items: Map<String,CharacterSummaryEntity>
)

data class CharacterSummaryEntity (
    @SerializedName("id")
    val id: String = "0",
    @SerializedName("rank")
    val rank: Int?    = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("element" )
    val element: String? = null,
    @SerializedName("weaponType")
    val weaponType: String? = null,
    @SerializedName("icon")
    val icon: String? = null,
    @SerializedName("birthday")
    val birthday: List<Int> = emptyList(),
    @SerializedName("release")
    val release: Int?    = null,
    @SerializedName("route")
    val route: String? = null,
)
