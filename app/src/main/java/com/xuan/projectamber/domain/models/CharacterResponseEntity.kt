package com.xuan.projectamber.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class CharacterResponseEntity(
    @SerializedName("response")
    val response: Int,
    @SerializedName("data")
    val data: CharacterEntity,
)

@Entity(tableName = "character_db")
data class CharacterEntity(
    @PrimaryKey(autoGenerate = false)
    @SerializedName("id")
    val id: String = "",
    @SerializedName("rank")
    val rank: Int? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("element")
    val element: String? = null,
    @SerializedName("weaponType")
    val weaponType: String? = null,
    @SerializedName("icon")
    val icon: String? = null,
    @SerializedName("birthday")
    val birthday: List<Int>? = null,
    @SerializedName("release")
    val release: Int? = null,
    @SerializedName("fetter")
    val fetterEntity: FetterEntity? = null,
    @SerializedName("upgrade")
    val upgradeEntity: CharacterUpgradeEntity? = null,
    @SerializedName("constellation")
    val constellation: Map<String,ConstellationEntity>? = null,
    @SerializedName("talent")
    val talent: Map<String,TalentEntity>? = null,
    @SerializedName("other")
    val other: OtherEntity? = null
)

data class FetterEntity(
    @SerializedName("title")
    val title: String,
    @SerializedName("detail")
    val detail: String,
    @SerializedName("constellation")
    val constellation: String,
    @SerializedName("native")
    val native: String,
    @SerializedName("cv")
    val cv: CvEntity,
)

data class CvEntity(
    @SerializedName("EN")
    val EN: String,
    @SerializedName("CHS")
    val CHS: String,
    @SerializedName("JP")
    val JP: String,
    @SerializedName("KR")
    val KR: String,
)

data class CharacterUpgradeEntity(
    @SerializedName("prop")
    val prop: List<Prop>,
    @SerializedName("promote")
    val promote: List<Promote>
)

data class Prop(
    @SerializedName("propType")
    val propType: String,
    @SerializedName("initValue")
    val initValue: Float,
    @SerializedName("type")
    val type: String,
)

data class Promote(
    @SerializedName("unlockMaxLevel")
    val unlockMaxLevel: Int,
    @SerializedName("promoteLevel")
    val promoteLevel: Int,
    @SerializedName("requiredPlayerLevel")
    val requiredPlayerLevel: Int,
    @SerializedName("coinCost")
    val coinCost: Int,
    @SerializedName("costItems")
    val costItems: Map<String,Int>?,
    @SerializedName("addProps")
    val addProps: Map<String, Float>
    )

data class ConstellationEntity(
    @SerializedName("name")
    val name: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("icon")
    val icon: String,
    )

data class TalentEntity(
    @SerializedName("type")
    val type: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("icon")
    val icon: String,
    @SerializedName("promote")
    val promote: Map<String,TalentPromoteEntity>,
)

data class TalentPromoteEntity(
    @SerializedName("level")
    val level: Int,
    @SerializedName("costItems")
    val costItems: Map<String, Int>?,
    @SerializedName("coinCost")
    val coinCost: Int,
    @SerializedName("description")
    val description: List<String>,
    @SerializedName("params")
    val params: List<Float>
)

data class OtherEntity(
    @SerializedName("nameCard")
    val nameCard: NameCard,
    @SerializedName("specialFood")
    val specialFood: SpecialFood,
)

data class NameCard(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("icon")
    val icon: String
)

data class SpecialFood(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("rank")
    val rank: Int,
    @SerializedName("effectIcon")
    val effectIcon: String,
    @SerializedName("icon")
    val icon: String
)