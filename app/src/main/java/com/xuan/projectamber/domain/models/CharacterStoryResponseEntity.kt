package com.xuan.projectamber.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class CharacterStoryResponseEntity(
    @SerializedName("response")
    val response: Int,
    @SerializedName("data")
    val data: CharacterStoryEntity,
)

@Entity(tableName = "story_db")
data class CharacterStoryEntity(
    @PrimaryKey(autoGenerate = false)
    var id: String = "",
    @SerializedName("quotes")
    val quotes: Map<String, QuoteEntity>? = null,
    @SerializedName("story")
    val story: Map<String, StoryEntity>? = null,
)

data class QuoteEntity(
    @SerializedName("title")
    val title: String,
    @SerializedName("audio")
    val audio: String,
    @SerializedName("text")
    val text: String,
    @SerializedName("tips")
    val tips: String,
)

data class StoryEntity(
    @SerializedName("title")
    val title: String,
    @SerializedName("text")
    val text: String,
    @SerializedName("tips")
    val tips: String,
)
