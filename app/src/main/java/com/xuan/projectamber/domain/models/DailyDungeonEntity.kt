package com.xuan.projectamber.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "dailyDungeon_db")
data class DailyDungeonEntity(
    @PrimaryKey(autoGenerate = false)
    @SerializedName("response")
    val response: Int,
    @SerializedName("data")
    val daysOfWeek: DaysOfWeek? = null,
)

data class DaysOfWeek(
    @SerializedName("monday")
    val monday: Map<String,Dungeon>,
    @SerializedName("tuesday")
    val tuesday: Map<String,Dungeon>,
    @SerializedName("wednesday")
    val wednesday: Map<String,Dungeon>,
    @SerializedName("thursday")
    val thursday: Map<String,Dungeon>,
    @SerializedName("friday")
    val friday: Map<String,Dungeon>,
    @SerializedName("saturday")
    val saturday: Map<String,Dungeon>,
)

data class Dungeon (
    @SerializedName("id")
    val id : Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("reward")
    val reward: List<Int>,
    @SerializedName("city")
    val city: Int,
        )

