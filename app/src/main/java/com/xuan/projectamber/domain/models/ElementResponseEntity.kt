package com.xuan.projectamber.domain.models

data class ElementResponseEntity(
    val response: Int,
    val data: ElementEntity
)

data class ElementEntity (
    val info: ElementInfoEntity? = null,
    val resonance: Map<String, ResonanceEntity> = mapOf(),
    val tutorials: Map<String, TutorialEntity> = mapOf(),
    val tips: Map<String, ElementTipEntity> = mapOf()
        )

data class ElementInfoEntity(
    val resonance: ResonanceInfoEntity,
)

data class ResonanceInfoEntity(
    val title: String,
    val description: String,
)

data class ResonanceEntity(
    val name: String,
    val elements: Map<String,Int>?,
    val description: String,
)

data class TutorialEntity(
    val name: String,
    val details: Map<String,TutorialDetailsEntity>,
    val icon: String,
)

data class TutorialDetailsEntity(
    val images: List<String>,
    val description: String,
)

data class ElementTipEntity(
    val description: String,
)