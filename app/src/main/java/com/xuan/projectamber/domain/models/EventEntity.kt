package com.xuan.projectamber.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "event_db")
data class EventEntity(
    @PrimaryKey(autoGenerate = false)
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: Name,
    @SerializedName("nameFull")
    val nameFull: NameFull,
    @SerializedName("description")
    val description: Description,
    @SerializedName("banner")
    val banner: Banner,
    @SerializedName("endAt")
    val endAt: String
)

data class Name (
    @SerializedName("EN")
    val name: String = ""
)

data class NameFull (
    @SerializedName("EN")
    val nameFull: String = ""
)

data class Description (
    @SerializedName("EN")
    val description: String = ""
)

data class Banner (
    @SerializedName("EN")
    val banner: String = ""
)

