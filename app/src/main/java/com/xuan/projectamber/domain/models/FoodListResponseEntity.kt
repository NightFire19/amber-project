package com.xuan.projectamber.domain.models

import com.google.gson.annotations.SerializedName

data class FoodListResponseEntity(
    @SerializedName("response")
    val response: Int,
    @SerializedName("data")
    val data: FoodListEntity
)

data class FoodListEntity(
    @SerializedName("items")
    val items: Map<String, FoodSummaryEntity>
)

data class FoodSummaryEntity(
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("type")
    val type: String,
    @SerializedName("recipe")
    val recipe: Boolean,
    @SerializedName("mapMark")
    val mapMark: Boolean,
    @SerializedName("icon")
    val icon: String,
    @SerializedName("rank")
    val rank: Int,
    @SerializedName("route")
    val route: String,
    @SerializedName("effectIcon")
    val effectIcon: String,
)