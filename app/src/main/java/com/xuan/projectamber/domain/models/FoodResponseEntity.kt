package com.xuan.projectamber.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.xuan.projectamber.domain.models.Description
import com.xuan.projectamber.domain.models.SourceEntity

data class FoodResponseEntity(
    @SerializedName("data")
    val data: FoodEntity,
)

@Entity(tableName = "food_db")
data class FoodEntity (
    @PrimaryKey(autoGenerate = false)
    @SerializedName("id")
    var id: String = "0",
    @SerializedName("name")
    val name: String = "name",
    @SerializedName("description")
    val description: String = "description",
    @SerializedName("type")
    val type: String = "type",
    @SerializedName("recipe")
    val recipe: RecipeEntity = RecipeEntity(),
    @SerializedName("mapMark")
    val mapMark: Boolean = false,
    @SerializedName("source")
    val source: List<SourceEntity> = listOf(),
    @SerializedName("icon")
    val icon: String = "",
    @SerializedName("rank")
    val rank: Int = 0,
    @SerializedName("route")
    val route: String = "route",
        )

data class RecipeEntity(
    @SerializedName("effect")
    val effect: Map<String,String> = mapOf(),
    @SerializedName("input")
    val input: Map<String,IngredientEntity> = mapOf(),
    @SerializedName("effectIcon")
    val effectIcon: String = "",
)

data class IngredientEntity(
    @SerializedName("icon")
    val icon: String,
    @SerializedName("count")
    val count: Int,
)