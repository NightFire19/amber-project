package com.xuan.projectamber.domain.models

import com.google.gson.annotations.SerializedName

data class FurnitureListResponseEntity(
    @SerializedName("response")
    val response: Int,
    @SerializedName("data")
    val data: FurnitureListEntity
)

data class FurnitureListEntity(
    @SerializedName("items")
    val items: Map<String, FurnitureSummaryEntity>
)

data class FurnitureSummaryEntity(
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("cost")
    val cost: Int,
    @SerializedName("comfort")
    val comfort: Int,
    @SerializedName("rank")
    val rank: Int,
    @SerializedName("icon")
    val icon: String,
    @SerializedName("route")
    val route: String,
    @SerializedName("categories")
    val categories: List<String>,
    @SerializedName("types")
    val types: List<String>
)
