package com.xuan.projectamber.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class FurnitureResponseEntity(
    @SerializedName("data")
    val data: FurnitureEntity,
)

@Entity(tableName = "furniture_db")
data class FurnitureEntity(
    @PrimaryKey(autoGenerate = false)
    @SerializedName("id")
    val id: String = "",
    @SerializedName("name")
    val name: String = "",
    @SerializedName("cost")
    val cost: Int? = 0,
    @SerializedName("comfort")
    val comfort: Int = 0,
    @SerializedName("rank")
    val rank: Int = 0,
    @SerializedName("icon")
    val icon: String = "",
    @SerializedName("route")
    val route: String = "",
    @SerializedName("categories")
    val categories: List<String> = listOf(),
    @SerializedName("types")
    val types: List<String> = listOf(),
    @SerializedName("description")
    val description: String  = "",
    @SerializedName("recipe")
    val recipe: FurnitureRecipeEntity? = null,
    @SerializedName("tips")
    val tips: String? = ""
)

data class FurnitureRecipeEntity(
    @SerializedName("exp")
    val exp: Int,
    @SerializedName("time")
    val time: Int,
    @SerializedName("input")
    val input: Map<String,IngredientEntity> = mapOf()
)