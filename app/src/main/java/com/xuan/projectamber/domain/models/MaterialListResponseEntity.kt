package com.xuan.projectamber.domain.models

import com.google.gson.annotations.SerializedName

data class MaterialListResponseEntity(
    @SerializedName("response")
    val response: Int,
    @SerializedName("data")
    val data: MaterialListEntity
)

data class MaterialListEntity(
    @SerializedName("items")
    val items: Map<String, MaterialSummaryEntity>
)

data class MaterialSummaryEntity(
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("type")
    val type: String,
    @SerializedName("recipe")
    val recipe: Boolean,
    @SerializedName("icon")
    val icon: String,
    @SerializedName("rank")
    val rank: Int,
    @SerializedName("route")
    val route: String,
)
