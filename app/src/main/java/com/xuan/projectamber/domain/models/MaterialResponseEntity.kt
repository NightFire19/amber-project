package com.xuan.projectamber.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class MaterialResponseEntity(
    @SerializedName("response")
    val response: Int,
    @SerializedName("data")
    val data: MaterialEntity,
)

@Entity(tableName = "material_db")
data class MaterialEntity(
    @PrimaryKey(autoGenerate = false)
    @SerializedName("id")
    var id: String = "0",
    @SerializedName("name")
    val name: String = "",
    @SerializedName("description")
    val description: String = "",
    @SerializedName("type")
    val type: String = "",
    @SerializedName("recipe")
    val recipe: Any? = Any(),
    @SerializedName("mapMark")
    val mapMark: Boolean = false,
    @SerializedName("source")
    var source: List<SourceEntity>? = emptyList(),
    @SerializedName("icon")
    val icon: String = "",
    @SerializedName("rank")
    val rank: Int = 1,
    @SerializedName("route")
    val route: String = "",
)

data class SourceEntity(
    @SerializedName("name")
    val name: String = "",
    @SerializedName("type")
    val type: String = "",
    @SerializedName("days")
    val days: List<String>? = listOf()
)
