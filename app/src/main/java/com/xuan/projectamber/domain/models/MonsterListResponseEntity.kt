package com.xuan.projectamber.domain.models

import com.google.gson.annotations.SerializedName

data class MonsterListResponseEntity(
    @SerializedName("response")
    val response: Int,
    @SerializedName("data")
    val data: MonsterListEntity
)

data class MonsterListEntity(
    @SerializedName("items")
    val items: Map<String, MonsterSummaryEntity>
)

data class MonsterSummaryEntity (
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("type")
    val type: String,
    @SerializedName("icon")
    val icon: String,
    @SerializedName("rank")
    val rank: Int,
    @SerializedName("route")
    val route: String,
        )
