package com.xuan.projectamber.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class MonsterResponseEntity(
    val response: Int,
    val data: MonsterEntity,
)

@Entity(tableName = "monster_db")
data class MonsterEntity(
    @PrimaryKey(autoGenerate = false)
    @SerializedName("id")
    val id: Int = 0,
    @SerializedName("name")
    val name: String = "",
    @SerializedName("type")
    val type: String = "",
    @SerializedName("icon")
    val icon: String = "",
    @SerializedName("route")
    val route: String = "",
    @SerializedName("title")
    val title: String = "",
    @SerializedName("specialName")
    val specialName: String = "",
    @SerializedName("description")
    val description: String = "",
    @SerializedName("entries")
    val entries: Map<String, EntryEntity> = mapOf(),
    @SerializedName("tips")
    val tips: Map<String, TipEntity>? = null,
)

data class EntryEntity(
    @SerializedName("id")
    val id: Int = 0,
    @SerializedName("type")
    val type: String = "",
    @SerializedName("hpDrops")
    val hpDrops: List<HpDropsEntity>? = emptyList(),
    @SerializedName("prop")
    val prop: List<PropEntity> = emptyList(),
    @SerializedName("resistance")
    val resistance: Map<String,Float> = emptyMap(),
    @SerializedName("reward")
    val reward: Map<String, RewardEntity> = mapOf(),
)

data class HpDropsEntity(
    @SerializedName("id")
    val id: Int,
    @SerializedName("hpPercent")
    val hpPercent: Int,
)

data class PropEntity(
    @SerializedName("propType")
    val propType: String,
    @SerializedName("initValue")
    val initValue: Float,
    @SerializedName("type")
    val type: String,
)

data class TipEntity(
    @SerializedName("images")
    val images: List<String>,
    @SerializedName("description")
    val description: String
)

data class RewardEntity(
    @SerializedName("rank")
    val rank: Int,
    @SerializedName("icon")
    val icon: String,
    @SerializedName("count")
    val count: String? = "0",
)
