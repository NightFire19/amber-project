package com.xuan.projectamber.domain.models

import com.google.gson.annotations.SerializedName

data class NamecardListResponseEntity(
    @SerializedName("data")
    val data: NamecardListEntity
)

data class NamecardListEntity(
    @SerializedName("items")
    val items: Map<String, NamecardSummaryEntity>
)

data class NamecardSummaryEntity(
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name:String,
    @SerializedName("type")
    val type: String,
    @SerializedName("rank")
    val rank: Int,
    @SerializedName("icon")
    val icon: String,
    @SerializedName("route")
    val route: String,
)