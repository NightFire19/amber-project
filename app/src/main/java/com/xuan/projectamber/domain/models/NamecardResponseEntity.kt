package com.xuan.projectamber.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class NamecardResponseEntity(
    @SerializedName("data")
    val data: NamecardEntity,
)

@Entity(tableName = "namecard_db")
data class NamecardEntity (
    @PrimaryKey(autoGenerate = false)
    @SerializedName("id")
    val id: Int = 0,
    @SerializedName("name")
    val name: String = "",
    @SerializedName("type")
    val type: String = "",
    @SerializedName("rank")
    val rank: Int = 0,
    @SerializedName("icon")
    val icon: String = "",
    @SerializedName("route")
    val route: String = "",
    @SerializedName("description")
    val description: String = "",
    @SerializedName("descriptionSpecial")
    val descriptionSpecial: String = "",
    @SerializedName("source")
    val source: String = ""
)
