package com.xuan.projectamber.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class QuestListResponseEntity(
    @SerializedName("data")
    val data: QuestItems
)

data class QuestItems(
    @SerializedName("items")
    val items : Map<String,QuestEntity> = mapOf()
)

@Entity(tableName = "quest_db")
data class QuestEntity(
    @PrimaryKey(autoGenerate = false)
    @SerializedName("id")
    val id: Int = 0,
    @SerializedName("type")
    val type: String? = "",
    @SerializedName("chapterNum")
    val chapterNum: String? = "",
    @SerializedName("chapterTitle")
    val chapterTitle: String = "",
    @SerializedName("chapterIcon")
    val chapterIcon: String? = "",
    @SerializedName("chapterImageTitle")
    val chapterImageTitle: String? = "",
    @SerializedName("route")
    val route: String = "",
    @SerializedName("chapterCount")
    val chapterCount : Int = 0
)
