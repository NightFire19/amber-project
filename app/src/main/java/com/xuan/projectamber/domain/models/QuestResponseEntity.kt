package com.xuan.projectamber.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class QuestResponseEntity (
    @SerializedName("data")
    val data: QuestDetailEntity
        )

@Entity(tableName = "questDetails_db")
data class QuestDetailEntity(
    @PrimaryKey(autoGenerate = false)
    var id: String = "",
    @SerializedName("info")
    val info: QuestInfoEntity? = null,
    @SerializedName("storyList")
    val storyList: Map<String,StoryListEntity> = mapOf()
)

data class QuestInfoEntity(
    @SerializedName("id")
    val id :Int,
    @SerializedName("type")
    val type: String?,
    @SerializedName("chapterNum")
    val chapterNum: String?,
    @SerializedName("chapterTitle")
    val chapterTitle: String,
    @SerializedName("chapterIcon")
    val chapterIcon: String?,
    @SerializedName("chapterImageTitle")
    val chapterImageTitle: String?,
    @SerializedName("route")
    val route: String,
    @SerializedName("chapterCount")
    val chapterCount: Int,
)

data class StoryListEntity(
    @SerializedName("id")
    val id: Int = 0,
    @SerializedName("info")
    val info: QuestStoryInfoEntity = QuestStoryInfoEntity(),
    @SerializedName("reward")
    val reward: Map<String, QuestRewardEntity> = mapOf(),
    @SerializedName("suggestTrackMainQuestList")
    val suggestTrackMainQuestList: Map<String,String> = mapOf(),
    @SerializedName("story")
    val story: Map<String,QuestStoryEntity> = mapOf(),
    @SerializedName("npcLIst")
    val npcLIst: List<String> = listOf()
)

data class QuestStoryInfoEntity(
    @SerializedName("title")
    val title: String = "",
    @SerializedName("description")
    val description: String = "",
)

data class QuestRewardEntity(
    @SerializedName("id")
    val id: Int,
    @SerializedName("icon")
    val icon: String,
    @SerializedName("type")
    val type: String,
    @SerializedName("rank")
    val rank: Int,
    @SerializedName("count")
    val count: Int,
)

data class QuestStoryEntity(
    @SerializedName("id")
    val id: Int,
    @SerializedName("isHidden")
    val isHidden: Boolean,
    @SerializedName("title")
    val title: String?,
    @SerializedName("stepDescription")
    val stepDescription: String?,
    @SerializedName("taskData")
    val taskData: List<TaskData>?,
    @SerializedName("narratorData")
    val narratorData: List<NarratorData>?
)

data class TaskData(
    @SerializedName("isAuto")
    val isAuto: Boolean,
    @SerializedName("condType")
    val condType: String,
    @SerializedName("taskType")
    val taskType: String,
    @SerializedName("initDialog")
    val initDialog: String,
    @SerializedName("items")
    val items: Map<String, TaskItem>
)

data class TaskItem(
    @SerializedName("type")
    val type: String,
    @SerializedName("role")
    val role: String,
    @SerializedName("text")
    val text: List<TaskText>
)

data class TaskText(
    @SerializedName("text")
    val text: String,
    @SerializedName("next")
    val next: String,
)

data class NarratorData(
    @SerializedName("id")
    val id: Int,
    @SerializedName("isWarning")
    val isWarning: Boolean,
    @SerializedName("type")
    val type: String,
    @SerializedName("items")
    val items: List<NarratorItem>
)

data class NarratorItem(
    @SerializedName("role")
    val role: String,
    @SerializedName("text")
    val text: String
)