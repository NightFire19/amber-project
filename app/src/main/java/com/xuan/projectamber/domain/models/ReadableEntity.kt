package com.xuan.projectamber.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class ReadableResponseEntity(
    @SerializedName("data")
    val data: String
)

@Entity(tableName = "readable_db")
data class ReadableEntity(
    @PrimaryKey(autoGenerate = false)
    val id: String,
    val data: String,
)
