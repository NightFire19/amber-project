package com.xuan.projectamber.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class ReliquaryResponseEntity(
    @SerializedName("data")
    val data: ReliquaryEntity
)
@Entity(tableName = "reliquary_db")
data class ReliquaryEntity(
    @PrimaryKey(autoGenerate = false)
    @SerializedName("id")
    val id: Int = 0,
    @SerializedName("name")
    val name: String = "",
    @SerializedName("levelList")
    val levelList: List<Int> = listOf(),
    @SerializedName("affixList")
    val affixList: Map<String,String>  = mapOf(),
    @SerializedName("icon")
    val icon: String = "",
    @SerializedName("route")
    val route: String = "",
    @SerializedName("suit")
    val suit: Map<String,SuitEntity> = mapOf()
)

data class SuitEntity(
    @SerializedName("name")
    val name: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("maxLevel")
    val maxLevel: Int,
    @SerializedName("icon")
    val icon: String,
)
