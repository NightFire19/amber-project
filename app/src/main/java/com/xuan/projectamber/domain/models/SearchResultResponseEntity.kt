package com.xuan.projectamber.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class SearchResultResponseEntity(
    @SerializedName("response")
    val response: Int = 0,
    @SerializedName("data")
    val data: List<SearchResultEntity>
)

@Entity(tableName = "search_db")
data class SearchResultEntity(
    @PrimaryKey(autoGenerate = false)
    @SerializedName("id")
    val id: String = "",
    @SerializedName("type")
    val type: String = "",
    @SerializedName("name")
    val name: String = "",
    @SerializedName("route")
    val route: String = "",
    @SerializedName("icon")
    val icon: String? = ""
)
