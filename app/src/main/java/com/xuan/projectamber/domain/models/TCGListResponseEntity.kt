package com.xuan.projectamber.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class TCGListResponseEntity(
    @SerializedName("response")
    val response: Int,
    @SerializedName("data")
    val data: TCGDataEntity,
)

data class TCGDataEntity(
    @SerializedName("types")
    val types: Map<String, String>,
    @SerializedName("items")
    val items: Map<String,TCGSummaryEntity>
)


@Entity(tableName = "tcgSummary_db")
data class TCGSummaryEntity(
    @PrimaryKey(autoGenerate = false)
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("type")
    val type: String,
    @SerializedName("tags")
    val tags: Map<String,String>?,
    @SerializedName("props")
    val props: Map<String,Int>?,
    @SerializedName("icon")
    val icon: String,
    @SerializedName("route")
    val route: String,
    @SerializedName("sortOrder")
    val sortOrder: Int,
)