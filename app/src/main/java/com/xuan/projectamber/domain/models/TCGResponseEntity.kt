package com.xuan.projectamber.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class TCGResponseEntity(
    @SerializedName("response")
    val response: Int,
    @SerializedName("data")
    val data: TCGEntity
)

@Entity(tableName = "tcg_db")
data class TCGEntity (
    @PrimaryKey(autoGenerate = false)
    @SerializedName("id")
    val id: Int = 0,
    @SerializedName("name")
    val name: String = "",
    @SerializedName("type")
    val type: String = "",
    @SerializedName("tags")
    val tags: Map<String, String> = mapOf(),
    @SerializedName("props")
    val props: Map<String, String> = mapOf(),
    @SerializedName("icon")
    val icon: String = "",
    @SerializedName("route")
    val route: String = "",
    @SerializedName("storyTitle")
    val storyTitle: String = "",
    @SerializedName("storyDetail")
    val storyDetail: String = "",
    @SerializedName("source")
    val source: String? = "",
    @SerializedName("dictionary")
    val dictionary: Map<String, DictionaryEntity>? = mapOf(),
    @SerializedName("talent")
    val talent: Map<String,TCGTalentEntity> = mapOf(),
    @SerializedName("relatedEntries")
    val relatedEntries: List<TCGRelatedEntity>? = listOf()
        )

data class DictionaryEntity(
    @SerializedName("name")
    val name: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("cost")
    val cost: Map<String,Int>?,
    @SerializedName("params")
    val params: Map<String,String>,
    @SerializedName("tags")
    val tags: Map<String,String>?,
    @SerializedName("icon")
    val icon: String?,
    @SerializedName("effect")
    val effect: String,
)

data class TCGTalentEntity(
    @SerializedName("name")
    val name: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("cost")
    val cost: Map<String, Int>?,
    @SerializedName("params")
    val params: Map<String,String>,
    @SerializedName("tags")
    val tags: Map<String,String>?,
    @SerializedName("icon")
    val icon: String?,
    @SerializedName("keywords")
    val keywords: Map<String, String>
)

data class TCGRelatedEntity (
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("type")
    val type: String,
    @SerializedName("icon")
    val icon: String,
        )
