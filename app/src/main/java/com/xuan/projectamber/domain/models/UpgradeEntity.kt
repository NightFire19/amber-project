package com.xuan.projectamber.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "upgrade_db")
data class UpgradeEntity(
    @PrimaryKey(autoGenerate = false)
    @SerializedName("response")
    val response: Int,
    @SerializedName("data")
    val data: DataUpgrade? = null,
)

data class DataUpgrade (
    @SerializedName("avatar")
    val avatar: Map<String,UpgradeDetail>,
    @SerializedName("weapon")
    val weapon: Map<String,UpgradeDetail>
        )

data class UpgradeDetail(
    @SerializedName("rank")
    val rank: Int,
    @SerializedName("icon")
    val icon: String,
    @SerializedName("items")
    val items: Map<String,Int>
)
