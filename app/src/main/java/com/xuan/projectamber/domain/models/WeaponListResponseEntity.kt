package com.xuan.projectamber.domain.models

import com.google.gson.annotations.SerializedName

data class WeaponListResponseEntity(
    @SerializedName("response")
    val response: Int,
    @SerializedName("data")
    val data: WeaponListEntity,
)
data class WeaponListEntity (
    @SerializedName("items")
    val items: Map<String,WeaponSummaryEntity>
)

data class WeaponSummaryEntity (
    @SerializedName("id")
    val id: String = "0",
    @SerializedName("rank")
    val rank: Int?    = null,
    @SerializedName("type")
    val type: String? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("icon")
    val icon: String? = null,
    @SerializedName("route" )
    val route: String? = null,
)