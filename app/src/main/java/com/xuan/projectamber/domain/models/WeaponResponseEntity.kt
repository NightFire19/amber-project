package com.xuan.projectamber.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class WeaponResponseEntity(
    @SerializedName("response")
    val response: Int,
    @SerializedName("data")
    val data: WeaponEntity,
)

@Entity(tableName = "weapon_db")
data class WeaponEntity(
    @PrimaryKey(autoGenerate = false)
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("rank")
    val rank: Int? = null,
    @SerializedName("type")
    val type: String? = "",
    @SerializedName("name")
    val name: String? = "",
    @SerializedName("description")
    val description: String? = "",
    @SerializedName("icon")
    val icon: String? = "",
    @SerializedName("storyId")
    val storyId: Int? = null,
    @SerializedName("affix")
    val affix: Map<String,AffixEntity>? = null,
    @SerializedName("route")
    val route: String? = "",
    @SerializedName("upgrade")
    val upgrade: WeaponUpgradeEntity? = null,
    @SerializedName("ascension")
    val ascension: Map<String, Int>? = null,
)

data class AffixEntity(
    @SerializedName("name")
    val name: String? = "",
    @SerializedName("upgrade")
    val upgrade: Map<String, String>? = null
)

data class WeaponUpgradeEntity(
    @SerializedName("awakenCost")
    val awakenCost: List<Int>? = null,
    @SerializedName("prop")
    val prop: List<WeaponPropEntity>? = null,
    @SerializedName("promote")
    val promote: List<WeaponPromoteEntity>? = null,
)

data class WeaponPropEntity(
    @SerializedName("propType")
    val propType: String = "",
    @SerializedName("initValue")
    val initValue: Float,
    @SerializedName("type")
    val type: String = "",
)

data class WeaponPromoteEntity(
    @SerializedName("promoteLevel")
    val promoteLevel: String? = "",
    @SerializedName("costItems")
    val costItems: Map<String,Int>? = null,
    @SerializedName("coinCost")
    val coinCost: Int? = null,
    @SerializedName("addProps")
    val addProps: Map<String, Float>? = null,
    @SerializedName("unlockMaxLevel")
    val unlockMaxLevel: Int? = null,
    @SerializedName("requiredPlayerLevel")
    val requiredPlayerLevel: Int? = null
)