package com.xuan.projectamber.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class WeaponStoryResponseEntity(
    @SerializedName("response")
    val response: Int,
    @SerializedName("data")
    val data: String,
)
@Entity(tableName = "weaponStory_db")
data class WeaponStoryEntity(
    @PrimaryKey(autoGenerate = false)
    val id: String = "",
    val story: String = "",
)
