package com.xuan.projectamber.domain.repository

import android.util.Log
import com.xuan.projectamber.data.ds.UserPreferencesRepository
import com.xuan.projectamber.data.local.AmberDB
import com.xuan.projectamber.data.local.AmberDao
import com.xuan.projectamber.data.remote.ApiService
import com.xuan.projectamber.data.repository.Repository
import com.xuan.projectamber.domain.models.*
import com.xuan.projectamber.util.Constants.LANGUAGE_MAP
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import javax.inject.Inject

class RepositoryImpl @Inject constructor(
    private val amberDB: AmberDB,
    private val apiService: ApiService,
    private val amberDao: AmberDao,
    private val userPreferencesRepository: UserPreferencesRepository
) : Repository {

    override suspend fun clearAllTables() = withContext(Dispatchers.IO) {
        amberDB.clearAllTables()
    }

    override suspend fun insertEvents(events: List<EventEntity>) = withContext(Dispatchers.IO) {
        amberDao.insertEvents(events)
    }

    override suspend fun getLikeEvents(search: String): Flow<List<EventEntity>> =
        withContext(Dispatchers.IO) {
            return@withContext amberDao.getLikeEvents(search)
        }

    override suspend fun getNotLikeEvents(search: String): Flow<List<EventEntity>> =
        withContext(Dispatchers.IO) {
            return@withContext amberDao.getNotLikeEvents(search)
        }

    override suspend fun fetchEvents(): Unit  = withContext(Dispatchers.IO)  {
        try {
            val apiResponse = apiService.getAllEvents()
            amberDao.deleteEvents()
            if (apiResponse.isSuccessful) {
                insertEvents(ArrayList(apiResponse.body()!!.values))
            } else {
                throw Exception("Something went wrong.")
            }
        } catch (e: Exception) {
            Log.v("test2",e.message.toString())
            throw e
        }
    }

    override suspend fun fetchDailyDungeons(): Unit = withContext(Dispatchers.IO) {
        try {
            val apiResponse = apiService.getDungeons(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!,)
            if (apiResponse.body() != null) insertDailyDungeons(apiResponse.body()!!)
        } catch (exception: Exception) {
            throw exception
        }
    }

    override suspend fun insertDailyDungeons(dailyDungeonEntity: DailyDungeonEntity) =
        withContext(Dispatchers.IO) {
            amberDao.insertDailyDungeons(dailyDungeonEntity)
        }

    override suspend fun getDailyDungeons(): Flow<DailyDungeonEntity> =
        withContext(Dispatchers.IO) {
            return@withContext amberDao.getDailyDungeons()
        }

    override suspend fun fetchUpgrades() = withContext(Dispatchers.IO) {
        try {
            val apiResponse = apiService.getUpgrades()
            if (apiResponse.body() != null) {
                amberDao.insertUpgrades(apiResponse.body()!!)
            }
        } catch (exception: Exception) {
            throw exception
        }
    }

    override suspend fun getUpgrades(): Flow<UpgradeEntity> = withContext(Dispatchers.IO) {
        return@withContext amberDao.getUpgrades()
    }

    override suspend fun fetchCharacter(id: String) = withContext(Dispatchers.IO) {
        val apiResponse = apiService.getCharacter(
            lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!,
            id = id
        )
        if (apiResponse.body() != null) {
            amberDao.insertCharacter(apiResponse.body()!!.data)
        }
    }

    override suspend fun fetchCharacterStory(id: String) {
        val apiResponse = apiService.getCharacterStory(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!, id = id)
        if (apiResponse.body() != null) {
            val holder = apiResponse.body()!!.data
            holder.id = id
            amberDao.insertCharacterStory(holder)
        }
    }

    override suspend fun getCharacter(id: String) = withContext(Dispatchers.IO) {

        if (!amberDao.isRowIsExist(id)) {
            fetchCharacter(id)
        }

        return@withContext amberDao.getCharacter(id)
    }

    override suspend fun getCharacterStory(id: String) = withContext(Dispatchers.IO) {
        if (!amberDao.isStoryRowIsExist(id)) {
            fetchCharacterStory(id)
        }
        return@withContext amberDao.getCharacterStory(id)
    }

    override suspend fun fetchWeapon(id: String) {
        val apiResponse = apiService.getWeapon(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!,id = id)
        if (apiResponse.body() != null) {
            amberDao.insertWeapon(apiResponse.body()!!.data)
        }
    }

    override suspend fun getWeapon(id: String) = withContext(Dispatchers.IO) {
        if (!amberDao.isWeaponRowIsExist(id)) {
            fetchWeapon(id)
        }
        return@withContext amberDao.getWeapon(id)
    }

    override suspend fun fetchWeaponStory(id: String) {
        val apiResponse = apiService.getWeaponStory(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!, id = id)
        if (apiResponse.body() != null) {
            amberDao.insertWeaponStory(
                WeaponStoryEntity(
                    id = id,
                    story = apiResponse.body()!!.data
                )
            )
        }
    }

    override suspend fun getWeaponStory(id: String) = withContext(Dispatchers.IO) {
        if (!amberDao.isWeaponStoryRowIsExist(id)) {
            fetchWeaponStory(id)
        }
        return@withContext amberDao.getWeaponStory(id)
    }

    override suspend fun fetchSearchResults() = withContext(Dispatchers.IO) {
        val apiResponse = apiService.getSearchResults(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!)
        if (apiResponse.body() != null) {
            if (apiResponse.body()!!.data.size != amberDao.getSearchResultCount()) {
                apiResponse.body()!!.data.forEach { searchResultEntity ->
                    amberDao.insertSearchResult(searchResultEntity)
                }
            }
        }
    }

    override suspend fun getSearchResults(query: String) = withContext(Dispatchers.IO) {
        return@withContext amberDao.getSearchResults(query)
    }

    override suspend fun fetchMonster(id: String) {
        val apiResponse = apiService.getMonster(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!, id = id)
        if (apiResponse.body() != null) {
            amberDao.insertMonster(apiResponse.body()!!.data)
        }
    }

    override suspend fun getMonster(id: String) = withContext(Dispatchers.IO) {
        if (!amberDao.doesMonsterRowExist(id)) {
            fetchMonster(id)
        }
        return@withContext amberDao.getMonster(id)
    }

    override suspend fun fetchMaterial(id: String) {
        val apiResponse = apiService.getMaterial(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!, id = id)
        if (apiResponse.body() != null) {
            apiResponse.body()!!.data.id = id
            if (apiResponse.body()!!.data.source == null) {
                apiResponse.body()!!.data.source = emptyList()
            }
            amberDao.insertMaterial(apiResponse.body()!!.data)
        }
    }

    override suspend fun getMaterial(id: String) = withContext(Dispatchers.IO) {
        if (!amberDao.doesMaterialRowExist(id)) {
            fetchMaterial(id)
        }
        return@withContext amberDao.getMaterial(id)
    }

    override suspend fun fetchResultList(category: String) =
        withContext(Dispatchers.IO) {
            when (category) {
                "avatar" -> {
                    val apiResponse = apiService.getCharacters(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!,)
                    if (apiResponse.body() != null) {
                        if (apiResponse.body()!!.data.items.size != amberDao.getCategorySearchCount(
                                category = category
                            )
                        ) {
                            apiResponse.body()!!.data.items.forEach { (_, characterSummaryEntity) ->
                                amberDao.insertCategorySummary(
                                    CategorySearchResultEntity(
                                        id = characterSummaryEntity.id,
                                        category = category,
                                        name = characterSummaryEntity.name,
                                        icon = characterSummaryEntity.icon,
                                        rank = characterSummaryEntity.rank,
                                        primaryType = characterSummaryEntity.weaponType,
                                        secondaryType = characterSummaryEntity.element,
                                        buffIcon = characterSummaryEntity.element,
                                    )
                                )
                            }
                        }
                    }
                }
                "weapon" -> {
                    val apiResponse = apiService.getWeapons(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!,)
                    if (apiResponse.body() != null) {
                        if (apiResponse.body()!!.data.items.size != amberDao.getCategorySearchCount(
                                category = category
                            )
                        ) {
                            apiResponse.body()!!.data.items.forEach { (_, weaponSummaryEntity) ->
                                amberDao.insertCategorySummary(
                                    CategorySearchResultEntity(
                                        id = weaponSummaryEntity.id,
                                        category = category,
                                        name = weaponSummaryEntity.name,
                                        icon = weaponSummaryEntity.icon,
                                        rank = weaponSummaryEntity.rank,
                                        primaryType = weaponSummaryEntity.type,
                                        secondaryType = ""
                                    )
                                )
                            }
                        }
                    }
                }
                "monster" -> {
                    val apiResponse = apiService.getMonsters(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!,)
                    if (apiResponse.body() != null) {
                        if (apiResponse.body()!!.data.items.size != amberDao.getCategorySearchCount(
                                category = category
                            )
                        ) {
                            apiResponse.body()!!.data.items.forEach { (_, monsterSummaryEntity) ->
                                amberDao.insertCategorySummary(
                                    CategorySearchResultEntity(
                                        id = monsterSummaryEntity.id,
                                        category = category,
                                        name = monsterSummaryEntity.name,
                                        icon = monsterSummaryEntity.icon,
                                        rank = monsterSummaryEntity.rank,
                                        primaryType = monsterSummaryEntity.type,
                                        secondaryType = ""
                                    )
                                )
                            }
                        }
                    }
                }
                "food" -> {
                    val apiResponse = apiService.getFoods(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!,)
                    if (apiResponse.body() != null) {
                        if (apiResponse.body()!!.data.items.size != amberDao.getCategorySearchCount(
                                category = category
                            )
                        ) {
                            apiResponse.body()!!.data.items.forEach { (_, monsterSummaryEntity) ->
                                amberDao.insertCategorySummary(
                                    CategorySearchResultEntity(
                                        id = monsterSummaryEntity.id,
                                        category = category,
                                        name = monsterSummaryEntity.name,
                                        icon = monsterSummaryEntity.icon,
                                        rank = monsterSummaryEntity.rank,
                                        primaryType = monsterSummaryEntity.effectIcon,
                                        secondaryType = "",
                                        buffIcon = monsterSummaryEntity.effectIcon
                                    )
                                )
                            }
                        }
                    }
                }
                "material" -> {
                    val apiResponse = apiService.getMaterials(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!,)
                    if (apiResponse.body() != null) {
                        if (apiResponse.body()!!.data.items.size != amberDao.getCategorySearchCount(
                                category = category
                            )
                        ) {
                            apiResponse.body()!!.data.items.forEach { (_, materialSummaryEntity) ->
                                amberDao.insertCategorySummary(
                                    CategorySearchResultEntity(
                                        id = materialSummaryEntity.id,
                                        category = category,
                                        name = materialSummaryEntity.name,
                                        icon = materialSummaryEntity.icon,
                                        rank = materialSummaryEntity.rank,
                                        primaryType = materialSummaryEntity.type,
                                        secondaryType = ""
                                    )
                                )
                            }
                        }
                    }
                }
                "furniture" -> {
                    val apiResponse = apiService.getFurniture(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!,)
                    if (apiResponse.body() != null) {
                        if (
                            apiResponse.body()!!.data.items.size != amberDao.getCategorySearchCount(
                                category = category
                            )
                        ) {
                            apiResponse.body()!!.data.items.forEach { (_, furnitureSummaryEntity) ->
                                amberDao.insertCategorySummary(
                                    CategorySearchResultEntity(
                                        id = furnitureSummaryEntity.id,
                                        category = category,
                                        name = furnitureSummaryEntity.name,
                                        icon = furnitureSummaryEntity.icon,
                                        rank = furnitureSummaryEntity.rank,
                                        primaryType = furnitureSummaryEntity.categories[0],
                                        secondaryType = ""
                                    )
                                )
                            }
                        }
                    }
                }
                "reliquary" -> {
                    val apiResponse = apiService.getArtifacts(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!,)
                    if (apiResponse.body() != null) {
                        if (
                            apiResponse.body()!!.data.items.size != amberDao.getCategorySearchCount(
                                category = category
                            )
                        ) {
                            apiResponse.body()!!.data.items.forEach { (_, artifactSummaryEntity) ->
                                amberDao.insertCategorySummary(
                                    CategorySearchResultEntity(
                                        id = artifactSummaryEntity.id,
                                        category = category,
                                        name = artifactSummaryEntity.name,
                                        icon = artifactSummaryEntity.icon,
                                        rank = artifactSummaryEntity.levelList.last(),
                                        primaryType = "",
                                        secondaryType = ""
                                    )
                                )
                            }
                        }
                    }
                }
                "namecard" -> {
                    val apiResponse = apiService.getNamecards(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!,)
                    if (apiResponse.body() != null) {
                        if (
                            apiResponse.body()!!.data.items.size != amberDao.getCategorySearchCount(
                                category = category
                            )
                        ) {
                            apiResponse.body()!!.data.items.forEach { (_, namecardSummaryEntity) ->
                                amberDao.insertCategorySummary(
                                    CategorySearchResultEntity(
                                        id = namecardSummaryEntity.id,
                                        category = category,
                                        name = namecardSummaryEntity.name,
                                        icon = namecardSummaryEntity.icon,
                                        rank = namecardSummaryEntity.rank,
                                        primaryType = namecardSummaryEntity.type,
                                        secondaryType = ""
                                    )
                                )
                            }
                        }
                    }
                }
                "book" -> {
                    val apiResponse = apiService.getBooks(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!,)
                    if (apiResponse.body() != null) {
                        if (
                            apiResponse.body()!!.data.items.size != amberDao.getCategorySearchCount(
                                category = category
                            )
                        ) {
                            apiResponse.body()!!.data.items.forEach { (_, bookSummaryEntity) ->
                                amberDao.insertCategorySummary(
                                    CategorySearchResultEntity(
                                        id = bookSummaryEntity.id,
                                        category = category,
                                        name = bookSummaryEntity.name,
                                        icon = bookSummaryEntity.icon,
                                        rank = bookSummaryEntity.rank,
                                        primaryType = "",
                                        secondaryType = ""
                                    )
                                )
                            }
                        }
                    }
                }
            }
        }


    override suspend fun getResultList(
        query: String,
        category: String,
        limit: Int,
    ) =
        withContext(Dispatchers.IO) {
            fetchResultList(category)

            return@withContext amberDao.getCategorySearchResults(
                query = query,
                category = category,
                limit = limit,
            )
        }

    override suspend fun getResultList(
        ids: List<String>,
        category: String,
    ) =
        withContext(Dispatchers.IO) {
            fetchResultList(category)
            return@withContext amberDao.getCategorySearchResults(
                ids = ids,
                category = category,
            )
        }

    override suspend fun fetchFood(id: String) {
        val apiResponse = apiService.getFood(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!, id = id)
        val food = apiResponse.body()!!.data
        food.id = id
        if (apiResponse.body() != null) {
            amberDao.insertFood(food)
        }
    }

    override suspend fun getFood(id: String) = withContext(Dispatchers.IO) {
        if (!amberDao.doesFoodRowExist(id)) {
            fetchFood(id)
        }
        return@withContext amberDao.getFood(id)
    }

    override suspend fun fetchFurniture(id: String) {
        val apiResponse = apiService.getFurniture(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!, id = id)
        val furniture = apiResponse.body()!!.data
        if (apiResponse.body() != null) {
            amberDao.insertFurniture(furniture)
        }
    }

    override suspend fun getFurniture(id: String) = withContext(Dispatchers.IO) {
        if (!amberDao.doesFurnitureRowExist(id)) {
            fetchFurniture(id)
        }
        return@withContext amberDao.getFurniture(id)
    }

    override suspend fun fetchNamecard(id: String) {
        val apiResponse = apiService.getNamecard(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!, id = id)
        val namecard = apiResponse.body()!!.data
        if (apiResponse.body() != null) {
            amberDao.insertNamecard(namecard)
        }
    }

    override suspend fun getNamecard(id: String) = withContext(Dispatchers.IO) {
        if (!amberDao.doesNamecardRowExist(id)) {
            fetchNamecard(id)
        }
        return@withContext amberDao.getNamecard(id)
    }

    override suspend fun fetchArtifact(id: String) {
        val apiResponse = apiService.getArtifact(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!, id = id)
        val artifact = apiResponse.body()!!.data
        if (apiResponse.body() != null) {
            amberDao.insertReliquary(artifact)
        }
    }

    override suspend fun getArtifact(id: String) = withContext(Dispatchers.IO) {
        if (!amberDao.doesReliquaryRowExist(id)) {
            fetchArtifact(id)
        }
        return@withContext amberDao.getReliquary(id)
    }

    override suspend fun fetchBook(id: String) {
        val apiResponse = apiService.getBook(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!, id = id)
        val book = apiResponse.body()!!.data
        if (apiResponse.body() != null) {
            amberDao.insertBook(book)
        }
    }

    override suspend fun getBook(id: String) = withContext(Dispatchers.IO) {
        if (!amberDao.doesReliquaryRowExist(id)) {
            fetchBook(id)
        }
        return@withContext amberDao.getBook(id)
    }

    override suspend fun fetchReadable(id: String) {
        val apiResponse = apiService.getReadable(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!, id = id)
        val readable = ReadableEntity(id = id, data = apiResponse.body()!!.data)
        if (apiResponse.body() != null) {
            amberDao.insertReadable(readable)
        }
    }

    override suspend fun getReadables(ids: List<String>) = withContext(Dispatchers.IO) {
        ids.forEach { id ->
            if (!amberDao.doesReadableRowExist(id)) {
                fetchReadable(id)
            }
        }

        return@withContext amberDao.getReadable(ids)
    }


    override suspend fun getChangelog() = withContext(Dispatchers.IO) {
        val apiResponse = apiService.getChangelog()
        if (apiResponse.isSuccessful) {
            return@withContext apiResponse.body()!!.data
        } else {
            throw Exception()
        }
    }

    override suspend fun fetchQuests() = withContext(Dispatchers.IO) {

        val apiResponse = apiService.getQuests(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!,)
        if (apiResponse.isSuccessful) {
            val quests = apiResponse.body()!!.data.items.values.toList()
            Log.v("test2", quests.toString())
            if (quests.size != amberDao.getQuestCount()) {
                amberDao.insertQuests(quests)
            }
        }
    }

    override suspend fun getQuestsByType(query: String, type: String) =
        withContext(Dispatchers.IO) {
            return@withContext amberDao.getQuests(query = query, type = type)
        }

    override suspend fun fetchQuest(id: String) = withContext(Dispatchers.IO) {
        val apiResponse = apiService.getQuest(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!,id = id)
        if (apiResponse.isSuccessful && apiResponse.body() != null) {
            val quest = apiResponse.body()!!.data
            quest.id = id
            Log.v("test2", quest.toString())
            amberDao.insertQuest(quest)
        }
    }

    override suspend fun getQuest(id: String) = withContext(Dispatchers.IO) {
        if (!amberDao.doesQuestDetailRowExist(id)) {
            fetchQuest(id)
        }
        return@withContext amberDao.getQuestDetail(id)
    }

    override suspend fun getElements() = withContext(Dispatchers.IO) {
        val apiResponse = apiService.getElements(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!,)
        if (apiResponse.isSuccessful && apiResponse.body() != null) {
            return@withContext apiService.getElements(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!,)
        } else {
            return@withContext null
        }
    }

    override suspend fun getTCGList(query: String) = withContext(Dispatchers.IO) {
        val apiResponse = apiService.getTCGList(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!,)
        if (apiResponse.isSuccessful && apiResponse.body() != null) {
            if (apiResponse.body()!!.data.items.size != amberDao.getTCGListCount()) {
                val tcgSummaries = apiResponse.body()!!.data.items
                amberDao.insertTCGSummaries(tcgSummaries.values.toList())
            }
        }
        return@withContext amberDao.getTCGList(query)
    }

    override suspend fun getTCGList(ids: List<String>) = withContext(Dispatchers.IO) {
        val apiResponse = apiService.getTCGList(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!,)
        if (apiResponse.isSuccessful && apiResponse.body() != null) {
            if (apiResponse.body()!!.data.items.size != amberDao.getTCGListCount()) {
                val tcgSummaries = apiResponse.body()!!.data.items
                amberDao.insertTCGSummaries(tcgSummaries.values.toList())
            }
        }
        return@withContext amberDao.getTCGList(ids)
    }

    override suspend fun fetchTCG(id: String) = withContext(Dispatchers.IO) {
        val apiResponse = apiService.getTCG(lang = LANGUAGE_MAP[userPreferencesRepository.languageFlow]!!, id = id)
        if (apiResponse.isSuccessful && apiResponse.body() != null) {
            amberDao.insertTCG(apiResponse.body()!!.data)
        }
    }

    override suspend fun getTCG(id: String) = withContext(Dispatchers.IO) {
        if (!amberDao.doesTCGDetailRowExist(id)) {
            fetchTCG(id)
        }
        return@withContext amberDao.getTCGDetail(id)
    }
}