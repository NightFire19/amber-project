package com.xuan.projectamber.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.xuan.projectamber.ui.screens.CharacterDetailsScreen
import com.xuan.projectamber.ui.screens.settings.SettingsScreen
import com.xuan.projectamber.ui.screens.bookDetails.BookDetailsScreen
import com.xuan.projectamber.ui.screens.reliquaryDetails.ReliquaryDetailsScreen
import com.xuan.projectamber.ui.screens.categories.CategoriesScreen
import com.xuan.projectamber.ui.screens.categorysearch.CategorySearchScreen
import com.xuan.projectamber.ui.screens.changelog.ChangelogScreen
import com.xuan.projectamber.ui.screens.elements.ElementsScreen
import com.xuan.projectamber.ui.screens.exception.ExceptionScreen
import com.xuan.projectamber.ui.screens.home.HomeScreen
import com.xuan.projectamber.ui.screens.materialDetails.MaterialDetailsScreen
import com.xuan.projectamber.ui.screens.monsterDetails.MonsterDetailsScreen
import com.xuan.projectamber.ui.screens.search.SearchScreen
import com.xuan.projectamber.ui.screens.weaponDetails.WeaponDetailsScreen
import com.xuan.projectamber.ui.screens.foodDetails.FoodDetailsScreen
import com.xuan.projectamber.ui.screens.funishingDetails.FurnishingDetailsScreen
import com.xuan.projectamber.ui.screens.namecardDetails.NamecardDetailsScreen
import com.xuan.projectamber.ui.screens.privacypolicy.PrivacyPolicyScreen
import com.xuan.projectamber.ui.screens.questDetails.QuestDetailsScreen
import com.xuan.projectamber.ui.screens.questList.QuestListScreen
import com.xuan.projectamber.ui.screens.tcgDetails.TCGDetailsScreen
import com.xuan.projectamber.ui.screens.tcgList.TCGListScreen

@Composable
fun NavGraph(navController: NavHostController) {
    NavHost(
        navController = navController,
        startDestination = Screens.Home.route
    ) {
        composable(route = Screens.Home.route) {
            HomeScreen(navController)
        }
        composable(route = Screens.Settings.route) {
            SettingsScreen(navController)
        }
        composable(route = Screens.Search.route) {
            SearchScreen(navController)
        }
        composable(route = Screens.Changelog.route) {
            ChangelogScreen(navController = navController)
        }
        composable(route = Screens.QuestList.route) {
            QuestListScreen(navController = navController)
        }
        composable(route = Screens.Elements.route) {
            ElementsScreen(navController = navController)
        }
        composable(route = Screens.TCGList.route) {
            TCGListScreen(navController = navController)
        }
        composable(route = Screens.PrivacyPolicy.route) {
            PrivacyPolicyScreen(navController = navController)
        }
        composable("characterDetails/{id}") { navBackStackEntry ->
            val characterId = navBackStackEntry.arguments?.getString("id")

            if (characterId != null) {
                CharacterDetailsScreen(upgradeId = characterId, navController = navController)
            }
        }
        composable("weaponDetails/{id}") { navBackStackEntry ->
            val weaponId = navBackStackEntry.arguments?.getString("id")

            if (weaponId != null) {
                WeaponDetailsScreen(upgradeId = weaponId, navController = navController)
            }
        }
        composable(route = Screens.Categories.route) {
            CategoriesScreen(navController = navController)
        }
        composable("monsterDetails/{id}") { navBackStackEntry ->
            val monsterId = navBackStackEntry.arguments?.getString("id")

            if (monsterId != null) {
                MonsterDetailsScreen(id = monsterId, navController = navController)
            }
        }
        composable("materialDetails/{id}") { navBackStackEntry ->
            val materialId = navBackStackEntry.arguments?.getString("id")

            if (materialId != null) {
                MaterialDetailsScreen(id = materialId, navController = navController)
            }
        }

        composable("categorySearch/{category}") { navBackStackEntry ->
            val category = navBackStackEntry.arguments?.getString("category")

            if (category != null) {
                CategorySearchScreen(category = category, navController = navController)
            }
        }

        composable("foodDetails/{id}") { navBackStackEntry ->
            val foodId = navBackStackEntry.arguments?.getString("id")

            if (foodId != null) {
                FoodDetailsScreen(id = foodId, navController = navController)
            }
        }

        composable("furnitureDetails/{id}") { navBackStackEntry ->
            val furnitureId = navBackStackEntry.arguments?.getString("id")

            if (furnitureId != null) {
                FurnishingDetailsScreen(id = furnitureId, navController = navController)
            }
        }

        composable("namecardDetails/{id}") { navBackStackEntry ->
            val namecardId = navBackStackEntry.arguments?.getString("id")

            if (namecardId != null) {
                NamecardDetailsScreen(id = namecardId, navController = navController)
            }
        }

        composable("artifactDetails/{id}") { navBackStackEntry ->
            val artifactId = navBackStackEntry.arguments?.getString("id")

            if (artifactId != null) {
                ReliquaryDetailsScreen(id = artifactId, navController = navController)
            }
        }

        composable("bookDetails/{id}") { navBackStackEntry ->
            val bookId = navBackStackEntry.arguments?.getString("id")

            if (bookId != null) {
                BookDetailsScreen(id = bookId, navController = navController)
            }
        }

        composable("questDetails/{id}") { navBackStackEntry ->
            val questId = navBackStackEntry.arguments?.getString("id")
            
            if (questId != null) {
                QuestDetailsScreen(id = questId, navController = navController)
            }
        }

        composable("tcgDetails/{id}") { navBackStackEntry ->
            val tcgId = navBackStackEntry.arguments?.getString("id")

            if (tcgId != null) {
                TCGDetailsScreen(id = tcgId, navController = navController)
            }
        }

        composable("exception/{message}") { navBackStackEntry ->
            val message = navBackStackEntry.arguments?.getString("message")

            if (message != null) {
                ExceptionScreen(navController = navController, exceptionInfo = message)
            }
        }
    }
}