package com.xuan.projectamber.navigation

sealed class Screens (val route: String) {
    object Home: Screens("home_screen")
    object Settings: Screens("settings_screen")
    object Search: Screens("search_screen")
    object Categories: Screens("categories_screen")
    object Changelog: Screens("changelog_screen")
    object QuestList: Screens("quest_list_screen")
    object Elements: Screens("elements_screen")
    object TCGList: Screens("tcg_screen")
    object PrivacyPolicy: Screens("privacy_policy_screen")
}