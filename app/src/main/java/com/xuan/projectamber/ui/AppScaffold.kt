package com.xuan.projectamber.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.viewinterop.AndroidView
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.compose.rememberNavController
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.AdView
import com.xuan.projectamber.navigation.NavGraph
import com.xuan.projectamber.ui.common.BottomNavItem
import com.xuan.projectamber.viewmodel.MainViewModel
import com.xuan.projectamber.R
import com.xuan.projectamber.ui.theme.ScreenBackGround


@Composable
fun AppScaffold(
    viewModel: MainViewModel = hiltViewModel()
) {
    val navController = rememberNavController()
    val width = LocalConfiguration.current.screenWidthDp

    Scaffold(
        topBar = {
            Row(
                modifier = Modifier.fillMaxWidth().background(color = ScreenBackGround),
                horizontalArrangement = Arrangement.Center
            ) {
                val adId = stringResource(id = R.string.TopBannerAdId)
                AndroidView(
                    modifier = Modifier.fillMaxWidth(),
                    factory = { context ->
                        AdView(context).apply {
                            setAdSize(
                                AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(
                                    context,
                                    width
                                )
                            )
                            adUnitId = adId
                            loadAd(AdRequest.Builder().build())
                        }
                    }
                )
            }
        },
        bottomBar = {
            BottomNavItem(navController = navController)
        }
    ) { innerPadding ->
        Box(modifier = Modifier.padding(innerPadding)) {
            NavGraph(navController = navController)
        }
    }
}