package com.xuan.projectamber.ui.common

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.xuan.projectamber.R
import com.xuan.projectamber.ui.theme.SecondaryBg
import com.xuan.projectamber.ui.theme.TextIconColor

@Composable
fun AmberTextField (
    value: String,
    onValueChange: (String) -> Unit
){
    TextField(
        value = value,
        onValueChange = { newEntry ->
            onValueChange(newEntry)
        },
        shape = RoundedCornerShape(32.dp),
        colors = TextFieldDefaults.colors(
            focusedContainerColor = SecondaryBg,
            unfocusedContainerColor = SecondaryBg,
            disabledContainerColor = SecondaryBg,
            cursorColor = TextIconColor,
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
        ),
        placeholder = {
            Text(
                text = stringResource(id = R.string.AmberTextFieldPlaceholder),
                fontWeight = FontWeight.ExtraLight
            )
        },
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth()
    )
}