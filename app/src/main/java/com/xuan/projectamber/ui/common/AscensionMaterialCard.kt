package com.xuan.projectamber.ui.common

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import coil.compose.AsyncImagePainter
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.size.Size
import com.xuan.projectamber.ui.theme.DarkGrayTextColor
import com.xuan.projectamber.ui.theme.NavBackGround
import com.xuan.projectamber.ui.theme.TextIconColor

@Composable
fun AscensionMaterialCard(
    itemId: String,
    quantity: Int,
    navController: NavController,
) {
    val materialUrl = "https://api.ambr.top/assets/UI/UI_ItemIcon_$itemId.png"
    Card(
        modifier = Modifier
            .padding(4.dp)
            .width(54.dp)
            .clickable {
                navController.navigate(route = "materialDetails/${itemId}")
            },
        colors = CardDefaults.cardColors(containerColor = NavBackGround),
        shape = RoundedCornerShape(4.dp)
    ) {
        Column{
            val painter = rememberAsyncImagePainter(
                ImageRequest.Builder(
                    LocalContext.current
                )
                    .data(materialUrl)
                    .apply(
                        block = fun ImageRequest.Builder.() {
                            size(Size.ORIGINAL)
                        }
                    )
                    .build()
            )
            val painterState = painter.state

            Box {
                Image(
                    painter = painter,
                    contentDescription = null,
                    modifier = Modifier.size(54.dp)
                )
                if (painterState is AsyncImagePainter.State.Loading) {
                    CircularProgressIndicator(
                        modifier = Modifier.padding(8.dp),
                        color = TextIconColor
                    )
                }
            }
            var quantityDisplayed = quantity.toString()
            if (quantityDisplayed.length >= 6) {
                quantityDisplayed = "${ quantity / 1000 }K"
            }
            Text(
                text = quantityDisplayed,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .background(TextIconColor)
                    .fillMaxWidth(),
                color = DarkGrayTextColor,
                fontWeight = FontWeight.SemiBold,
            )
        }
    }
}