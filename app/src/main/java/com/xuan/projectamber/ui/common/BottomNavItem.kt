package com.xuan.projectamber.ui.common

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.xuan.projectamber.navigation.Screens
import com.xuan.projectamber.ui.theme.BorderColor
import com.xuan.projectamber.ui.theme.NavBackGround
import com.xuan.projectamber.ui.theme.TextIconColor

@Composable
fun BottomNavItem (
    navController: NavController
)
{
    Column {
        Divider(color = BorderColor)
        Row(
            modifier = Modifier
                .background(NavBackGround)
                .fillMaxWidth()
                .padding(2.dp),
            horizontalArrangement = Arrangement.SpaceAround
        ) {
            IconButton(
                onClick = {
                    navController.navigate(Screens.Home.route)
                }
            ) {
                Icon(
                    imageVector = Icons.Filled.Home,
                    contentDescription = "",
                    tint = TextIconColor
                )
            }
            IconButton(
                onClick = {
                    navController.navigate(Screens.Categories.route)
                }
            ) {
                Icon(
                    imageVector = Icons.Filled.Menu,
                    contentDescription = "",
                    tint = TextIconColor
                )
            }
            IconButton(
                onClick = {
                    navController.navigate(Screens.Search.route)
                }
            ) {
                Icon(
                    imageVector = Icons.Filled.Search,
                    contentDescription = "",
                    tint = TextIconColor
                )
            }
            IconButton(onClick = { navController.navigate(Screens.Settings.route) }) {
                Icon(
                    imageVector = Icons.Filled.Settings,
                    contentDescription = "",
                    tint = TextIconColor
                )
            }
        }
    }
}
