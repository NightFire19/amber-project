package com.xuan.projectamber.ui.common

import androidx.compose.material3.SliderColors
import androidx.compose.material3.SliderDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import com.xuan.projectamber.ui.theme.NavBackGround
import com.xuan.projectamber.ui.theme.SliderThumb
import com.xuan.projectamber.ui.theme.SliderTrack

@Composable
fun CustomSliderColors(): SliderColors = SliderDefaults.colors(
    activeTickColor = Color.Transparent,
    inactiveTickColor = Color.Transparent,
    thumbColor = SliderThumb,
    activeTrackColor = SliderTrack,
    inactiveTrackColor = NavBackGround,
)