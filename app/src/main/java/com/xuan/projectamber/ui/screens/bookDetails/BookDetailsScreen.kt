package com.xuan.projectamber.ui.screens.bookDetails

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.filled.ArrowDropUp
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.xuan.projectamber.ui.theme.*
import com.xuan.projectamber.viewmodel.BookDetailsViewModel

@Composable
fun BookDetailsScreen(
    id: String,
    navController: NavController,
    viewModel: BookDetailsViewModel = hiltViewModel()
) {
    viewModel.getBook(id)
    val book = viewModel.book
    val readables = viewModel.volumes
    val dropDownVisibility = viewModel.dropDownVisibility
    val currentReadableTitle = viewModel.currentReadableTitle
    val currentReadableDescription = viewModel.currentReadableDescription
    val currentReadableBody = viewModel.currentReadableBody


    Surface(
        color = ScreenBackGround,
        modifier = Modifier
            .fillMaxSize()
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .verticalScroll(
                    rememberScrollState(),
                )
                .padding(4.dp),
        ) {
            Text(
                text = book.name,
                color = TextIconColor,
                fontWeight = FontWeight.Bold,
                fontSize = 24.sp,
                modifier = Modifier.padding(8.dp)
            )
            if (readables.size > 1) {
                Box (
                    modifier = Modifier
                        .padding(8.dp)
                        .clip(RoundedCornerShape(8.dp))
                        .background(TextIconColor)
                        .fillMaxWidth()
                        .clickable {
                            viewModel.onDropdownMenuClicked()
                        }
                ) {
                    Row (
                        horizontalArrangement = Arrangement.SpaceBetween,
                        modifier = Modifier.fillMaxWidth(),
                        verticalAlignment = Alignment.CenterVertically
                            ) {
                        Text(
                            text = currentReadableTitle,
                            color = SecondaryBg,
                            textAlign = TextAlign.Center,
                            fontWeight = FontWeight.Bold,
                            modifier = Modifier
                                .padding(4.dp),
                        )
                        if (dropDownVisibility) {
                            Icon(
                                imageVector = Icons.Filled.ArrowDropUp,
                                contentDescription = "",
                                tint = SecondaryBg
                            )
                        } else {
                            Icon(
                                imageVector = Icons.Filled.ArrowDropDown,
                                contentDescription = "",
                                tint = SecondaryBg
                            )
                        }
                    }
                    DropdownMenu(
                        expanded = dropDownVisibility,
                        onDismissRequest = { viewModel.onDropdownDismissRequest() },
                        modifier = Modifier
                            .background(
                                TextIconColor
                            )
                    ) {
                        book.volume?.forEachIndexed { index, volumeEntity ->
                            DropdownMenuItem(
                                text = {
                                    Text(
                                        text = volumeEntity.name,
                                        color = SecondaryBg
                                    )
                                },
                                onClick = {
                                    viewModel.onDropdownMenuItemClick(index)
                                }
                            )
                        }
                    }
                }
            } else if (readables.size == 1) {
                Text(
                    text = currentReadableTitle,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier.padding(8.dp)
                )
            }
            TextCard(text = currentReadableDescription)
            TextCard(text = currentReadableBody)
        }
    }
}

@Composable
fun TextCard(text: String) {
    Card(
        modifier = Modifier.fillMaxWidth().padding(8.dp),
        colors = CardDefaults.cardColors(containerColor = SecondaryBg),
        border = BorderStroke(2.dp, BorderColor),
        shape = RoundedCornerShape(8.dp)
    ) {
        Text(
            modifier = Modifier.padding(8.dp),
            text = text.replace("\\n","\n")
        )
    }
}