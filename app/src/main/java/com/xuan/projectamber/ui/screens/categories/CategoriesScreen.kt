package com.xuan.projectamber.ui.screens.categories

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material.icons.outlined.Backpack
import androidx.compose.material.icons.outlined.Contacts
import androidx.compose.material.icons.outlined.FormatListBulleted
import androidx.compose.material.icons.outlined.TravelExplore
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.xuan.projectamber.R
import com.xuan.projectamber.navigation.Screens
import com.xuan.projectamber.ui.common.AutoResizedText
import com.xuan.projectamber.ui.theme.BorderColor
import com.xuan.projectamber.ui.theme.NavBackGround
import com.xuan.projectamber.ui.theme.TextIconColor


@Composable
fun CategoriesScreen(
    navController: NavController
) {
    Surface(
        color = NavBackGround,
        modifier = Modifier
            .fillMaxSize()
    ) {
        Column {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(NavBackGround)
                    .padding(8.dp),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically

            ) {

                Text(
                    text = stringResource(id = R.string.CategoriesHeader),
                    fontWeight = FontWeight.Bold,
                    fontSize = 24.sp,
                )
                OutlinedButton(
                    onClick = {
                        navController.popBackStack()
                    },
                    modifier = Modifier
                        .size(48.dp),
                    shape = CircleShape,
                    border = BorderStroke(2.dp, Color.Gray),
                    contentPadding = PaddingValues(8.dp),  //avoid the little icon
                    colors = ButtonDefaults.outlinedButtonColors(
                        containerColor = TextIconColor,
                        contentColor = Color.Black
                    )
                ) {
                    Icon(
                        imageVector = Icons.Filled.Close,
                        contentDescription = "close",
                    )
                }
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .verticalScroll(rememberScrollState()),
            ) {
                Column(
                    modifier = Modifier
                        .padding(2.dp)
                        .weight(1f)
                ) {
                    CategoriesCard(
                        text = stringResource(id = R.string.CategoriesCharactersLabel),
                        icon = Icons.Filled.Person,
                        navController = navController,
                        navRoute = "categorySearch/avatar"
                    )
                    CategoriesCard(
                        text = stringResource(id = R.string.CategoriesFoodLabel),
                        icon = Icons.Filled.Fastfood,
                        navController = navController,
                        navRoute = "categorySearch/food"
                    )
                    CategoriesCard(
                        text = stringResource(id = R.string.CategoriesMaterialsLabel),
                        icon = Icons.Outlined.Backpack,
                        navController = navController,
                        navRoute = "categorySearch/material"
                    )
                    CategoriesCard(
                        text = stringResource(id = R.string.CategoriesArtifactsLabel),
                        icon = Icons.Filled.AccountBalance,
                        navController = navController,
                        navRoute = "categorySearch/reliquary"
                    )
                    CategoriesCard(
                        text = stringResource(id = R.string.CategoriesNamecardsLabel),
                        icon = Icons.Outlined.Contacts,
                        navController = navController,
                        navRoute = "categorySearch/namecard"
                    )
                    CategoriesCard(
                        text = stringResource(id = R.string.CategoriesBooksLabel),
                        icon = Icons.Filled.MenuBook,
                        navController = navController,
                        navRoute = "categorySearch/book"
                    )
                    CategoriesCard(
                        text = stringResource(id = R.string.CategoriesTCGLabel),
                        icon = R.drawable.tcg,
                        navController = navController,
                        navRoute = Screens.TCGList.route
                    )
                }
                Column(
                    modifier = Modifier
                        .padding(2.dp)
                        .weight(1f)
                ) {
                    CategoriesCard(
                        text = stringResource(id = R.string.CategoriesWeaponsLabel),
                        icon = R.drawable.swords,
                        navController = navController,
                        navRoute = "categorySearch/weapon"
                    )
                    CategoriesCard(
                        text = stringResource(id = R.string.CategoriesChangelogLabel),
                        icon = Icons.Outlined.FormatListBulleted,
                        navController = navController,
                        navRoute = Screens.Changelog.route
                    )
                    CategoriesCard(
                        text = stringResource(id = R.string.CategoriesFurnishingsLabel),
                        icon = R.drawable.sereniteapot,
                        navController = navController,
                        navRoute = "categorySearch/furniture"
                    )
                    CategoriesCard(
                        text = stringResource(id = R.string.CategoriesLivingBeingsLabel),
                        icon = Icons.Filled.Pets,
                        navController = navController,
                        navRoute = "categorySearch/monster"
                    )
                    CategoriesCard(
                        text = stringResource(id = R.string.CategoriesElementsLabel),
                        icon = R.drawable.explosion,
                        navController = navController,
                        navRoute = Screens.Elements.route
                    )
                    CategoriesCard(
                        text = stringResource(id = R.string.CategoriesTravelLogLabel),
                        icon = Icons.Outlined.TravelExplore,
                        navController = navController,
                        navRoute = Screens.QuestList.route
                    )
                }

            }
        }

    }
}

@Composable
fun CategoriesCard(
    text: String,
    icon: ImageVector,
    navController: NavController,
    navRoute: String,
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(4.dp)
            .clickable {
                if (navRoute != "") {
                    navController.navigate(
                        navRoute
                    )
                }
            },
        colors = CardDefaults.cardColors(containerColor = BorderColor),
    ) {
        Row(
            modifier = Modifier
                .padding(4.dp)
                .fillMaxWidth()
                .height(36.dp),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Icon(
                imageVector = icon,
                modifier = Modifier
                    .padding(end = 2.dp),
                contentDescription = "",
                tint = TextIconColor,
            )
            AutoResizedText(
                modifier = Modifier
                    .padding(2.dp)
                    .fillMaxWidth(),
                fontWeight = FontWeight.Bold,
                color = TextIconColor,
                text = text,
            )
        }
    }
}

@Composable
fun CategoriesCard(
    text: String,
    icon: Int,
    navController: NavController,
    navRoute: String,
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(4.dp)
            .clickable {
                if (navRoute != "") {
                    navController.navigate(
                        navRoute
                    )
                }
            },
        colors = CardDefaults.cardColors(containerColor = BorderColor),
    ) {
        Row(
            modifier = Modifier
                .padding(4.dp)
                .fillMaxWidth()
                .height(36.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Icon(
                painter = painterResource(id = icon),
                modifier = Modifier
                    .size(24.dp)
                    .padding(horizontal = 4.dp),
                contentDescription = "",
                tint = TextIconColor,
            )
            AutoResizedText(
                modifier = Modifier
                    .padding(2.dp)
                    .fillMaxWidth(),
                fontWeight = FontWeight.Bold,
                text = text,
                color = TextIconColor,
            )
        }
    }
}