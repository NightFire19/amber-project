package com.xuan.projectamber.ui.screens.categorysearch

import android.util.Log
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.rounded.ExpandMore
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.AsyncImagePainter
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.size.Size
import com.xuan.projectamber.R
import com.xuan.projectamber.domain.models.CategorySearchResultEntity
import com.xuan.projectamber.ui.common.AmberTextField
import com.xuan.projectamber.ui.theme.*
import com.xuan.projectamber.util.Constants.TYPE
import com.xuan.projectamber.viewmodel.CategorySearchViewModel
import com.xuan.projectamber.ui.common.AutoResizedText
import com.xuan.projectamber.util.Constants
import com.xuan.projectamber.util.Constants.CATEGORY_STRING_MAP
import com.xuan.projectamber.util.Constants.ERROR_IMG_URL

@Composable
fun CategorySearchScreen(
    category: String,
    navController: NavController,
    viewModel: CategorySearchViewModel = hiltViewModel()
) {
    LaunchedEffect(Unit) {
        viewModel.fetchResults(category)
    }
    val showCard = viewModel.showExpandCard
    val searchResults = viewModel.searchResults
    val query = viewModel.query

    Surface(
        color = NavBackGround,
        modifier = Modifier
            .fillMaxSize()
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .verticalScroll(rememberScrollState()),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(NavBackGround),
                verticalAlignment = Alignment.CenterVertically

            ) {
                IconButton(
                    onClick = { navController.popBackStack() },
                    modifier = Modifier.padding(end = 8.dp)
                ) {
                    Icon(
                        imageVector = Icons.Filled.ArrowBack,
                        contentDescription = "",
                        tint = TextIconColor
                    )
                }
                Text(
                    text = stringResource(id = R.string.CategorySearchHeader),
                    fontWeight = FontWeight.Bold,
                )
                Text(
                    text = stringResource(id = CATEGORY_STRING_MAP[category] ?: 0),
                    fontWeight = FontWeight.Bold,
                )
            }
            AmberTextField(
                value = query,
            ) { newEntry ->
                viewModel.onSearchEntryValueChange(newQuery = newEntry)
            }
            if (searchResults.isEmpty() && query == "") {
                Column(
                    modifier = Modifier
                        .background(NavBackGround)
                        .weight(1f)
                        .fillMaxWidth()
                        .clip(RoundedCornerShape(4.dp)),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally,
                ) {
                    CircularProgressIndicator(
                        modifier = Modifier.size(48.dp),
                        color = TextIconColor,
                    )
                }
            } else {
                LazyVerticalGrid(
                    modifier = Modifier
                        .background(NavBackGround)
                        .padding(top = 8.dp)
                        .weight(1f)
                        .clip(RoundedCornerShape(4.dp)),
                    columns = GridCells.Adaptive(100.dp),
                ) {
                    items(
                        count = viewModel.searchResults.size,
                        key = {
                            viewModel.searchResults[it].id
                        }
                    ) { index ->
                        ResultCard(
                            viewModel = viewModel,
                            result = viewModel.searchResults[index],
                            navController = navController
                        )
                    }
                    item {
                        if (showCard) {
                            LoadCard { viewModel.onExpandClicked() }
                        }
                    }
                }
            }
        }
    }
}

@Composable
private fun ResultCard(
    viewModel: CategorySearchViewModel,
    result: CategorySearchResultEntity,
    navController: NavController,
) {
    var iconUrl = "https://api.ambr.top/assets/UI/"

    when (result.category) {
        "monster" -> iconUrl += "monster/"
        "furniture" -> iconUrl += "furniture/"
        "reliquary" -> iconUrl += "reliquary/"
        "namecard" -> iconUrl += "namecard/"
    }

    iconUrl += result.icon + ".png"
    var buffUrl = 0

    if (result.buffIcon != "") {
        buffUrl = TYPE[result.buffIcon]!!
    }

    Card(
        modifier = Modifier
            .padding(4.dp)
            .clickable {
                Log.v("test2", viewModel.getRoute(result))
                if (viewModel.getRoute(result) != "") {
                    navController.navigate(viewModel.getRoute(result))
                }
            },
        colors = CardDefaults.cardColors(containerColor = NavBackGround),
        shape = RoundedCornerShape(4.dp)
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(
                    brush = Brush.horizontalGradient(
                        colors = Constants.RANK_COLORS[result.rank.toString()] ?: listOf(
                            SecondaryBg,
                            SecondaryBg
                        )
                    )
                ),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            Box {
                val painter = rememberAsyncImagePainter(
                    ImageRequest.Builder(
                        LocalContext.current
                    )
                        .data(iconUrl)
                        .apply(
                            block = fun ImageRequest.Builder.() {
                                size(Size.ORIGINAL)
                            }
                        )
                        .build()
                )
                val imageState = painter.state

                Image(
                    painter = painter,
                    contentScale = ContentScale.FillWidth,
                    contentDescription = null,
                    modifier = Modifier
                        .size(100.dp)
                )

                if (imageState is AsyncImagePainter.State.Loading) {
                    CircularProgressIndicator(
                        modifier = Modifier
                            .padding(32.dp)
                            .fillMaxWidth(),
                        color = TextIconColor
                    )
                }

                if (imageState is AsyncImagePainter.State.Error) {
                    val errorPainter = rememberAsyncImagePainter(
                        ImageRequest.Builder(
                            LocalContext.current
                        )
                            .data(ERROR_IMG_URL)
                            .apply(
                                block = fun ImageRequest.Builder.() {
                                    size(Size.ORIGINAL)
                                }
                            )
                            .build()
                    )
                    Image(
                        painter = errorPainter,
                        contentScale = ContentScale.FillWidth,
                        contentDescription = null,
                        modifier = Modifier
                            .size(100.dp)
                            .background(
                                color = NavBackGround
                            )
                    )
                }

                Column(
                    verticalArrangement = Arrangement.SpaceBetween,
                    modifier = Modifier.matchParentSize()
                ) {
                    if (buffUrl != 0) {
                        Row(modifier = Modifier.fillMaxWidth()) {
                            Image(
                                painter = painterResource(
                                    id = buffUrl
                                ),
                                contentScale = ContentScale.FillWidth,
                                contentDescription = null,
                                modifier = Modifier
                                    .weight(2f)
                                    .padding(2.dp)
                            )

                            Spacer(modifier = Modifier.weight(5f))
                        }
                    } else {
                        Spacer(modifier = Modifier.size(2.dp))
                    }
                    LazyRow(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.Center
                    ) {
                        result.rank?.let { it1 ->
                            items(it1) {
                                Image(
                                    painter = painterResource(
                                        id = R.drawable.star
                                    ),
                                    contentDescription = "Star",
                                    modifier = Modifier
                                        .size(12.dp),
                                )
                            }
                        }
                    }
                }
            }
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(36.dp)
                    .background(color = TextIconColor),
                verticalArrangement = Arrangement.Center,
            ) {
                AutoResizedText(
                    text = result.name!!,
                    modifier = Modifier
                        .padding(1.dp)
                        .fillMaxWidth(),
                    textAlign = TextAlign.Center,
                    fontWeight = FontWeight.SemiBold,
                    color = ScreenBackGround,
                )
            }
        }
    }
}

@Composable
private fun LoadCard(onExpandClicked: () -> Unit) {
    Card(
        modifier = Modifier
            .height(148.dp)
            .padding(4.dp)
            .clickable {
                onExpandClicked()
            },
        colors = CardDefaults.cardColors(containerColor = NavBackGround),
        border = BorderStroke(2.dp, Color.Gray),
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Text(
                text = stringResource(id = R.string.LoadCardText),
                textAlign = TextAlign.Center
            )
            Icon(
                imageVector = Icons.Rounded.ExpandMore,
                contentDescription = "close",
            )
        }
    }
}