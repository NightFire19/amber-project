package com.xuan.projectamber.ui.screens.changelog

import android.util.Log
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.filled.ArrowDropUp
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.AsyncImagePainter
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.size.Size
import com.xuan.projectamber.R
import com.xuan.projectamber.domain.models.CategorySearchResultEntity
import com.xuan.projectamber.ui.common.AutoResizedText
import com.xuan.projectamber.ui.screens.tcgList.TCGCard
import com.xuan.projectamber.ui.theme.NavBackGround
import com.xuan.projectamber.ui.theme.ScreenBackGround
import com.xuan.projectamber.ui.theme.SecondaryBg
import com.xuan.projectamber.ui.theme.TextIconColor
import com.xuan.projectamber.util.Constants
import com.xuan.projectamber.viewmodel.ChangelogViewModel

@OptIn(ExperimentalLayoutApi::class)
@Composable
fun ChangelogScreen(
    navController: NavController,
    viewModel: ChangelogViewModel = hiltViewModel()
) {
    val changelog = viewModel.changeLogs
    val currentChangelog = viewModel.currentChangelog
    val avatars = viewModel.avatar
    val weapons = viewModel.weapon
    val materials = viewModel.material
    val food = viewModel.food
    val namecards = viewModel.namecard
    val furnishings = viewModel.furnishing
    val reliquaries = viewModel.reliquary
    val monster = viewModel.monster
    val gcg = viewModel.gcg
    val configuration = LocalConfiguration.current
    val width = configuration.screenWidthDp / 5
    val dropDownVisibility = viewModel.dropDownVisibility

    Surface(
        color = ScreenBackGround,
        modifier = Modifier
            .fillMaxSize()
    ) {
        Column(
            modifier = Modifier
                .padding(8.dp)
                .verticalScroll(rememberScrollState())
                .fillMaxSize()

        ) {
            Text(
                text = stringResource(R.string.ChangelogHeader),
                fontWeight = FontWeight.Bold,
                fontSize = 24.sp,
            )

            Box(
                modifier = Modifier
                    .padding(8.dp)
                    .clip(RoundedCornerShape(8.dp))
                    .background(TextIconColor)
                    .clickable {
                        viewModel.onDropdownMenuClicked()
                    }
            ) {
                Row(
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        text = stringResource(id = R.string.ChangelogVersionLabel, currentChangelog.version),
                        color = SecondaryBg,
                        textAlign = TextAlign.Center,
                        fontWeight = FontWeight.Bold,
                        modifier = Modifier
                            .padding(vertical = 4.dp, horizontal = 16.dp),
                    )
                    if (dropDownVisibility) {
                        Icon(
                            imageVector = Icons.Filled.ArrowDropUp,
                            contentDescription = "",
                            tint = SecondaryBg
                        )
                    } else {
                        Icon(
                            imageVector = Icons.Filled.ArrowDropDown,
                            contentDescription = "",
                            tint = SecondaryBg
                        )
                    }
                }
                DropdownMenu(
                    expanded = dropDownVisibility,
                    onDismissRequest = { viewModel.onDropdownDismissRequest() },
                    modifier = Modifier
                        .background(
                            TextIconColor
                        )
                ) {
                    changelog.forEachIndexed { index, changelogEntity ->
                        DropdownMenuItem(
                            text = {
                                Text(
                                    text = stringResource(id = R.string.ChangelogVersionLabel, changelogEntity.version),
                                    color = SecondaryBg
                                )
                            },
                            onClick = {
                                viewModel.onDropdownMenuItemClick(index)
                            }
                        )
                    }
                }
            }

            ChangelogCategory(
                viewModel = viewModel,
                navController = navController,
                categoryTitle = stringResource(R.string.CategoriesCharactersLabel),
                categoryList = avatars,
                width = width
            )

            ChangelogCategory(
                viewModel = viewModel,
                navController = navController,
                categoryTitle = stringResource(R.string.CategoriesWeaponsLabel),
                categoryList = weapons,
                width = width
            )

            ChangelogCategory(
                viewModel = viewModel,
                navController = navController,
                categoryTitle = stringResource(R.string.CategoriesMaterialsLabel),
                categoryList = materials,
                width = width
            )
            ChangelogCategory(
                viewModel = viewModel,
                navController = navController,
                categoryTitle = stringResource(R.string.CategoriesFoodLabel),
                categoryList = food,
                width = width
            )
            ChangelogCategory(
                viewModel = viewModel,
                navController = navController,
                categoryTitle = stringResource(R.string.CategoriesNamecardsLabel),
                categoryList = namecards,
                width = width
            )
            ChangelogCategory(
                viewModel = viewModel,
                navController = navController,
                categoryTitle = stringResource(R.string.CategoriesFurnishingsLabel),
                categoryList = furnishings,
                width = width
            )
            ChangelogCategory(
                viewModel = viewModel,
                navController = navController,
                categoryTitle = stringResource(R.string.CategoriesArtifactsLabel),
                categoryList = reliquaries,
                width = width
            )
            ChangelogCategory(
                viewModel = viewModel,
                navController = navController,
                categoryTitle = stringResource(R.string.CategoriesLivingBeingsLabel),
                categoryList = monster,
                width = width
            )
            if (!gcg.isNullOrEmpty()) {
                Text(
                    text = stringResource(R.string.CategoriesTCGLabel),
                    fontWeight = FontWeight.Bold,
                    fontSize = 24.sp,
                )
                FlowRow(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.Center
                ) {
                    gcg.forEach { tcgSummaryEntity ->
                        Box(
                            modifier = Modifier.width(128.dp)
                        ) {
                            TCGCard(
                                tcgSummaryEntity = tcgSummaryEntity,
                                navController = navController
                            )
                        }
                    }
                }
            }
        }
    }
}

//
@OptIn(ExperimentalLayoutApi::class)
@Composable
fun ChangelogCategory(
    viewModel: ChangelogViewModel,
    navController: NavController,
    categoryTitle: String,
    categoryList: List<CategorySearchResultEntity>,
    width: Int,
) {
    if (categoryList.isNotEmpty()) {
        Text(
            text = categoryTitle,
            fontWeight = FontWeight.Bold,
            fontSize = 24.sp,
        )
        FlowRow(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Center
        ) {
            categoryList.forEach { result ->
                Box(
                    modifier = Modifier.width(width.dp)
                ) {
                    ResultCard(
                        viewModel = viewModel,
                        result = result,
                        navController = navController
                    )
                }
            }
        }
    }
}

@Composable
private fun ResultCard(
    viewModel: ChangelogViewModel,
    result: CategorySearchResultEntity,
    navController: NavController,
) {
    var iconUrl = "https://api.ambr.top/assets/UI/"

    when (result.category) {
        "monster" -> iconUrl += "monster/"
        "furniture" -> iconUrl += "furniture/"
        "reliquary" -> iconUrl += "reliquary/"
        "namecard" -> iconUrl += "namecard/"
    }

    iconUrl += result.icon + ".png"
    var buffUrl = 0

    if (result.buffIcon != "") {
        buffUrl = Constants.TYPE[result.buffIcon]!!
    }

    Card(
        modifier = Modifier
            .padding(4.dp)
            .clickable {
                navController.navigate(
                    viewModel.getRoute(result)
                )
            },
        colors = CardDefaults.cardColors(containerColor = NavBackGround),
        shape = RoundedCornerShape(4.dp)
    ) {
        Column {
            Box {
                Spacer(modifier = Modifier.matchParentSize())
                val painter = rememberAsyncImagePainter(
                    ImageRequest.Builder(
                        LocalContext.current
                    )
                        .data(iconUrl)
                        .apply(
                            block = fun ImageRequest.Builder.() {
                                size(Size.ORIGINAL)
                            }
                        )
                        .build()
                )
                val imageState = painter.state

                if (imageState is AsyncImagePainter.State.Loading) {
                    CircularProgressIndicator(
                        modifier = Modifier
                            .padding(32.dp)
                            .fillMaxWidth(),
                        color = SecondaryBg
                    )
                }

                if (imageState is AsyncImagePainter.State.Error) {
                    val errorPainter = rememberAsyncImagePainter(
                        ImageRequest.Builder(
                            LocalContext.current
                        )
                            .data(Constants.ERROR_IMG_URL)
                            .apply(
                                block = fun ImageRequest.Builder.() {
                                    size(Size.ORIGINAL)
                                }
                            )
                            .build()
                    )
                    Image(
                        painter = errorPainter,
                        contentScale = ContentScale.FillWidth,
                        contentDescription = null,
                        modifier = Modifier
                            .fillMaxSize()
                            .background(
                                color = NavBackGround
                            )
                    )
                }

                Image(
                    painter = painter,
                    contentScale = ContentScale.FillWidth,
                    contentDescription = null,
                    modifier = Modifier
                        .fillMaxSize()
                        .background(
                            color = NavBackGround
                        )
                )
                Column(
                    verticalArrangement = Arrangement.SpaceBetween,
                    modifier = Modifier.matchParentSize()
                ) {
                    if (buffUrl != 0) {
                        Row(modifier = Modifier.fillMaxWidth()) {
                            Image(
                                painter = painterResource(
                                    id = buffUrl
                                ),
                                contentScale = ContentScale.FillWidth,
                                contentDescription = null,
                                modifier = Modifier
                                    .weight(2f)
                                    .padding(2.dp)
                            )

                            Spacer(modifier = Modifier.weight(5f))
                        }
                    } else {
                        Spacer(modifier = Modifier.size(2.dp))
                    }
                    LazyRow(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.Center
                    ) {
                        result.rank?.let { it1 ->
                            items(it1) {
                                Image(
                                    painter = painterResource(
                                        id = R.drawable.star
                                    ),
                                    contentDescription = "Star",
                                    modifier = Modifier
                                        .size(12.dp),
                                )
                            }
                        }
                    }
                }
            }
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(36.dp)
                    .background(color = TextIconColor),
                verticalArrangement = Arrangement.Center,
            ) {
                AutoResizedText(
                    text = result.name!!,
                    modifier = Modifier
                        .padding(1.dp)
                        .fillMaxWidth(),
                    textAlign = TextAlign.Center,
                    fontWeight = FontWeight.SemiBold,
                    color = ScreenBackGround,
                )
            }
        }
    }
}