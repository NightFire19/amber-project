package com.xuan.projectamber.ui.screens

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.graphics.*
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.size.Size
import com.xuan.projectamber.R
import com.xuan.projectamber.ui.screens.characterDetails.ascension.Ascension
import com.xuan.projectamber.ui.screens.characterDetails.constellation.Constellation
import com.xuan.projectamber.ui.screens.characterDetails.other.Other
import com.xuan.projectamber.ui.screens.characterDetails.profile.Profile
import com.xuan.projectamber.ui.screens.characterDetails.story.Story
import com.xuan.projectamber.ui.screens.characterDetails.talent.Talent
import com.xuan.projectamber.ui.theme.*
import com.xuan.projectamber.util.Constants
import com.xuan.projectamber.util.Constants.CHARACTER_TAB_CATEGORIES
import com.xuan.projectamber.viewmodel.CharacterDetailsViewModel

@Composable
fun CharacterDetailsScreen (
    upgradeId: String,
    navController: NavController,
    viewModel: CharacterDetailsViewModel = hiltViewModel(),
    )
{
    viewModel.getCharacter(upgradeId)
    viewModel.getCharacterStory(upgradeId)

    val character = viewModel.character.collectAsState()
    val story = viewModel.charStory.collectAsState()

    val gachaUrl = "https://api.ambr.top/assets/UI/UI_Gacha_AvatarImg_"+
            (character.value.icon?.substringAfterLast("_")) +
            ".png"
    val profileUrl = "https://api.ambr.top/assets/UI/"+
            character.value.icon +
            ".png"

    val talentUrl = "https://api.ambr.top/assets/UI/"+character.value.talent?.get("0")?.icon+".png"

    var tabCategories = CHARACTER_TAB_CATEGORIES.dropLast(n = 1)

    if (viewModel.hasOther.value) {
        tabCategories = CHARACTER_TAB_CATEGORIES
    }
    var category by remember { mutableStateOf(0) }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(color = ScreenBackGround)
            .verticalScroll(
                rememberScrollState(),
            ),

        ) {
        Surface(
            color = ScreenBackGround,
            modifier = Modifier
                .fillMaxSize()
        ) {
            Row(
                modifier = Modifier.fillMaxWidth()
            ) {
                Image(
                    painter = rememberAsyncImagePainter(
                        ImageRequest.Builder(
                            LocalContext.current
                        ).data(gachaUrl).apply(block = fun ImageRequest.Builder.() {
                            size(Size.ORIGINAL)
                        }).build()
                    ),
                    contentScale = ContentScale.FillHeight,
                    contentDescription = null,
                    modifier = Modifier
                        .fillMaxWidth()
                        .graphicsLayer { alpha = 0.99f }
                        .drawWithContent {
                            val colors = listOf(
                                Color.Black,
                                Color.Transparent
                            )
                            drawContent()
                            drawRect(
                                brush = Brush.verticalGradient(colors),
                                blendMode = BlendMode.DstIn
                            )
                        }
                )
            }
            Column() {
                Spacer(modifier = Modifier.height(200.dp))

                Card(
                    colors = CardDefaults.cardColors(containerColor = SecondaryBg),
                    border = BorderStroke(2.dp, BorderColor),
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 8.dp, horizontal = 4.dp)
                ) {
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(8.dp),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Column(
                            modifier = Modifier.padding(horizontal = 8.dp).weight(1f),
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            if (viewModel.elementPng != 0) {
                                Image(
                                    painter = painterResource(
                                        id = viewModel.elementPng),
                                    contentDescription = "Element",
                                    modifier = Modifier
                                        .padding(8.dp)
                                        .size(48.dp),
                                )
                            }

                            if (viewModel.weaponTypePng != 0) {
                                Image(
                                    painter = rememberAsyncImagePainter(
                                        ImageRequest.Builder(
                                            LocalContext.current
                                        )
                                            .data(talentUrl)
                                            .apply(
                                                block = fun ImageRequest.Builder.() {
                                                    size(Size.ORIGINAL)
                                                }
                                            )
                                            .build()
                                    ),
                                    contentDescription = null,
                                    modifier = Modifier
                                        .size(48.dp)
                                        .background(
                                            color = NavBackGround,
                                            shape = CircleShape
                                        )
                                        .border(2.dp, BorderColor, CircleShape),
                                )
                            }
                        }
                        Column(
                            modifier = Modifier.padding(horizontal = 8.dp).weight(2f),
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            character.value.name?.let { text ->
                                Text(
                                    fontWeight = FontWeight.Bold,
                                    text = text,
                                    fontSize = 24.sp,
                                    textAlign = TextAlign.Center,
                                )
                            }
                            character.value.fetterEntity?.let { fetterEntity ->
                                if (fetterEntity.title != "") {
                                    Text(
                                        text = fetterEntity.title,
                                        textAlign = TextAlign.Center,
                                    )
                                }
                            }
                            LazyRow() {
                                character.value.rank?.let { count ->
                                    items(count) {
                                        Image(
                                            painter = painterResource(
                                                id = R.drawable.star),
                                            contentDescription = "Star",
                                            modifier = Modifier
                                                .padding(
                                                    end = 4.dp,
                                                    top = 4.dp,
                                                    bottom = 8.dp
                                                )
                                                .size(24.dp),
                                        )
                                    }
                                }
                            }
                        }
                        Card(
                            modifier = Modifier
                                .padding(16.dp)
                                .weight(1f),
                            colors = CardDefaults.cardColors(containerColor = NavBackGround),
                            border = BorderStroke(2.dp, BorderColor),
                            shape = RoundedCornerShape(4.dp)
                        ) {
                            Box(
                                modifier = Modifier.background(
                                    brush = Brush.horizontalGradient(
                                        colors = Constants.RANK_COLORS[character.value.rank.toString()] ?: listOf(SecondaryBg, SecondaryBg)
                                    )
                                )
                            ) {
                                Image(
                                    painter = rememberAsyncImagePainter(
                                        ImageRequest.Builder(
                                            LocalContext.current
                                        )
                                            .data(profileUrl)
                                            .apply(
                                                block = fun ImageRequest.Builder.() {
                                                    size(Size.ORIGINAL)
                                                }
                                            )
                                            .build()
                                    ),
                                    contentDescription = null,
                                    modifier = Modifier.fillMaxSize()
                                )
                            }
                        }
                    }
                }

                Card(
                    colors = CardDefaults.cardColors(containerColor = SecondaryBg),
                    border = BorderStroke(2.dp, BorderColor),
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(8.dp)
                ) {
                    ScrollableTabRow(
                        selectedTabIndex = category,
                        edgePadding = 0.dp
                    ) {
                        tabCategories.forEachIndexed { index, title ->
                            Tab(selected = category == index,
                                onClick = {
                                    category = index
                                },
                                text = { Text(text = stringResource(title)) },
                                modifier = Modifier
                                    .background(
                                        if (category == index) {
                                            BorderColor
                                        } else {
                                            SecondaryBg
                                        }
                                    )
                            )
                        }
                    }
                }
                when(category) {
                    0 -> {
                        Profile(
                            character = character,
                            viewModel = viewModel
                        )
                    }
                    1 -> {
                        Constellation(
                            character = character,
                            viewModel = viewModel
                        )
                    }

                    2 -> {
                        Talent(
                            character = character,
                            viewModel = viewModel
                        )
                    }
                    3 -> {
                        Ascension(
                            character = character,
                            viewModel = viewModel,
                            navController = navController,
                        )
                    }
                    4 -> {
                        Story(
                            story = story,
                            viewModel = viewModel
                        )
                    }
                    5 -> {
                        Other(
                            character = character,
                            viewModel = viewModel,
                            navController = navController
                        )
                    }
                }
            }
        }
    }
}

