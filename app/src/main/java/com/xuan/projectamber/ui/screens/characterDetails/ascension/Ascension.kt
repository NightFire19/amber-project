package com.xuan.projectamber.ui.screens.characterDetails.ascension

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.size.Size
import com.xuan.projectamber.R
import com.xuan.projectamber.domain.models.CharacterEntity
import com.xuan.projectamber.domain.models.TalentEntity
import com.xuan.projectamber.ui.common.AscensionMaterialCard
import com.xuan.projectamber.ui.theme.*
import com.xuan.projectamber.util.Constants.ASCENSION_LEVELS
import com.xuan.projectamber.viewmodel.CharacterDetailsViewModel
import com.xuan.projectamber.viewmodel.TalentLevelSelector


@OptIn(ExperimentalLayoutApi::class)
@Composable
fun Ascension(
    navController: NavController,
    character: State<CharacterEntity>,
    viewModel: CharacterDetailsViewModel
) {
    val upperLevelBoundValue = viewModel.upperLevelBoundValue.collectAsState()
    val lowerLevelBoundValue = viewModel.lowerLevelBoundValue.collectAsState()
    val maxLevelIndex = viewModel.maxLevelIndex.collectAsState()
    val promoteMap = viewModel.promoteMap.collectAsState()

    Column {
        Card(
            modifier = Modifier.padding(8.dp),
            colors = CardDefaults.cardColors(containerColor = SecondaryBg),
            border = BorderStroke(2.dp, BorderColor),
        ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Center
            ) {

                FlowRow(
                    modifier = Modifier
                        .padding(4.dp)
                ) {
                    for (index in 0 until promoteMap.value.keys.size) {
                        AscensionMaterialCard(
                            navController = navController,
                            itemId = promoteMap.value.keys.sortedDescending()[index],
                            quantity = promoteMap.value[promoteMap.value.keys.sortedDescending()[index]]!!
                        )
                    }
                }
            }
        }
        Card(
            modifier = Modifier.padding(8.dp),
            colors = CardDefaults.cardColors(containerColor = SecondaryBg),
            border = BorderStroke(2.dp, BorderColor),
        ) {
            Column(
                modifier = Modifier.padding(8.dp)
            ) {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Row (
                        verticalAlignment = Alignment.CenterVertically,
                    ) {
                        Checkbox(
                            checked = viewModel.levelCheckedState.value,
                            onCheckedChange = {
                                viewModel.levelCheckedState.value = it
                                viewModel.getMaterials()
                                              },
                            colors = CheckboxDefaults.colors(
                                checkedColor = CheckedColor,
                                uncheckedColor = TextIconColor,
                                checkmarkColor = TextIconColor,
                            )
                        )
                        Text(
                            text = stringResource(R.string.CharacterDetailsAscensionLevelLabel),
                            fontWeight = FontWeight.SemiBold,
                            fontSize = 18.sp,
                        )
                    }
                    Row (
                        verticalAlignment = Alignment.CenterVertically,
                    ) {
                        Box (
                            modifier = Modifier
                                .clip(RoundedCornerShape(16.dp, 0.dp, 0.dp, 16.dp))
                                .background(TextIconColor)
                                .width(48.dp)
                                .clickable {
                                    viewModel.lowerLevelBoundVisibility.value = true
                                }
                        ) {
                            Text(
                                text = lowerLevelBoundValue.value,
                                color = SecondaryBg,
                                textAlign = TextAlign.Center,
                                fontWeight = FontWeight.Bold,
                                modifier = Modifier
                                    .fillMaxSize()
                                    .padding(4.dp),
                            )
                            DropdownMenu(
                                expanded = viewModel.lowerLevelBoundVisibility.value,
                                onDismissRequest = { viewModel.lowerLevelBoundVisibility.value = false },
                                modifier = Modifier
                                    .background(
                                        TextIconColor
                                    )
                            ) {
                                ASCENSION_LEVELS.subList(0,maxLevelIndex.value).forEachIndexed { index, level ->
                                    DropdownMenuItem(
                                        onClick = {
                                            viewModel.onLowerLevelBoundChange(index)
                                        },
                                        text = {
                                            Text(
                                                text = level,
                                                color = SecondaryBg
                                            )
                                        }
                                    )
                                }
                            }
                        }
                        Spacer(
                            modifier = Modifier.padding(8.dp)
                        )
                        Box (
                            modifier = Modifier
                                .clip(RoundedCornerShape(0.dp, 16.dp, 16.dp, 0.dp))
                                .background(TextIconColor)
                                .width(48.dp)
                                .clickable {
                                    viewModel.upperLevelBoundVisibility.value = true
                                }
                        ) {
                            Text(
                                text = upperLevelBoundValue.value,
                                color = SecondaryBg,
                                textAlign = TextAlign.Center,
                                fontWeight = FontWeight.Bold,
                                modifier = Modifier
                                    .fillMaxSize()
                                    .padding(4.dp),
                            )
                            DropdownMenu(
                                expanded = viewModel.upperLevelBoundVisibility.value,
                                onDismissRequest = { viewModel.upperLevelBoundVisibility.value = false },
                                modifier = Modifier
                                    .background(
                                        TextIconColor
                                    )
                            ) {
                                ASCENSION_LEVELS.subList(1, ASCENSION_LEVELS.size).forEachIndexed { index, level ->
                                    DropdownMenuItem(
                                        onClick = {
                                            viewModel.onUpperLevelBoundChange(index+1)
                                        },
                                        text = {
                                            Text(
                                                text = level,
                                                color = SecondaryBg
                                            )
                                        }
                                    )
                                }
                            }
                        }
                    }
                }
                Divider(
                    color = BorderColor,
                    thickness = 4.dp,
                    modifier = Modifier.padding(bottom = 8.dp)
                )
                Text(
                    text = stringResource(R.string.CharacterDetailsAscensionTalentLabel),
                    fontWeight = FontWeight.SemiBold,
                    fontSize = 18.sp,
                )
                Column {
                    character.value.talent?.let {
                        TalentRow(
                            talentEntity = it["0"],
                            checkState = viewModel.normalAttackCheckedState,
                            levelBounds = viewModel.normalAttackLevelBounds,
                            viewModel = viewModel
                        )
                        TalentRow(
                            talentEntity = it["1"],
                            checkState = viewModel.skillCheckedState,
                            levelBounds = viewModel.skillLevelBounds,
                            viewModel = viewModel
                        )
                        if (it["2"] != null) {

                            TalentRow(
                                talentEntity = it["4"],
                                checkState = viewModel.burstCheckedState,
                                levelBounds = viewModel.burstLevelBounds,
                                viewModel = viewModel
                            )
                        } else {
                            TalentRow(
                                talentEntity = it["3"],
                                checkState = viewModel.burstCheckedState,
                                levelBounds = viewModel.burstLevelBounds,
                                viewModel = viewModel
                            )
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun TalentRow(
    talentEntity: TalentEntity?,
    viewModel: CharacterDetailsViewModel,
    checkState: MutableState<Boolean>,
    levelBounds: State<TalentLevelSelector>
) {
    talentEntity?.let {talent ->
        val talentUrl = "https://api.ambr.top/assets/UI/"+talent.icon+".png"

        Row (
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween
                ){
            Row {
                Checkbox(
                    checked = checkState.value,
                    onCheckedChange = {
                        checkState.value = it
                        viewModel.getMaterials()
                                      },
                    colors = CheckboxDefaults.colors(
                        checkedColor = CheckedColor,
                        uncheckedColor = TextIconColor,
                        checkmarkColor = TextIconColor,
                    )
                )
                Image(
                    painter = rememberAsyncImagePainter(
                        ImageRequest.Builder(
                            LocalContext.current
                        )
                            .data(talentUrl)
                            .apply(
                                block = fun ImageRequest.Builder.() {
                                    size(Size.ORIGINAL)
                                }
                            )
                            .build()
                    ),
                    contentDescription = null,
                    modifier = Modifier
                        .size(48.dp)
                        .background(
                            color = NavBackGround,
                            shape = CircleShape
                        )
                        .border(2.dp, BorderColor, CircleShape),
                )
            }
            Row {
                Box (
                    modifier = Modifier
                        .clip(RoundedCornerShape(16.dp, 0.dp, 0.dp, 16.dp))
                        .background(TextIconColor)
                        .width(48.dp)
                        .clickable {
                            levelBounds.value.lowerVisibility.value = true
                        }
                ) {
                    Text(
                        text = levelBounds.value.lowerLevelBound.value.toString(),
                        color = SecondaryBg,
                        textAlign = TextAlign.Center,
                        fontWeight = FontWeight.Bold,
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(4.dp),
                    )
                    DropdownMenu(
                        expanded = levelBounds.value.lowerVisibility.value,
                        onDismissRequest = { levelBounds.value.lowerVisibility.value = false },
                        modifier = Modifier
                            .background(
                                TextIconColor
                            )
                    ) {
                        (1 until levelBounds.value.upperLevelBound.value).toList().forEachIndexed { index, level -> //replace this with values from Constants.kt
                            DropdownMenuItem(
                                onClick = {
                                    viewModel.onLowerTalentLevelChange(
                                        type = levelBounds.value.type,
                                        index = index
                                    )
                                },
                                text = {
                                    Text(
                                        text = level.toString(),
                                        color = SecondaryBg
                                    )
                                }
                            )
                        }
                    }
                }
                Spacer(
                    modifier = Modifier.padding(8.dp)
                )
                Box (
                    modifier = Modifier
                        .clip(RoundedCornerShape(0.dp, 16.dp, 16.dp, 0.dp))
                        .background(TextIconColor)
                        .width(48.dp)
                        .clickable {
                            levelBounds.value.upperVisibility.value = true
                        }
                ) {
                    Text(
                        text = levelBounds.value.upperLevelBound.value.toString(),
                        color = SecondaryBg,
                        textAlign = TextAlign.Center,
                        fontWeight = FontWeight.Bold,
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(4.dp),
                    )
                    DropdownMenu(
                        expanded = levelBounds.value.upperVisibility.value,
                        onDismissRequest = { levelBounds.value.upperVisibility.value = false },
                        modifier = Modifier
                            .background(
                                TextIconColor
                            )
                    ) {
                        (2.. 10).toList().forEachIndexed { index, level -> //replace this with values from Constants.kt
                            DropdownMenuItem(
                                onClick = {
                                    levelBounds.value.index.value = index
                                    viewModel.onUpperTalentLevelChange(levelBounds)
                                },
                                text = {
                                    Text(
                                        text = level.toString(),
                                        color = SecondaryBg
                                    )
                                }
                            )
                        }
                    }
                }
            }
        }
    }
}

