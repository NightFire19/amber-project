package com.xuan.projectamber.ui.screens.characterDetails.constellation

import android.util.Log
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.size.Size
import com.xuan.projectamber.domain.models.CharacterEntity
import com.xuan.projectamber.domain.models.ConstellationEntity
import com.xuan.projectamber.ui.theme.BorderColor
import com.xuan.projectamber.ui.theme.NavBackGround
import com.xuan.projectamber.ui.theme.SecondaryBg
import com.xuan.projectamber.ui.theme.TextIconColor
import com.xuan.projectamber.viewmodel.CharacterDetailsViewModel
import de.charlex.compose.HtmlText

@Composable
fun Constellation(
    character: State<CharacterEntity>,
    viewModel: CharacterDetailsViewModel
) {
    character.value.constellation?.forEach {
        ConstellationCard(level = it.key.toInt(), constellationEntity = it.value, viewModel = viewModel)
    }
}

@Composable
fun ConstellationCard(
    level: Int,
    constellationEntity: ConstellationEntity,
    viewModel: CharacterDetailsViewModel
) {
    val constellationUrl = "https://api.ambr.top/assets/UI/"+constellationEntity.icon+".png"

    Card(
        modifier = Modifier.padding(8.dp),
        colors = CardDefaults.cardColors(containerColor = SecondaryBg),
        border = BorderStroke(2.dp, BorderColor),
        ) {
        Column(
            modifier = Modifier.padding(8.dp)
        ) {
            Row(
                modifier = Modifier.padding(8.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    painter = rememberAsyncImagePainter(
                        ImageRequest.Builder(
                            LocalContext.current
                        )
                            .data(constellationUrl)
                            .apply(
                                block = fun ImageRequest.Builder.() {
                                    size(Size.ORIGINAL)
                                }
                            )
                            .build()
                    ),
                    contentDescription = null,
                    modifier = Modifier
                        .size(48.dp)
                        .background(
                            color = NavBackGround,
                            shape = CircleShape
                        )
                        .border(2.dp, BorderColor, CircleShape),
                )
                Spacer(modifier = Modifier.size(8.dp))
                Text(
                    text = (level+1).toString()+". "+constellationEntity.name,
                    fontWeight = FontWeight.SemiBold,
                    fontSize = 18.sp,
                )
            }
            Divider(
                color = BorderColor,
                thickness = 4.dp,
                modifier = Modifier.padding(bottom = 8.dp)
            )
            HtmlText(
                text = viewModel.toHtmlText(constellationEntity.description),
                color = TextIconColor,
                fontFamily = FontFamily.Default
            )
        }
    }
}