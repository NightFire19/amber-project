
package com.xuan.projectamber.ui.screens.characterDetails.other

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.size.Size
import com.xuan.projectamber.R
import com.xuan.projectamber.domain.models.CharacterEntity
import com.xuan.projectamber.ui.theme.BorderColor
import com.xuan.projectamber.ui.theme.DarkGrayTextColor
import com.xuan.projectamber.ui.theme.NavBackGround
import com.xuan.projectamber.ui.theme.SecondaryBg
import com.xuan.projectamber.ui.theme.TextIconColor
import com.xuan.projectamber.viewmodel.CharacterDetailsViewModel

@Composable
fun Other(
    character: State<CharacterEntity>,
    viewModel: CharacterDetailsViewModel,
    navController: NavController,
) {

    val namecardUrl = "https://api.ambr.top/assets/UI/namecard//UI_NameCardPic_"+
            (character.value.other?.nameCard?.icon?.substringAfterLast("_")) +
            "_P.png"

    val foodUrl = "https://api.ambr.top/assets/UI/" +
            character.value.other?.specialFood?.icon +
            ".png"

    val buffUrl = "https://api.ambr.top/assets/UI/" +
            character.value.other?.specialFood?.effectIcon +
            ".png"

    Column() {
        Card(
            modifier = Modifier.padding(8.dp),
            colors = CardDefaults.cardColors(containerColor = SecondaryBg),
            border = BorderStroke(2.dp, BorderColor),
        ) {
            Column(
                modifier = Modifier.padding(8.dp)
            ) {
                Text(
                    text = stringResource(id = R.string.CharacterDetailsOtherNamecardHeader),
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier.padding(vertical = 4.dp)
                )
                Card(
                    modifier = Modifier
                        .fillMaxWidth(),
                    colors = CardDefaults.cardColors(containerColor = TextIconColor),
                    ) {
                    Column() {
                        Image(
                            painter = rememberAsyncImagePainter(
                                ImageRequest.Builder(
                                    LocalContext.current
                                ).data(namecardUrl).apply(block = fun ImageRequest.Builder.() {
                                    size(Size.ORIGINAL)
                                }).build()
                            ),
                            contentScale = ContentScale.FillWidth,
                            contentDescription = null,
                            modifier = Modifier.fillMaxWidth()
                        )
                        character.value.other?.nameCard?.let { nameCard ->
                            Column(
                                modifier = Modifier.padding(8.dp)
                            ) {
                                Text(
                                    text = nameCard.name,
                                    color = DarkGrayTextColor,
                                    fontWeight = FontWeight.Bold
                                )
                                Divider(
                                    color = BorderColor,
                                    thickness = 2.dp,
                                    modifier = Modifier.padding(vertical = 2.dp)
                                )
                                Text(
                                    text = nameCard.description.replace("\\n","\n"),
                                    color = DarkGrayTextColor,
                                )
                            }
                        }
                    }
                }
            }
        }
        Card(
            modifier = Modifier
                .padding(8.dp)
                .fillMaxWidth(),
            colors = CardDefaults.cardColors(containerColor = SecondaryBg),
            border = BorderStroke(2.dp, BorderColor),
        ) {
            Column(
                modifier = Modifier.padding(8.dp),
            ) {
                Text(
                    text = stringResource(id = R.string.CharacterDetailsOtherSpecialtyHeader),
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier.padding(vertical = 4.dp)
                )
                Row() {
                    Spacer(modifier = Modifier.weight(1f))
                    Card(
                        modifier = Modifier
                            .weight(1f)
                            .clickable {
                                navController.navigate(
                                    "foodDetails/{id}"
                                        .replace(
                                            oldValue = "{id}",
                                            newValue = character.value.other?.specialFood?.id.toString()
                                        )
                                )
                            },
                        colors = CardDefaults.cardColors(containerColor = TextIconColor),
                        ) {
                        Column(
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            Box(
                                Modifier.background(NavBackGround)
                            ) {
                                Image(
                                    painter = rememberAsyncImagePainter(
                                        ImageRequest.Builder(
                                            LocalContext.current
                                        )
                                            .data(foodUrl)
                                            .apply(
                                                block = fun ImageRequest.Builder.() {
                                                    size(Size.ORIGINAL)
                                                }
                                            )
                                            .build()
                                    ),
                                    contentScale = ContentScale.FillWidth,
                                    contentDescription = null,
                                    modifier = Modifier.fillMaxWidth()
                                )
                                Row (modifier = Modifier.padding(8.dp)){
                                    Column (modifier = Modifier.weight(2f)) {
                                        Image(
                                            painter = rememberAsyncImagePainter(
                                                ImageRequest.Builder(
                                                    LocalContext.current
                                                )
                                                    .data(buffUrl)
                                                    .apply(
                                                        block = fun ImageRequest.Builder.() {
                                                            size(Size.ORIGINAL)
                                                        }
                                                    )
                                                    .build()
                                            ),
                                            contentScale = ContentScale.FillWidth,
                                            contentDescription = null,
                                            modifier = Modifier.fillMaxWidth()
                                        )
                                    }
                                    Spacer(modifier = Modifier.weight(5f))
                                }
                            }
                            character.value.other?.specialFood?.let { specialFood ->
                                Text(
                                    text = specialFood.name,
                                    color = DarkGrayTextColor,
                                    fontWeight = FontWeight.Bold,
                                    textAlign = TextAlign.Center,
                                )
                            }
                        }
                    }
                    Spacer(modifier = Modifier.weight(1f))
                }
            }
        }
    }
}