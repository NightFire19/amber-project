package com.xuan.projectamber.ui.screens.characterDetails.profile

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.xuan.projectamber.R
import com.xuan.projectamber.domain.models.CharacterEntity
import com.xuan.projectamber.ui.common.CustomSliderColors
import com.xuan.projectamber.ui.theme.*
import com.xuan.projectamber.viewmodel.CharacterDetailsViewModel

@Composable
fun Profile(
    character: State<CharacterEntity>,
    viewModel: CharacterDetailsViewModel
) {
    Card(
        colors = CardDefaults.cardColors(containerColor = SecondaryBg),
        border = BorderStroke(2.dp, BorderColor),
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 8.dp, horizontal = 4.dp)
    ) {
        Column {
            val selectedValue: Int by viewModel.selected.observeAsState(initial = 0)
            val selectedLevel: Int by viewModel.level.observeAsState(initial = 0)

            val health: Int by viewModel.health.observeAsState(initial = 0)
            val attack: Int by viewModel.attack.observeAsState(initial = 0)
            val defense: Int by viewModel.defense.observeAsState(initial = 0)
            val ascensionStat: Float by viewModel.ascensionStat.observeAsState(initial = 0f)
            val ascensionName: Int by viewModel.ascensionName.observeAsState(initial = 0)

            Row(
                modifier = Modifier
                    .fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Spacer(modifier = Modifier.size(8.dp))
                Text(
                    text = stringResource(id = R.string.CharacterDetailsProfileLvLabel, selectedLevel),
                    fontSize = 36.sp,
                    fontWeight = FontWeight.Bold,
                )
                LevelSlider(
                    value = selectedValue.toFloat(),
                ) {
                    viewModel.onValueChanged(it)
                }
            }
            if (character.value.upgradeEntity != null) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 8.dp)
                        .background(RowPrimary)
                        .padding(4.dp),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(text = stringResource(R.string.FIGHT_PROP_BASE_HP))
                    Text(health.toString())

                }
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 8.dp)
                        .background(RowSecondary)
                        .padding(4.dp),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(text = stringResource(id = R.string.FIGHT_PROP_BASE_ATTACK))
                    Text(attack.toString())
                }
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 8.dp)
                        .background(RowPrimary)
                        .padding(4.dp),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(text = stringResource(id = R.string.FIGHT_PROP_BASE_DEFENSE))
                    Text(defense.toString())
                }
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 8.dp)
                        .background(RowSecondary)
                        .padding(4.dp),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(
                        text = stringResource(ascensionName)
                    )
                    if (viewModel.isAscensionStatPercentage) {
                        Text(
                            text = viewModel.convertToPercentage(ascensionStat)
                        )
                    } else {
                        Text(
                            text = ascensionStat.toString()
                        )
                    }
                }
            }
            character.value.fetterEntity?.cv?.let { cv ->
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 8.dp)
                        .background(RowPrimary)
                        .padding(4.dp),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(text = stringResource(id = R.string.CharacterDetailsProfileVAEnglish))
                    Text(
                        cv.EN
                    )
                }
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 8.dp)
                        .background(RowSecondary)
                        .padding(4.dp),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(text = stringResource(id = R.string.CharacterDetailsProfileVAChinese))
                    Text(
                        cv.CHS
                    )
                }
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 8.dp)
                        .background(RowPrimary)
                        .padding(4.dp),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(text = stringResource(id = R.string.CharacterDetailsProfileVAJapanese))
                    Text(
                        cv.JP
                    )
                }
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 8.dp)
                        .background(RowSecondary)
                        .padding(4.dp),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(text = stringResource(id = R.string.CharacterDetailsProfileVAKorean))
                    Text(
                        cv.KR
                    )
                }
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 8.dp)
                    .background(RowPrimary)
                    .padding(4.dp),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    text = stringResource(R.string.CharacterDetailsProfileBirthdayLabel)
                )
                if (character.value.birthday != null) {
                    Text(
                        text =
                        character.value.birthday!![0].toString() +
                                "/" +
                                character.value.birthday!![1]
                    )
                } else {
                    Text(text = stringResource(R.string.CharacterDetailsProfileNoBirthdayValue))
                }
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 8.dp)
                    .background(RowSecondary)
                    .padding(4.dp),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    text = stringResource(R.string.CharacterDetailsProfileAffiliationLabel)
                )
                if (character.value.fetterEntity != null) {
                    Text(
                        text = character.value.fetterEntity!!.native
                    )
                }
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 8.dp)
                    .background(RowPrimary)
                    .padding(4.dp),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    text = stringResource(R.string.CharacterDetailsProfileElementLabel)
                )
                if (viewModel.elementPng != 0) {
                    Image(
                        painter = painterResource(
                            id = viewModel.elementPng
                        ),
                        contentDescription = "Element",
                        modifier = Modifier
                            .padding(end = 4.dp)
                            .size(24.dp),
                    )
                }
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 8.dp)
                    .background(RowSecondary)
                    .padding(4.dp),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    text = stringResource(R.string.CharacterDetailsProfileConstellationLabel)
                )
                if (character.value.fetterEntity != null) {
                    Text(
                        text = character.value.fetterEntity!!.constellation
                    )
                }
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 8.dp)
                    .padding(4.dp),
            ) {
                character.value.fetterEntity?.let { fetterEntity ->
                    Text(
                        text = fetterEntity.detail,
                        color = TextIconColor.copy(alpha = 0.7f),
                        modifier = Modifier.padding(8.dp)
                    )
                }
            }
        }
    }
}

@Composable
fun LevelSlider(value: Float, onValueChange: (Int) -> Unit) {
    val (sliderValue, setSliderValue) = remember { mutableStateOf(value) }

    Row (
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp),
        verticalAlignment = Alignment.CenterVertically
            ){
        Slider(
            modifier = Modifier
                .fillMaxWidth(),
            value = sliderValue,
            valueRange = 0f..9f,
            steps = 8,
            colors = CustomSliderColors(),
            onValueChange = {
                setSliderValue(it)
                onValueChange(it.toInt())
            }
        )
    }
}


