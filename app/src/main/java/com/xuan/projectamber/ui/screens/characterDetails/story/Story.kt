package com.xuan.projectamber.ui.screens.characterDetails.story

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.xuan.projectamber.R
import com.xuan.projectamber.domain.models.CharacterStoryEntity
import com.xuan.projectamber.ui.theme.BorderColor
import com.xuan.projectamber.ui.theme.SecondaryBg
import com.xuan.projectamber.ui.theme.TextIconColor
import com.xuan.projectamber.viewmodel.CharacterDetailsViewModel
import de.charlex.compose.HtmlText

@Composable
fun Story(
    viewModel: CharacterDetailsViewModel,
    story: State<CharacterStoryEntity?>,
) {
    Column {
        if (story.value != null) {
            Card(
                modifier = Modifier.padding(8.dp),
                colors = CardDefaults.cardColors(containerColor = SecondaryBg),
                border = BorderStroke(2.dp, BorderColor),
            ) {
                Column {
                    Row(
                        verticalAlignment = Alignment.CenterVertically,
                        modifier = Modifier
                            .fillMaxWidth()
                            .clickable {
                                viewModel.storySelector = true
                            }
                    ) {
                        RadioButton(
                            selected = viewModel.storySelector,
                            onClick = {
                                viewModel.storySelector = true
                            },
                            colors = RadioButtonDefaults.colors(
                                selectedColor = TextIconColor,
                                unselectedColor = TextIconColor,
                            )
                        )
                        Text(
                            text = stringResource(id = R.string.CharacterDetailsStoryStoryOption),
                            modifier = Modifier.fillMaxWidth()
                        )
                    }
                    Row(
                        verticalAlignment = Alignment.CenterVertically,
                        modifier = Modifier
                            .fillMaxWidth()
                            .clickable {
                                viewModel.storySelector = false
                            }
                    ) {
                        RadioButton(
                            selected = !viewModel.storySelector,
                            onClick = {
                                viewModel.storySelector = false
                            },
                            colors = RadioButtonDefaults.colors(
                                selectedColor = TextIconColor,
                                unselectedColor = TextIconColor,
                            ),
                        )
                        Text(
                            text = stringResource(id = R.string.CharacterDetailsStoryQuoteOption),
                            modifier = Modifier.fillMaxWidth()
                        )
                    }
                }
            }
            Divider(
                color = BorderColor,
                thickness = 4.dp,
                modifier = Modifier.padding(vertical = 8.dp)
            )
            Column {
                if (viewModel.storySelector) {
                    story.value!!.story?.forEach {storyEntry ->
                        Column {
                            Text(
                                text = storyEntry.value.title,
                                fontWeight = FontWeight.Bold,
                                modifier = Modifier.padding(start = 8.dp)
                            )
                            Card(
                                modifier = Modifier.padding(8.dp),
                                colors = CardDefaults.cardColors(containerColor = SecondaryBg),
                                border = BorderStroke(2.dp, BorderColor),
                            ) {
                                val columnStrings = storyEntry.value.text.split("\\n")
                                Column (
                                    modifier = Modifier.padding(8.dp),
                                ){
                                    for (string in columnStrings) {
                                        HtmlText(
                                            text = viewModel.toHtmlText(string),
                                            color = TextIconColor,
                                            fontFamily = FontFamily.Default,
                                            modifier = Modifier
                                                .padding(8.dp)
                                                .fillMaxWidth(),
                                        )
                                    }
                                }
                            }
                            Spacer(
                                modifier = Modifier.size(8.dp)
                            )
                        }
                    }
                } else {
                    story.value!!.quotes?.forEach { quoteEntry ->
                        Column {
                            Text(
                                text = quoteEntry.value.title,
                                fontWeight = FontWeight.Bold,
                                modifier = Modifier.padding(start = 8.dp)
                            )
                            Card(
                                modifier = Modifier.padding(8.dp),
                                colors = CardDefaults.cardColors(containerColor = SecondaryBg),
                                border = BorderStroke(2.dp, BorderColor),
                            ) {

                                val columnStrings = quoteEntry.value.text.split("\\n")
                                Column (
                                    modifier = Modifier.padding(8.dp),
                                ){
                                    for (string in columnStrings) {
                                        HtmlText(
                                            text = viewModel.toHtmlText(string),
                                            color = TextIconColor,
                                            fontFamily = FontFamily.Default,
                                            modifier = Modifier
                                                .padding(8.dp)
                                                .fillMaxWidth(),
                                        )
                                    }
                                }
                            }
                            Spacer(
                                modifier = Modifier.size(8.dp)
                            )
                        }
                    }
                }
            }
        } else {
            Column(
                modifier = Modifier.fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center,
            ) {
                Text(
                    text = stringResource(R.string.CharacterDetailsStoryNullStatement),
                    modifier = Modifier.padding(16.dp)
                )
            }
        }
    }
}



