package com.xuan.projectamber.ui.screens.characterDetails.talent

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.size.Size
import com.xuan.projectamber.R
import com.xuan.projectamber.domain.models.CharacterEntity
import com.xuan.projectamber.domain.models.TalentEntity
import com.xuan.projectamber.ui.common.CustomSliderColors
import com.xuan.projectamber.ui.theme.*
import com.xuan.projectamber.viewmodel.CharacterDetailsViewModel
import de.charlex.compose.HtmlText
import kotlin.math.roundToInt

@Composable
fun Talent(
    character: State<CharacterEntity>,
    viewModel: CharacterDetailsViewModel
) {
    val selectedNormalAttack: Int by viewModel.selectedNormalAttack.observeAsState(initial = 0)
    val selectedSkill: Int by viewModel.selectedSkill.observeAsState(initial = 0)
    val selectedBurst: Int by viewModel.selectedBurst.observeAsState(initial = 0)


    Column() {
        Text(
            text = stringResource(R.string.CharacterDetailsTalentCombatLabel),
            modifier = Modifier.padding(8.dp),
            fontWeight = FontWeight.SemiBold,
            fontSize = 18.sp,
        )
        character.value.talent?.let { it ->
            TalentCard(
                talentEntity = it["0"]!!,
                viewModel = viewModel,
                selectedValue = selectedNormalAttack,
            ) { int ->
                viewModel.onNormalAttackValueChanged(int)
            }
            TalentCard(
                talentEntity = it["1"]!!,
                viewModel = viewModel,
                selectedValue = selectedSkill,
            ) { int ->
                viewModel.onSkillValueChanged(int)
            }
            if (it["2"] != null) {
                TalentCard(
                    talentEntity = it["2"]!!,
                    viewModel = viewModel,
                )
            }
            if (it["3"] != null) {
                TalentCard(
                    talentEntity = it["3"]!!,
                    viewModel = viewModel,
                    selectedValue = selectedBurst,
                ) { int ->
                    viewModel.onBurstValueChanged(int)
                }
            }

            if (it["4"] != null) {
                if (it["4"]?.type != 2) {
                    TalentCard(
                        talentEntity = it["4"]!!,
                        viewModel = viewModel,
                        selectedValue = selectedBurst,
                    ) { int ->
                        viewModel.onBurstValueChanged(int)
                    }
                }
            }
        }
        Text(
            text = stringResource(id = R.string.CharacterDetailsTalentPassiveLabel),
            modifier = Modifier.padding(8.dp),
            fontWeight = FontWeight.SemiBold,
            fontSize = 18.sp,
        )
        character.value.talent?.forEach {
            if (it.value.type == 2) {
                TalentCard(
                    talentEntity = it.value,
                    viewModel = viewModel
                )
            }
        }
    }
}


@Composable
fun TalentCard(
    talentEntity: TalentEntity,
    viewModel: CharacterDetailsViewModel
) {
    val talentUrl = "https://api.ambr.top/assets/UI/" + talentEntity.icon + ".png"

    Card(
        modifier = Modifier.padding(8.dp),
        colors = CardDefaults.cardColors(containerColor = SecondaryBg),
        border = BorderStroke(2.dp, BorderColor),
    ) {
        Column(
            modifier = Modifier.padding(8.dp)
        ) {
            Row(
                modifier = Modifier.padding(8.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    painter = rememberAsyncImagePainter(
                        ImageRequest.Builder(
                            LocalContext.current
                        )
                            .data(talentUrl)
                            .apply(
                                block = fun ImageRequest.Builder.() {
                                    size(Size.ORIGINAL)
                                }
                            )
                            .build()
                    ),
                    contentDescription = null,
                    modifier = Modifier
                        .size(48.dp)
                        .background(
                            color = NavBackGround,
                            shape = CircleShape
                        )
                        .border(2.dp, BorderColor, CircleShape),
                )
                Spacer(modifier = Modifier.size(8.dp))
                Text(
                    text = talentEntity.name,
                    fontWeight = FontWeight.SemiBold,
                    fontSize = 18.sp,
                )
            }
            Divider(
                color = BorderColor,
                thickness = 4.dp,
                modifier = Modifier.padding(bottom = 8.dp)
            )
            val columnStrings = talentEntity.description.split("\\n")
            Column(
                modifier = Modifier.padding(8.dp),
            ) {
                for (string in columnStrings) {
                    HtmlText(
                        text = viewModel.toHtmlText(string),
                        color = TextIconColor,
                        fontFamily = FontFamily.Default
                    )
                }
            }
        }
    }
}

@Composable
fun TalentCard(
    talentEntity: TalentEntity,
    viewModel: CharacterDetailsViewModel,
    selectedValue: Int,
    onValueChange: (Int) -> Unit,
) {
    val talentUrl = "https://api.ambr.top/assets/UI/" + talentEntity.icon + ".png"

    Card(
        modifier = Modifier.padding(8.dp),
        colors = CardDefaults.cardColors(containerColor = SecondaryBg),
        border = BorderStroke(2.dp, BorderColor),
    ) {
        Column(
            modifier = Modifier.padding(8.dp)
        ) {
            Row(
                modifier = Modifier.padding(8.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    painter = rememberAsyncImagePainter(
                        ImageRequest.Builder(
                            LocalContext.current
                        )
                            .data(talentUrl)
                            .apply(
                                block = fun ImageRequest.Builder.() {
                                    size(Size.ORIGINAL)
                                }
                            )
                            .build()
                    ),
                    contentDescription = null,
                    modifier = Modifier
                        .size(48.dp)
                        .background(
                            color = NavBackGround,
                            shape = CircleShape
                        )
                        .border(2.dp, BorderColor, CircleShape),
                )
                Spacer(modifier = Modifier.size(8.dp))
                Text(
                    text = talentEntity.name,
                    fontWeight = FontWeight.SemiBold,
                    fontSize = 18.sp,
                )
            }
            Divider(
                color = BorderColor,
                thickness = 4.dp,
                modifier = Modifier.padding(bottom = 8.dp)
            )

            val columnStrings = talentEntity.description.split("\\n")

            Column(
                modifier = Modifier.padding(8.dp),
            ) {
                for (string in columnStrings) {
                    HtmlText(
                        text = viewModel.toHtmlText(string),
                        color = TextIconColor,
                        fontFamily = FontFamily.Default
                    )
                }
            }

            Card(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 8.dp),
                colors = CardDefaults.cardColors(containerColor = BorderColor),
            ) {
                Row(
                    horizontalArrangement = Arrangement.Center,
                    modifier = Modifier
                        .padding(8.dp)
                        .fillMaxWidth()
                ) {
                    Text(
                        text = stringResource(id = R.string.CharacterDetailsTalentLevelLabel, selectedValue+1), //"Skill Attributes (Lv. " + (selectedValue + 1) + ")"
                        fontWeight = FontWeight.SemiBold,
                        fontSize = 18.sp,
                    )
                }
            }
            LevelSlider(
                value = selectedValue.toFloat(),
            ) {
                onValueChange(it)
            }
            talentEntity.promote["1"]?.description?.forEach { description ->
                if (description != "") {
                    Row(
                        modifier = Modifier
                            .padding(vertical = 4.dp)
                            .background(color = ScreenBackGround)
                            .fillMaxWidth(),
                        horizontalArrangement = Arrangement.SpaceBetween,
                        verticalAlignment = Alignment.CenterVertically,
                    ) {
                        Column(
                            modifier = Modifier
                                .padding(8.dp)
                                .weight(3f)
                        ) {
                            Text(
                                text = viewModel.getDescriptionText(description),
                                color = GoldTextColor,
                            )
                        }
                        Column(
                            modifier = Modifier
                                .padding(8.dp)
                                .weight(1f)
                        ) {
                            Text(
                                text = viewModel.getParams(
                                    description,
                                    selectedValue,
                                    talentEntity
                                ),
                                modifier = Modifier.fillMaxWidth(),
                                fontWeight = FontWeight.SemiBold,
                                textAlign = TextAlign.End,
                            )
                        }
                    }
                }
            }
        }
    }
}

@Composable
private fun LevelSlider(value: Float, onValueChange: (Int) -> Unit) {
    val (sliderValue, setSliderValue) = remember { mutableStateOf(value) }

    Row(
        modifier = Modifier
            .fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Slider(
            modifier = Modifier
                .fillMaxWidth(),
            value = sliderValue,
            valueRange = 0f..9f,
            steps = 8,
            colors = CustomSliderColors(),
            onValueChange = {
                setSliderValue(it)
                onValueChange(it.roundToInt())
            }
        )
    }
}
