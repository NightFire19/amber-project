package com.xuan.projectamber.ui.screens.elements

import android.content.Context
import android.util.Log
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowLeft
import androidx.compose.material.icons.filled.ArrowRight
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.rememberAsyncImagePainter
import coil.imageLoader
import coil.request.ImageRequest
import coil.size.Size
import com.xuan.projectamber.R
import com.xuan.projectamber.domain.models.ResonanceEntity
import com.xuan.projectamber.domain.models.TutorialDetailsEntity
import com.xuan.projectamber.domain.models.TutorialEntity
import com.xuan.projectamber.ui.theme.*
import com.xuan.projectamber.util.Constants.TYPE
import com.xuan.projectamber.viewmodel.ElementsViewModel
import de.charlex.compose.HtmlText
import java.util.*

@Composable
fun ElementsScreen(
    navController: NavController,
    viewModel: ElementsViewModel = hiltViewModel()
) {
    val elementsEntity = viewModel.elementsEntity
    val context = LocalContext.current

    Surface(
        color = ScreenBackGround,
        modifier = Modifier
            .fillMaxSize()
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .verticalScroll(rememberScrollState())
                .padding(8.dp)
        ) {
            if (elementsEntity.info != null) {
                Text(
                    text = elementsEntity.info.resonance.title,
                    fontWeight = FontWeight.SemiBold,
                    fontSize = 24.sp,
                    modifier = Modifier.padding(vertical = 8.dp)
                )
                Text(
                    text = elementsEntity.info.resonance.description
                )

                elementsEntity.resonance.forEach { (s, resonanceEntity) ->
                    ResonanceCard(resonanceEntity = resonanceEntity)
                }

                Text(
                    text = stringResource(R.string.ElementsReactionsHeader),
                    fontWeight = FontWeight.SemiBold,
                    fontSize = 24.sp,
                    modifier = Modifier.padding(vertical = 8.dp)
                )

                elementsEntity.tutorials.forEach { (_, tutorialEntity) ->
                    TutorialCard(
                        viewModel = viewModel,
                        tutorialEntity = tutorialEntity,
                        context = context
                    )
                }

                Text(
                    text = stringResource(R.string.ElementsTipsHeader),
                    fontWeight = FontWeight.SemiBold,
                    fontSize = 24.sp,
                    modifier = Modifier.padding(vertical = 8.dp)
                )
                elementsEntity.tips.forEach { (s, elementTipEntity) ->
                    Row(
                        modifier = Modifier.fillMaxWidth()
                    ) {
                        Text(
                            text = "\u2022",
                            modifier = Modifier.padding(8.dp)
                        )
                        Text(
                            text = elementTipEntity.description,
                            modifier = Modifier.padding(8.dp)
                        )
                    }
                }

            }
        }
    }
}

@Composable
fun ResonanceCard(
    resonanceEntity: ResonanceEntity
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp),
        colors = CardDefaults.cardColors(containerColor = SecondaryBg),
        border = BorderStroke(2.dp, BorderColor),
        shape = RoundedCornerShape(8.dp)
    ) {
        Column {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(color = ScreenBackGround),
                verticalAlignment = Alignment.CenterVertically
            ) {

                if (resonanceEntity.elements != null) {
                    resonanceEntity.elements.forEach { (element, count) ->
                        Log.v("test2", element)
                        val icon = TYPE[element.replaceFirstChar {
                            if (it.isLowerCase()) it.titlecase(
                                Locale.ROOT
                            ) else it.toString()
                        }]

                        if (icon != null) {
                            LazyRow(
                                modifier = Modifier.padding(start = 16.dp, end = 8.dp)
                            ) {
                                items(count) {
                                    Image(
                                        painter = painterResource(
                                            id = icon
                                        ),
                                        contentDescription = "Element",
                                        modifier = Modifier
                                            .padding(end = 4.dp)
                                            .size(24.dp),
                                    )
                                }
                            }
                        }
                    }
                }
                Text(
                    modifier = Modifier.padding(8.dp),
                    text = resonanceEntity.name,
                    fontSize = 16.sp
                )
            }
            Text(
                modifier = Modifier.padding(4.dp),
                text = resonanceEntity.description
            )
        }
    }

}

@Composable
fun TutorialCard(
    viewModel: ElementsViewModel,
    tutorialEntity: TutorialEntity,
    context: Context
) {

    val detailsList = tutorialEntity.details.values.toList()
    val iconUrl = "https://api.ambr.top/assets/UI/other/${tutorialEntity.icon}.png"

    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp),
        colors = CardDefaults.cardColors(containerColor = SecondaryBg),
        border = BorderStroke(2.dp, BorderColor),
        shape = RoundedCornerShape(8.dp)
    ) {
        Column {
            Row(
                modifier = Modifier.padding(8.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    painter = rememberAsyncImagePainter(
                        ImageRequest.Builder(
                            LocalContext.current
                        )
                            .data(iconUrl)
                            .apply(
                                block = fun ImageRequest.Builder.() {
                                    size(Size.ORIGINAL)
                                }
                            )
                            .build()
                    ),
                    contentDescription = null,
                    modifier = Modifier
                        .size(48.dp)
                        .background(
                            color = NavBackGround,
                            shape = CircleShape
                        )
                        .border(2.dp, BorderColor, CircleShape),
                )
                Text(
                    text = tutorialEntity.name,
                    modifier = Modifier.padding(horizontal = 8.dp)
                )
            }
            Divider(color = BorderColor)
            TutorialDetails(
                viewModel = viewModel,
                detailsList = detailsList,
                context = context,
            )
        }
    }
}

@Composable
fun TutorialDetails(
    viewModel: ElementsViewModel,
    detailsList: List<TutorialDetailsEntity>,
    context: Context
) {
    var detailsListIndex by remember { mutableStateOf(0) }
    var currentDetail by remember { mutableStateOf(detailsList[detailsListIndex]) }

    val imageUrl = "https://api.ambr.top/assets/UI/tutorial/${currentDetail.images.first()}.png"

    LaunchedEffect(Unit) {
        detailsList.forEach { tutorialDetailsEntity ->
            val request = ImageRequest.Builder(context)
                .data("https://api.ambr.top/assets/UI/tutorial/${tutorialDetailsEntity.images.first()}.png")
                .build()
            context.imageLoader.enqueue(request)
        }
    }

    Column {
        Box {
            Image(
                painter = rememberAsyncImagePainter(
                    ImageRequest.Builder(
                        LocalContext.current
                    ).data(imageUrl).apply(block = fun ImageRequest.Builder.() {
                        size(Size.ORIGINAL)
                    }).build()
                ),
                contentScale = ContentScale.FillWidth,
                contentDescription = null,
                modifier = Modifier.fillMaxWidth()
            )
            if (detailsList.size > 1) {
                Column(
                    modifier = Modifier.matchParentSize(),
                    verticalArrangement = Arrangement.Center
                ) {
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.SpaceBetween
                    ) {
                        Box(
                            modifier = Modifier
                                .height(36.dp)
                                .width(36.dp)
                                .background(color = Color.Black.copy(alpha = .5f))
                                .clickable {
                                    if (detailsListIndex == 0) {
                                        detailsListIndex = detailsList.size - 1
                                    } else {
                                        detailsListIndex--
                                    }
                                    currentDetail = detailsList[detailsListIndex]
                                }
                        ) {
                            Column(
                                modifier = Modifier.fillMaxHeight(),
                                verticalArrangement = Arrangement.Center
                            ) {
                                Icon(
                                    modifier = Modifier.fillMaxSize(),
                                    imageVector = Icons.Filled.ArrowLeft,
                                    contentDescription = "",
                                    tint = TextIconColor
                                )
                            }
                        }
                        Box(
                            modifier = Modifier
                                .height(36.dp)
                                .width(36.dp)
                                .background(color = Color.Black.copy(alpha = .5f))
                                .clickable {
                                    if (detailsListIndex == detailsList.size - 1) {
                                        detailsListIndex = 0
                                    } else {
                                        detailsListIndex++
                                    }
                                    currentDetail = detailsList[detailsListIndex]
                                }
                        ) {
                            Column(
                                modifier = Modifier.fillMaxHeight(),
                                verticalArrangement = Arrangement.Center
                            ) {
                                Icon(
                                    modifier = Modifier.fillMaxSize(),
                                    imageVector = Icons.Filled.ArrowRight,
                                    contentDescription = "",
                                    tint = TextIconColor
                                )
                            }
                        }
                    }
                }
            }
        }
        HtmlText(
            text = viewModel.toHtmlText(currentDetail.description),
            modifier = Modifier.padding(16.dp),
            color = TextIconColor
        )
    }
}