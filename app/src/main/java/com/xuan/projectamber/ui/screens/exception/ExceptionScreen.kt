package com.xuan.projectamber.ui.screens.exception

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.xuan.projectamber.R
import com.xuan.projectamber.ui.theme.NavBackGround
import com.xuan.projectamber.ui.theme.TextIconColor

@Composable
fun ExceptionScreen (
    navController: NavController,
    exceptionInfo: String
) {
    val uriHandler = LocalUriHandler.current

    Surface(
        color = NavBackGround,
        modifier = Modifier
            .fillMaxSize()
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(8.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceBetween,
        ) {
            Column {
                Image(
                    painter = painterResource(R.drawable.error_sticker),
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 16.dp),
                    contentDescription = "Deadge",
                )
                Text(
                    text = stringResource(id = R.string.ExceptionMainBody),
                    fontSize = 24.sp,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier
                        .padding(bottom = 8.dp)
                        .fillMaxWidth(),
                    textAlign = TextAlign.Center
                )
                Text(
                    text = exceptionInfo
                )
            }
            Column {
                Text(
                    text = stringResource(id = R.string.ExceptionCheckConnectionBody)
                )
                Row (
                    modifier = Modifier.fillMaxWidth(),
                        ) {
                    IconButton(
                        content = {
                            Icon(
                                painter = painterResource(
                                    id = R.drawable.gitlab
                                ),
                                contentDescription = "",
                                tint = TextIconColor,
                                modifier = Modifier.size(24.dp)
                            )
                        },
                        onClick = {
                            uriHandler.openUri("https://gitlab.com/NightFire19/amber-project")
                        }
                    )
                }
            }
        }
    }
}