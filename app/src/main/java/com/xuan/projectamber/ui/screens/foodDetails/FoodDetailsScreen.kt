package com.xuan.projectamber.ui.screens.foodDetails

import android.util.Log
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.LocationOn
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.size.Size
import com.xuan.projectamber.R
import com.xuan.projectamber.ui.screens.materialDetails.SourceCard
import com.xuan.projectamber.ui.theme.*
import com.xuan.projectamber.domain.models.IngredientEntity
import com.xuan.projectamber.viewmodel.FoodDetailsViewModel

@Composable
fun FoodDetailsScreen(
    id: String,
    navController: NavController,
    viewModel: FoodDetailsViewModel = hiltViewModel()
) {
    val food = viewModel.foodState.collectAsState()

    val foodIconUrl = "https://api.ambr.top/assets/UI/${ food.value.food.value.icon }.png"

    viewModel.getFood(id)

    Surface(
        color = ScreenBackGround,
        modifier = Modifier
            .fillMaxSize()
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .verticalScroll(
                    rememberScrollState(),
                ),

            ) {
            Text(
                text = food.value.food.value.name,
                color = TextIconColor,
                fontWeight = FontWeight.Bold,
                fontSize = 24.sp,
                modifier = Modifier.padding(horizontal = 8.dp, vertical = 4.dp)
            )
            Card(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp),
                colors = CardDefaults.cardColors(containerColor = SecondaryBg),
            ) {
                Column {
                    Row(
                        horizontalArrangement = Arrangement.SpaceBetween,
                        modifier = Modifier
                            .padding(8.dp)
                            .height(128.dp)
                    ) {
                        Column(
                            modifier = Modifier
                                .weight(1f)
                                .fillMaxHeight(),
                            verticalArrangement = Arrangement.SpaceBetween,
                        ) {
                            Text(food.value.food.value.type)
                            LazyRow {
                                food.value.food.value.rank.let { count ->
                                    items(count) {
                                        Image(
                                            painter = painterResource(
                                                id = R.drawable.star
                                            ),
                                            contentDescription = "Star",
                                            modifier = Modifier
                                                .padding(
                                                    end = 4.dp,
                                                    top = 4.dp,
                                                    bottom = 8.dp
                                                )
                                                .size(24.dp),
                                        )
                                    }
                                }
                            }
                        }

                        Spacer(modifier = Modifier.weight(1f))

                        Image(
                            painter = rememberAsyncImagePainter(
                                ImageRequest.Builder(
                                    LocalContext.current
                                )
                                    .data(foodIconUrl)
                                    .apply(
                                        block = fun ImageRequest.Builder.() {
                                            size(Size.ORIGINAL)
                                        }
                                    )
                                    .build()
                            ),
                            contentScale = ContentScale.FillWidth,
                            contentDescription = null,
                            modifier = Modifier
                                .weight(1f)
                                .fillMaxSize()
                        )
                    }


                    Column(
                        modifier = Modifier
                            .background(TextIconColor)
                            .fillMaxWidth()
                            .padding(8.dp)
                    ) {

                        if (food.value.food.value.recipe.input.isNotEmpty()) {
                            val ingredients = food.value.food.value.recipe.input
                            Text(
                                text = stringResource(id = R.string.FoodDetailsIngredientsHeader),
                                color = NavBackGround,
                            )
                            Card(
                                modifier = Modifier.fillMaxWidth(),
                                colors = CardDefaults.cardColors(containerColor = Color.LightGray),
                                border = BorderStroke(2.dp, BorderColor),
                            ) {
                                LazyRow {
                                    items(count = ingredients.size) {
                                        Box(
                                            modifier = Modifier
                                                .size(width = 81.dp, height = Dp.Unspecified)
                                                .padding(8.dp)
                                        ) {
                                            IngredientCard(
                                                itemId = ingredients.keys.toList()[it],
                                                ingredientEntity = ingredients[ingredients.keys.toList()[it]]!!,
                                                navController = navController
                                            )
                                        }
                                    }
                                }

                            }
                        }

                        Text(
                            text = food.value.food.value.description,
                            color = NavBackGround
                        )
                        Spacer(modifier = Modifier.size(8.dp))
                        Card(
                            modifier = Modifier.fillMaxWidth(),
                            colors = CardDefaults.cardColors(containerColor = TextIconColor),
                            border = BorderStroke(2.dp, BorderColor),
                        ) {
                            Column {
                                if (food.value.food.value.source!!.isNotEmpty()) {
                                    Row(
                                        modifier = Modifier
                                            .fillMaxWidth()
                                            .padding(horizontal = 8.dp, vertical = 4.dp)
                                            .clickable {
                                                //TODO
                                            },
                                        horizontalArrangement = Arrangement.SpaceBetween,
                                    ) {
                                        Row {
                                            Icon(
                                                imageVector = Icons.Filled.LocationOn,
                                                contentDescription = "",
                                                tint = NavBackGround
                                            )

                                            Text(
                                                text = stringResource(R.string.FoodDetailsSourcesLabel),
                                                color = NavBackGround,
                                                fontWeight = FontWeight.Bold,
                                                modifier = Modifier.padding(horizontal = 8.dp)
                                            )
                                        }
                                    }

                                    Spacer(modifier = Modifier.padding(top = 4.dp))
                                    food.value.food.value.source.forEach { sourceEntity ->
                                        SourceCard(sourceEntity = sourceEntity)
                                    }
                                    Spacer(modifier = Modifier.padding(bottom = 8.dp))

                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun IngredientCard(
    itemId: String,
    ingredientEntity: IngredientEntity,
    navController: NavController,
) {
    val materialUrl = "https://api.ambr.top/assets/UI/${ ingredientEntity.icon }.png"
    Log.v("test2", itemId)
    Card(
        modifier = Modifier
            .clickable {
                navController.navigate(
                    "materialDetails/{id}"
                        .replace(
                            oldValue = "{id}",
                            newValue = itemId
                        )
                )
            },
        colors = CardDefaults.cardColors(containerColor = NavBackGround),
        shape = RoundedCornerShape(4.dp)
    ) {
        Column {
            Image(
                painter = rememberAsyncImagePainter(
                    ImageRequest.Builder(
                        LocalContext.current
                    )
                        .data(materialUrl)
                        .apply(
                            block = fun ImageRequest.Builder.() {
                                size(Size.ORIGINAL)
                            }
                        )
                        .build()
                ),
                contentDescription = null,
                modifier = Modifier.fillMaxWidth()
            )
            val quantityDisplayed = ingredientEntity.count.toString()
            Text(
                text = quantityDisplayed,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .background(TextIconColor)
                    .fillMaxWidth(),
                color = ScreenBackGround
            )
        }
    }
}