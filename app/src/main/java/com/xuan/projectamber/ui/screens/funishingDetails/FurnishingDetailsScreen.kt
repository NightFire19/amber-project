package com.xuan.projectamber.ui.screens.funishingDetails

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.size.Size
import com.xuan.projectamber.R
import com.xuan.projectamber.ui.theme.*
import com.xuan.projectamber.ui.screens.foodDetails.IngredientCard
import com.xuan.projectamber.util.Constants
import com.xuan.projectamber.viewmodel.FurnitureDetailsViewModel

@Composable
fun FurnishingDetailsScreen(
    id: String,
    navController: NavController,
    viewModel: FurnitureDetailsViewModel = hiltViewModel()
) {
    val furniture = viewModel.furniture

    val furnitureIconUrl = "https://api.ambr.top/assets/UI/furniture/" + furniture.icon + ".png"

    viewModel.getFurniture(id)

    Surface(
        color = ScreenBackGround,
        modifier = Modifier
            .fillMaxSize()
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .verticalScroll(
                    rememberScrollState(),
                ),

            ) {
            Text(
                text = furniture.name,
                color = TextIconColor,
                fontWeight = FontWeight.Bold,
                fontSize = 24.sp,
                modifier = Modifier.padding(horizontal = 8.dp, vertical = 4.dp)
            )
            Card(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp),
                colors = CardDefaults.cardColors(containerColor = SecondaryBg),
            ) {
                Column(
                    modifier = Modifier.background(
                        brush = Brush.horizontalGradient(
                            colors = Constants.RANK_COLORS[furniture.rank.toString()] ?: listOf(
                                SecondaryBg,
                                SecondaryBg
                            )
                        )
                    )
                ) {
                    Row(
                        horizontalArrangement = Arrangement.SpaceBetween,
                        modifier = Modifier
                            .padding(8.dp)
                            .height(128.dp)
                    ) {
                        Column(
                            modifier = Modifier
                                .weight(2f)
                                .fillMaxHeight(),
                            verticalArrangement = Arrangement.SpaceBetween,
                        ) {
                            Column {
                                if (furniture.categories.isNotEmpty()) {
                                    Text(
                                        text = furniture.categories[0],
                                        fontWeight = FontWeight.Bold
                                    )
                                }
                                if (furniture.types.isNotEmpty()) {
                                    Text(
                                        text = furniture.types[0],
                                        fontSize = 12.sp
                                    )
                                }
                            }

                            Column {
                                Row {
                                    Text(
                                        text = stringResource(id = R.string.FurnishingDetailsEnergyLabel),
                                        fontWeight = FontWeight.SemiBold
                                    )
                                    Text(text = " ${furniture.comfort}")
                                }
                                Row {
                                    Text(
                                        text = stringResource(id = R.string.FurnishingDetailsLoadLabel),
                                        fontWeight = FontWeight.SemiBold
                                    )
                                    if (furniture.cost == null || furniture.cost == 0) {
                                        Text(
                                            text = stringResource(id = R.string.FurnishingDetailsNoCostLabel),
                                            modifier = Modifier.padding(start = 4.dp)
                                        )
                                    } else {
                                        Text(" ${furniture.cost}")
                                    }
                                }
                            }

                            LazyRow {
                                items(furniture.rank) {
                                    Image(
                                        painter = painterResource(
                                            id = R.drawable.star
                                        ),
                                        contentDescription = "Star",
                                        modifier = Modifier
                                            .padding(
                                                end = 4.dp,
                                                top = 4.dp,
                                                bottom = 8.dp
                                            )
                                            .size(24.dp),
                                    )
                                }
                            }

                        }

                        Image(
                            painter = rememberAsyncImagePainter(
                                ImageRequest.Builder(
                                    LocalContext.current
                                )
                                    .data(furnitureIconUrl)
                                    .apply(
                                        block = fun ImageRequest.Builder.() {
                                            size(Size.ORIGINAL)
                                        }
                                    )
                                    .build()
                            ),
                            contentScale = ContentScale.FillWidth,
                            contentDescription = null,
                            modifier = Modifier
                                .weight(1f)
                                .fillMaxSize()
                        )
                    }


                    Column(
                        modifier = Modifier
                            .background(TextIconColor)
                            .fillMaxWidth()
                            .padding(8.dp)
                    ) {
                        if (furniture.recipe != null) {
                            Text(
                                text = stringResource(
                                    id = R.string.FurnishingDetailsRecipeExpLabel,
                                    furniture.recipe.exp
                                ), //"\u2022 Trust: ${furniture.recipe.exp}"
                                fontWeight = FontWeight.Bold,
                                color = NavBackGround
                            )
                            Text(
                                text = stringResource(
                                    id = R.string.FurnishingDetailsRecipeTimeLabel,
                                    furniture.recipe.time / 360
                                ), //creation time furniture.recipe.time/360
                                fontWeight = FontWeight.Bold,
                                color = NavBackGround
                            )
                            val ingredients = furniture.recipe.input
                            Text(
                                text = stringResource(id = R.string.FurnishingDetailsRecipeIngredientsLabel),
                                color = NavBackGround,
                            )
                            Card(
                                modifier = Modifier.fillMaxWidth(),
                                colors = CardDefaults.cardColors(containerColor = Color.LightGray),
                            ) {
                                LazyRow {
                                    items(count = ingredients.size) {
                                        Box(
                                            modifier = Modifier
                                                .size(width = 81.dp, height = Dp.Unspecified)
                                                .padding(8.dp)
                                        ) {
                                            IngredientCard(
                                                itemId = ingredients.keys.toList()[it],
                                                ingredientEntity = ingredients[ingredients.keys.toList()[it]]!!,
                                                navController = navController
                                            )
                                        }
                                    }
                                }
                            }
                        }

                        Text(
                            text = furniture.description,
                            color = NavBackGround
                        )
                        Spacer(modifier = Modifier.size(8.dp))
                    }
                }
            }
        }
    }
}