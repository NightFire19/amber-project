package com.xuan.projectamber.ui.screens.home

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.Tab
import androidx.compose.material3.TabRow
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.xuan.projectamber.R
import com.xuan.projectamber.ui.screens.home.components.serverReset.ServerResetTab
import com.xuan.projectamber.ui.screens.home.components.dailyfarming.DailyFarming
import com.xuan.projectamber.ui.screens.home.components.notices.Notices
import com.xuan.projectamber.ui.screens.home.components.notices.components.NoticeDialog
import com.xuan.projectamber.ui.theme.BorderColor
import com.xuan.projectamber.ui.theme.ScreenBackGround
import com.xuan.projectamber.ui.theme.SecondaryBg
import com.xuan.projectamber.util.Constants.SERVER_REGIONS
import com.xuan.projectamber.viewmodel.HomeViewModel

@Composable
fun HomeScreen(
    navController: NavController,
    viewModel: HomeViewModel = hiltViewModel()
) {

    val dailyDungeons = viewModel.dailyDungeons.collectAsState()
    val upgrades = viewModel.upgrades.collectAsState()
    val errorState = viewModel.isException.first
    val errorMessage = viewModel.isException.second

    if (dailyDungeons.value.daysOfWeek != null && upgrades.value.isNotEmpty()) {
        viewModel.createUpgradeMap()
    }

    if (errorState) {
        viewModel.errorCaught()
        navController.navigate(
            "exception/${errorMessage}"
        )
    }

    Box(
        modifier = Modifier
            .fillMaxSize()
    ) {
        Column(
            modifier = Modifier
                .verticalScroll(rememberScrollState())
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(color = ScreenBackGround)
                    .padding(8.dp),
            ) {
                Text(
                    text = stringResource(id = R.string.HomeTitle),
                    fontWeight = FontWeight.ExtraBold,
                    fontSize = 36.sp
                )
                Text(
                    text = stringResource(id = R.string.HomeSubTitle),
                    fontWeight = FontWeight.SemiBold,
                    fontSize = 18.sp,
                    modifier = Modifier.padding(bottom = 16.dp)
                )
                Card(
                    colors = CardDefaults.cardColors(containerColor = SecondaryBg),
                    border = BorderStroke(2.dp, BorderColor),
                    modifier = Modifier.fillMaxWidth()
                ) {
                    Text(
                        text = stringResource(id = R.string.HomeIntroduction),
                        modifier = Modifier.padding(8.dp),
                    )
                }
                ServerReset(
                    viewModel = viewModel
                )
                Notices(
                    viewModel = viewModel
                )
                DailyFarming(
                    navController = navController,
                    viewModel = viewModel,
                )
            }
        }
        NoticeDialog(viewModel = viewModel, eventEntity = viewModel.noticeDialogEvent.value)
    }
}

@Composable
fun ServerReset(
    viewModel: HomeViewModel
) {
    var serverRegion by remember { mutableStateOf(0) }
    val tabRegions = SERVER_REGIONS

    Text(
        text = stringResource(id = R.string.HomeServerResetHeader),
        fontWeight = FontWeight.Bold,
        fontSize = 24.sp,
        modifier = Modifier.padding(vertical = 16.dp)
    )
    Card(
        colors = CardDefaults.cardColors(containerColor = SecondaryBg),
        border = BorderStroke(2.dp, BorderColor),
        modifier = Modifier
            .fillMaxWidth()
    ) {
        Column {
            TabRow(
                selectedTabIndex = serverRegion,
                indicator = {},
                divider = {
                    Divider(
                        modifier = Modifier.size(2.dp),
                        color = BorderColor
                    )
                }
            ) { // 3.
                tabRegions.forEachIndexed { index, title ->
                    Tab(
                        selected = serverRegion == index,
                        onClick = {
                            serverRegion = index
                        },
                        text = {
                            Text(
                                text = stringResource(id = title)
                            )
                        },
                        modifier = Modifier
                            .background(
                                if (serverRegion == index) {
                                    BorderColor
                                } else {
                                    SecondaryBg
                                }
                            )
                    )
                }
            }
            Row(
                modifier = Modifier
                    .padding(16.dp)
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                ServerResetTab(
                    tabIndex = serverRegion,
                    viewModel = viewModel,
                )
            }
        }
    }
}