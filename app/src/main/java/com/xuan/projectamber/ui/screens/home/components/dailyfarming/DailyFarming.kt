package com.xuan.projectamber.ui.screens.home.components.dailyfarming

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import coil.compose.rememberAsyncImagePainter
import com.xuan.projectamber.R
import com.xuan.projectamber.ui.screens.home.components.dailyfarming.components.DayDropdown
import com.xuan.projectamber.ui.screens.home.components.dailyfarming.components.UpgradeCard
import com.xuan.projectamber.ui.theme.BorderColor
import com.xuan.projectamber.ui.theme.ScreenBackGround
import com.xuan.projectamber.ui.theme.SecondaryBg
import com.xuan.projectamber.util.Constants
import com.xuan.projectamber.viewmodel.HomeViewModel

@OptIn(ExperimentalLayoutApi::class)
@Composable
fun DailyFarming(
    navController: NavController,
    viewModel: HomeViewModel
) {
    val dungeonList = viewModel.currentDungeonList

    Column() {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 16.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = stringResource(id = R.string.HomeDailyFarmingHeader),
                fontWeight = FontWeight.Bold,
                fontSize = 24.sp,
                modifier = Modifier
                    .weight(3f)
            )
            Spacer(modifier = Modifier.weight(1f))
            Column(
                modifier = Modifier.weight(2f),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                DayDropdown(viewModel)
            }
        }
        Card(
            colors = CardDefaults.cardColors(containerColor = SecondaryBg),
            border = BorderStroke(2.dp, BorderColor),
            modifier = Modifier
                .fillMaxWidth()
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .heightIn(0.dp, 512.dp)
                    .verticalScroll(rememberScrollState())
            ) {
                if (viewModel.isSunday.value) {
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(16.dp),
                        horizontalArrangement = Arrangement.Center
                    ) {
                        Text(
                            text = stringResource(id = R.string.HomeDailyFarmingSunday)
                        )
                    }
                } else {
                    dungeonList.forEach{ dungeon ->
                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(4.dp)
                                .background(color = ScreenBackGround)
                                .padding(8.dp),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            val url = "https://api.ambr.top/assets/UI/UI_ItemIcon_${dungeon.icon}.png"
                            Image(
                                painter = rememberAsyncImagePainter(url),
                                contentScale = ContentScale.FillWidth,
                                contentDescription = null,
                                modifier = Modifier
                                    .size(36.dp)
                                    .padding(4.dp)
                            )
                            Text(
                                text = dungeon.name.substringAfter(":")+
                                    " (${ Constants.CITIES[dungeon.city - 1] }) "
                            )
                        }

                        FlowRow(
                            modifier = Modifier
                                .padding(4.dp)
                                .fillMaxWidth()
                        ) {
                            dungeon.upgrades.forEach {simpleUpgradeEntity ->
                                UpgradeCard(
                                    upgradeId = simpleUpgradeEntity.id,
                                    upgradeUrl = simpleUpgradeEntity.imageUrl,
                                    upgradeRank = simpleUpgradeEntity.rank,
                                    isCharacter = simpleUpgradeEntity.isCharacter,
                                    navController = navController
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}
