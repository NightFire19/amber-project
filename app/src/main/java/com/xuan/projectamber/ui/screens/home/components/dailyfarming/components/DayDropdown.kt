package com.xuan.projectamber.ui.screens.home.components.dailyfarming.components

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.xuan.projectamber.ui.theme.BorderColor
import com.xuan.projectamber.ui.theme.SecondaryBg
import com.xuan.projectamber.ui.theme.TextIconColor
import com.xuan.projectamber.viewmodel.HomeViewModel
import java.time.DayOfWeek
import java.util.Calendar

@Composable
fun DayDropdown (
    viewModel: HomeViewModel
){
    var expanded by remember { mutableStateOf(false) }
    val items = DayOfWeek.values()
    var dayOfWeek = Calendar.getInstance().get(Calendar.DAY_OF_WEEK)-2
    if (dayOfWeek==-1) dayOfWeek = 6
    var selectedIndex by remember { mutableStateOf(dayOfWeek) }
    Card(
        border = BorderStroke(2.dp, BorderColor)
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .clickable(onClick = { expanded = true })
                .background(
                    SecondaryBg
                )
                .padding(4.dp),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                items[selectedIndex].name.lowercase().replaceFirstChar { it.uppercase() },
            )
            Icon(
                imageVector = Icons.Filled.ArrowDropDown,
                contentDescription = "",
                tint = TextIconColor
            )
        }
        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
            modifier = Modifier
                .background(
                    SecondaryBg
                )
        ) {
            items.forEachIndexed { index, s ->
                DropdownMenuItem(
                    onClick = {
                        selectedIndex = index
                        expanded = false

                        if (index !=6) {
                            viewModel.getDailyDungeon(index+2)
                            viewModel.isSunday.value=false

                        } else {
                            viewModel.isSunday.value=true
                        }
                    },
                    text = { Text(s.name.lowercase().replaceFirstChar { it.uppercase() }) }
                )
            }
        }
    }
}