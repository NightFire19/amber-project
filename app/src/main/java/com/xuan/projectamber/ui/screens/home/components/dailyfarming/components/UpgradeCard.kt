package com.xuan.projectamber.ui.screens.home.components.dailyfarming.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import coil.compose.AsyncImagePainter
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.size.Size
import com.xuan.projectamber.ui.theme.NavBackGround
import com.xuan.projectamber.ui.theme.SecondaryBg
import com.xuan.projectamber.ui.theme.TextIconColor
import com.xuan.projectamber.util.Constants


@Composable
fun UpgradeCard(
    upgradeId: String,
    upgradeUrl: String,
    isCharacter: Boolean,
    navController: NavController,
    upgradeRank: Int,
) {
    Card(
        modifier = Modifier
            .padding(4.dp)
            .width(44.dp)
            .clickable(
                onClick = {
                    if (!isCharacter) {
                        navController.navigate(
                            "weaponDetails/{id}"
                                .replace(
                                    oldValue = "{id}",
                                    newValue = upgradeId
                                )
                        )
                    } else {
                        navController.navigate(
                            "characterDetails/{id}"
                                .replace(
                                    oldValue = "{id}",
                                    newValue = upgradeId
                                )
                        )
                    }
                }
            ),
        colors = CardDefaults.cardColors(containerColor = NavBackGround),
        shape = RoundedCornerShape(4.dp)
    ) {
        val painter = rememberAsyncImagePainter(
            ImageRequest.Builder(
                LocalContext.current
            )
                .data(upgradeUrl)
                .apply(
                    block = fun ImageRequest.Builder.() {
                        size(Size.ORIGINAL)
                    }
                )
                .build()
        )
        val painterState = painter.state

        if (painterState is AsyncImagePainter.State.Loading) {
            CircularProgressIndicator(
                modifier = Modifier.padding(2.dp),
                color = TextIconColor
            )
        }
        Box(
            modifier = Modifier.background(
                brush = Brush.horizontalGradient(
                    colors = Constants.RANK_COLORS[upgradeRank.toString()] ?: listOf(
                        SecondaryBg,
                        SecondaryBg
                    )
                )
            )
        ) {
            Image(
                painter = painter,
                contentDescription = null,
                modifier = Modifier.fillMaxSize()
            )
        }
    }
}