package com.xuan.projectamber.ui.screens.home.components.notices

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.CalendarToday
import androidx.compose.material3.*
import androidx.compose.material3.TabRowDefaults.tabIndicatorOffset
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.xuan.projectamber.R
import com.xuan.projectamber.ui.theme.BorderColor
import com.xuan.projectamber.ui.theme.SecondaryBg
import com.xuan.projectamber.ui.theme.TextIconColor
import com.xuan.projectamber.util.Constants.NOTICE_CATEGORIES
import com.xuan.projectamber.viewmodel.HomeViewModel

@Composable
fun Notices(
    viewModel: HomeViewModel
) {

    val tabCategory = NOTICE_CATEGORIES

    Column {
        Text(
            text = stringResource(id = R.string.HomeNoticeHeader),
            fontWeight = FontWeight.Bold,
            fontSize = 24.sp,
            modifier = Modifier.padding(vertical = 16.dp)
        )
        Card(
            colors = CardDefaults.cardColors(containerColor = SecondaryBg),
            border = BorderStroke(2.dp, BorderColor),
            modifier = Modifier
                .fillMaxWidth()
        ) {
            Column {
                TabRow(
                    selectedTabIndex = viewModel.noticeCategory.value,
                    containerColor = BorderColor,
                    indicator = { tabPositions ->
                        TabRowDefaults.Indicator(
                            color = BorderColor,
                            modifier = Modifier.tabIndicatorOffset(tabPositions[viewModel.noticeCategory.value])
                        )
                    },
                    divider = {},
                ) { // 3.
                    tabCategory.forEachIndexed { index, title ->
                        Tab(
                            selected = viewModel.noticeCategory.value == index,
                            onClick = {
                                viewModel.noticeCategory.value = index
                                when (viewModel.noticeCategory.value) {
                                    0 -> viewModel.getBanners()
                                    1 -> viewModel.getEvents()
                                    2 -> viewModel.getOther()
                                }
                            },
                            text = {
                                Text(
                                    text = stringResource(title)
                                )
                            },
                            modifier = Modifier
                                .background(
                                    SecondaryBg
                                ),
                        )
                    }
                }

                val listOfEvents = viewModel.eventList

                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(256.dp)
                        .verticalScroll(rememberScrollState())
                ) {
                    listOfEvents.forEach { event ->
                        if (event != null) {
                            Box(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .clickable(
                                        onClick = {
                                            viewModel.noticeDialogVisibility.value = true
                                            viewModel.noticeDialogEvent.value = event
                                        }
                                    )
                                    .drawBehind {
                                        drawLine(
                                            color = BorderColor,
                                            start = Offset(0f, size.height),
                                            end = Offset(size.width, size.height),
                                            strokeWidth = 2.dp.toPx()
                                        )
                                    },
                            ) {
                                Row(
                                    modifier = Modifier.padding(4.dp),
                                    verticalAlignment = Alignment.CenterVertically
                                ) {
                                    Icon(
                                        imageVector = Icons.Filled.CalendarToday,
                                        contentDescription = null,
                                        tint = TextIconColor,
                                        modifier = Modifier.padding(8.dp)
                                    )
                                    Text(
                                        text = event.nameFull.nameFull
                                    )
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
