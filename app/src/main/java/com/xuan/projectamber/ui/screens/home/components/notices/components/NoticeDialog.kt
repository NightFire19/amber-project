package com.xuan.projectamber.ui.screens.home.components.notices.components

import android.util.Log
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.size.Size
import com.xuan.projectamber.domain.models.EventEntity
import com.xuan.projectamber.ui.theme.BorderColor
import com.xuan.projectamber.ui.theme.SecondaryBg
import com.xuan.projectamber.ui.theme.TextIconColor
import com.xuan.projectamber.viewmodel.HomeViewModel

@Composable
fun NoticeDialog (
    viewModel: HomeViewModel,
    eventEntity: EventEntity?
) {
    if (viewModel.noticeDialogVisibility.value) {
        Box(modifier = Modifier.fillMaxSize()) {

            Surface(
                color = Color.Black.copy(alpha = 0.6f),
                modifier = Modifier
                    .fillMaxSize()
                    .clickable(
                        onClick = {
                            viewModel.noticeDialogVisibility.value = false
                        }
                    )
            ) {

            }

            Card(
                colors = CardDefaults.cardColors(containerColor = SecondaryBg),
                border = BorderStroke(2.dp, BorderColor),
                modifier = Modifier
                    .fillMaxSize()
                    .padding(vertical = 64.dp, horizontal = 32.dp)
            ) {
                if (eventEntity != null) {
                    Column() {
                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .background(BorderColor)
                                .padding(8.dp),
                            horizontalArrangement = Arrangement.SpaceBetween,
                            verticalAlignment = Alignment.CenterVertically

                        ) {

                            Text(
                                text = eventEntity.name.name,
                                fontWeight = FontWeight.Bold,
                                fontSize = 16.sp,
                            )
                            OutlinedButton(
                                onClick = {
                                    viewModel.noticeDialogVisibility.value = false
                                          },
                                modifier = Modifier
                                    .size(36.dp),
                                shape = CircleShape,
                                border= BorderStroke(2.dp, Color.Gray),
                                contentPadding = PaddingValues(8.dp),
                                colors = ButtonDefaults.outlinedButtonColors(
                                    containerColor =  TextIconColor,
                                    contentColor = Color.Black)
                            ) {
                                Icon(
                                    imageVector = Icons.Filled.Close,
                                    contentDescription = "close",
                                )
                            }
                        }
                        Column(
                            modifier = Modifier
                                .fillMaxSize()
                                .verticalScroll(
                                    rememberScrollState()
                                )
                                .padding(8.dp),
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            Box(modifier = Modifier.fillMaxWidth()) {
                                Image(
                                    painter = rememberAsyncImagePainter(
                                        ImageRequest.Builder(
                                            LocalContext.current
                                        ).data(eventEntity.banner.banner).apply(block = fun ImageRequest.Builder.() {
                                            size(Size.ORIGINAL)
                                        }).build()
                                    ),
                                    contentScale = ContentScale.FillWidth,
                                    contentDescription = null,
                                    modifier = Modifier.fillMaxWidth()
                                )
                            }

                            Text("\n"+viewModel.htmlToText(eventEntity.description.description))
                        }
                    }
                }
            }
        }
    }
}