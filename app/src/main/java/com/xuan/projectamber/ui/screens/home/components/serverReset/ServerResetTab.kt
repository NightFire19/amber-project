package com.xuan.projectamber.ui.screens.home.components.serverReset


import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.xuan.projectamber.viewmodel.HomeViewModel

@Composable
fun ServerResetTab (
    tabIndex: Int,
    viewModel: HomeViewModel,
){

    val europeTimer: String by viewModel.timeUntilEuropeReset.observeAsState(initial = "")
    val americaTimer: String by viewModel.timeUntilAmericaReset.observeAsState(initial = "")
    val asiaTimer: String by viewModel.timeUntilAsiaReset.observeAsState(initial = "")

    when (tabIndex) {
        0 -> {
            Text(europeTimer, fontWeight = FontWeight.SemiBold, fontSize = 24.sp)
        }
        1 -> {
            Text(americaTimer, fontWeight = FontWeight.SemiBold, fontSize = 24.sp)
        }
        2 -> {
            Text(asiaTimer, fontWeight = FontWeight.SemiBold, fontSize = 24.sp)
        }
    }
}