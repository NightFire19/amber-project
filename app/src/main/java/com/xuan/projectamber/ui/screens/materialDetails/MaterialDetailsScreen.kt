package com.xuan.projectamber.ui.screens.materialDetails

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.filled.ArrowDropUp
import androidx.compose.material.icons.filled.LocationOn
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.size.Size
import com.xuan.projectamber.R
import com.xuan.projectamber.domain.models.SourceEntity
import com.xuan.projectamber.ui.common.AscensionMaterialCard
import com.xuan.projectamber.ui.theme.BorderColor
import com.xuan.projectamber.ui.theme.DarkGrayTextColor
import com.xuan.projectamber.ui.theme.NavBackGround
import com.xuan.projectamber.ui.theme.ScreenBackGround
import com.xuan.projectamber.ui.theme.SecondaryBg
import com.xuan.projectamber.ui.theme.TextIconColor
import com.xuan.projectamber.util.Constants
import com.xuan.projectamber.viewmodel.MaterialDetailsViewModel
import java.util.Locale

@Composable
fun MaterialDetailsScreen(
    id: String,
    navController: NavController,
    viewModel: MaterialDetailsViewModel = hiltViewModel()
) {
    val material = viewModel.materialState.collectAsState()
    val materialUrl =
        "https://api.ambr.top/assets/UI/" + material.value.material.value.icon + ".png"
    viewModel.getMaterial(id)
    Surface(
        color = ScreenBackGround,
        modifier = Modifier
            .fillMaxSize()
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .verticalScroll(
                    rememberScrollState(),
                ),

            ) {
            Text(
                text = material.value.material.value.name,
                color = TextIconColor,
                fontWeight = FontWeight.Bold,
                fontSize = 24.sp,
                modifier = Modifier.padding(horizontal = 8.dp, vertical = 4.dp)
            )

            Card(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp),
                colors = CardDefaults.cardColors(containerColor = SecondaryBg),
            ) {
                Column(
                    modifier = Modifier.background(
                        brush = Brush.horizontalGradient(
                            colors = Constants.RANK_COLORS[material.value.material.value.rank.toString()] ?: listOf(
                                SecondaryBg,
                                SecondaryBg
                            )
                        )
                    )
                ) {
                    Row(
                        horizontalArrangement = Arrangement.SpaceBetween,
                        modifier = Modifier
                            .height(128.dp)
                    ) {
                        Column(
                            modifier = Modifier
                                .weight(2f)
                                .padding(8.dp)
                                .fillMaxHeight(),
                            verticalArrangement = Arrangement.SpaceBetween,
                        ) {
                            Text(material.value.material.value.type)
                            LazyRow {
                                material.value.material.value.rank.let { count ->
                                    items(count) {
                                        Image(
                                            painter = painterResource(
                                                id = R.drawable.star
                                            ),
                                            contentDescription = "Star",
                                            modifier = Modifier
                                                .padding(
                                                    end = 4.dp,
                                                    top = 4.dp,
                                                    bottom = 8.dp
                                                )
                                                .size(24.dp),
                                        )
                                    }
                                }
                            }

                        }
                        Image(
                            painter = rememberAsyncImagePainter(
                                ImageRequest.Builder(
                                    LocalContext.current
                                )
                                    .data(materialUrl)
                                    .apply(
                                        block = fun ImageRequest.Builder.() {
                                            size(Size.ORIGINAL)
                                        }
                                    )
                                    .build()
                            ),
                            contentScale = ContentScale.FillWidth,
                            contentDescription = null,
                            modifier = Modifier.weight(1f)
                        )
                    }

                    Column(
                        modifier = Modifier
                            .background(TextIconColor)
                            .fillMaxWidth()
                            .padding(8.dp)
                    ) {
                        Text(
                            text = material.value.material.value.description.replace("\\n", "\n"),
                            color = NavBackGround
                        )
                        Spacer(modifier = Modifier.size(8.dp))
                        Card(
                            modifier = Modifier.fillMaxWidth(),
                            colors = CardDefaults.cardColors(containerColor = TextIconColor),
                            border = BorderStroke(2.dp, DarkGrayTextColor),
                        ) {
                            Column (
                                modifier = Modifier.padding(vertical = 8.dp)
                                    ) {
                                if (material.value.material.value.source!!.isNotEmpty()) {
                                    Row(
                                        modifier = Modifier
                                            .fillMaxWidth()
                                            .padding(horizontal = 8.dp, vertical = 4.dp)
                                            .clickable {
                                                viewModel.onSourceClicked()
                                            },
                                        horizontalArrangement = Arrangement.SpaceBetween,
                                    ) {
                                        Row {
                                            Icon(
                                                imageVector = Icons.Filled.LocationOn,
                                                contentDescription = "",
                                                tint = DarkGrayTextColor
                                            )

                                            Text(
                                                text = stringResource(id = R.string.MaterialDetailsSourcesLabel),
                                                color = DarkGrayTextColor,
                                                fontWeight = FontWeight.Bold,
                                                modifier = Modifier.padding(horizontal = 8.dp)
                                            )
                                        }
                                        if (material.value.sourceVisibility.value) {
                                            Icon(
                                                imageVector = Icons.Filled.ArrowDropUp,
                                                contentDescription = "",
                                                tint = DarkGrayTextColor
                                            )
                                        } else {
                                            Icon(
                                                imageVector = Icons.Filled.ArrowDropDown,
                                                contentDescription = "",
                                                tint = DarkGrayTextColor
                                            )
                                        }
                                    }
                                    AnimatedVisibility(
                                        visible = material.value.sourceVisibility.value,
                                    ) {
                                        material.value.material.value.source?.forEach { sourceEntity ->
                                            SourceCard(sourceEntity = sourceEntity)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (material.value.recipeMap.value.isNotEmpty()) {
                Text(
                    text = stringResource(id = R.string.MaterialDetailsRecipeHeader),
                    color = TextIconColor,
                    fontWeight = FontWeight.Bold,
                    fontSize = 24.sp,
                    modifier = Modifier.padding(horizontal = 8.dp, vertical = 4.dp)
                )
                Card(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(8.dp),
                    colors = CardDefaults.cardColors(containerColor = SecondaryBg),
                    border = BorderStroke(2.dp, BorderColor),

                    ) {
                    Column {
                        material.value.recipeMap.value.keys.toList().forEach { rowIndex ->
                            Row {
                                material.value.recipeMap.value[rowIndex]!!.keys.forEach { materialId ->
                                    Box(
                                        modifier = Modifier
                                            .size(width = 81.dp, height = Dp.Unspecified)
                                            .padding(8.dp)
                                    ) {
                                        AscensionMaterialCard(
                                            itemId = materialId,
                                            quantity = material.value.recipeMap.value[rowIndex]!![materialId]!!.count,
                                            navController = navController
                                        )
                                    }
                                }
                            }
                            if (rowIndex != material.value.recipeMap.value.keys.toList()[material.value.recipeMap.value.size - 1]) {
                                Divider(color = BorderColor)
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun SourceCard(
    sourceEntity: SourceEntity
) {
    var sourceString: String = sourceEntity.name

    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 4.dp, horizontal = 16.dp),
        colors = CardDefaults.cardColors(containerColor = DarkGrayTextColor),
        shape = RoundedCornerShape(8.dp)
    ) {
        Row {
            if (sourceEntity.days!!.isNotEmpty()) {
                sourceString += " ("
                sourceEntity.days.forEachIndexed { index, day ->
                    sourceString += day.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.ROOT) else it.toString() }

                    if (index != sourceEntity.days.size - 1) {
                        sourceString += ", "
                    }
                }
                sourceString += ")"
            }
            Text(
                modifier = Modifier.padding(8.dp),
                text = sourceString,
            )
        }
    }
}

