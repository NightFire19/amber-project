package com.xuan.projectamber.ui.screens.monsterDetails

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.size.Size
import com.xuan.projectamber.R
import com.xuan.projectamber.domain.models.EntryEntity
import com.xuan.projectamber.domain.models.RewardEntity
import com.xuan.projectamber.ui.common.CustomSliderColors
import com.xuan.projectamber.ui.theme.*
import com.xuan.projectamber.viewmodel.MonsterDetailsViewModel
import com.xuan.projectamber.viewmodel.MonsterScreenState
import de.charlex.compose.HtmlText

@Composable
fun MonsterDetailsScreen(
    id: String,
    navController: NavController,
    viewModel: MonsterDetailsViewModel = hiltViewModel(),
) {
    viewModel.getMonster(id)

    val monsterState = viewModel.monsterState.collectAsState()
    val monsterUrl = "https://api.ambr.top/assets/UI/monster/"+monsterState.value.monster.icon+".png"
    val configuration = LocalConfiguration.current
    val width = configuration.screenWidthDp/8-8

    Surface(
        color = ScreenBackGround,
        modifier = Modifier
            .fillMaxSize()
    ) {
        Column (
            modifier = Modifier.verticalScroll(rememberScrollState())
        ) {
            Row (
                modifier = Modifier
                    .padding(8.dp)
                    .fillMaxWidth(),
            ) {
                Image(
                    painter = rememberAsyncImagePainter(
                        ImageRequest.Builder(
                            LocalContext.current
                        )
                            .data(monsterUrl)
                            .apply(
                                block = fun ImageRequest.Builder.() {
                                    size(Size.ORIGINAL)
                                }
                            )
                            .build()
                    ),
                    contentDescription = null,
                    modifier = Modifier
                        .size(64.dp)
                        .background(
                            color = SecondaryBg,
                        )
                        .clip(RoundedCornerShape(4.dp))
                        .border(2.dp, BorderColor, RoundedCornerShape(4.dp))
                )
                Column (
                    modifier = Modifier.padding(start = 8.dp)
                ) {
                    Text(
                        text = monsterState.value.monster.type,
                        style = TextStyle(textDecoration = TextDecoration.Underline)
                    )
                    Text(
                        text = monsterState.value.monster.name,
                        fontWeight = FontWeight.Bold
                    )
                    Text(
                        text = monsterState.value.monster.specialName
                    )
                }
            }
            Card(
                modifier = Modifier
                    .padding(8.dp),
                colors = CardDefaults.cardColors(containerColor = SecondaryBg),
                border = BorderStroke(2.dp, BorderColor),
            ) {
                val monsterDescription = monsterState.value.monster.description.replace("\\n","\n")
                HtmlText(
                    text = monsterDescription,
                    color = TextIconColor,
                    modifier = Modifier.padding(8.dp)
                )
            }

            if (monsterState.value.currentEntry.value?.reward  != null) {
                Rewards(
                    entry = monsterState.value.currentEntry.value,
                    navController = navController,
                    width = width
                )
            }

            Text(
                text = stringResource(id = R.string.MonsterDetailsStatsHeader),
                fontWeight = FontWeight.Bold,
                modifier = Modifier.padding(start = 8.dp)
            )
            Card(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp),
                colors = CardDefaults.cardColors(containerColor = SecondaryBg),
                border = BorderStroke(2.dp, BorderColor),
            ) {
                Column (
                    modifier = Modifier.padding(8.dp)
                ) {
                    if (monsterState.value.monster.entries.size>1) {

                        Column {
                            Card(
                                modifier = Modifier.clickable {
                                    viewModel.variantSelectionDropdownVisibility.value = true
                                },
                                shape = RoundedCornerShape(2.dp)
                            ) {
                                Row (modifier = Modifier.padding(8.dp)) {
                                    Text(
                                        text = stringResource(id = R.string.MonsterDetailsVariantDropdownLabel, monsterState.value.variant)
                                    )
                                    Spacer(modifier = Modifier.padding(horizontal = 64.dp))
                                    Icon(
                                        imageVector = Icons.Filled.ArrowDropDown,
                                        contentDescription = "",
                                        tint = TextIconColor
                                    )
                                }
                                DropdownMenu(
                                    expanded = viewModel.variantSelectionDropdownVisibility.value,
                                    onDismissRequest = {
                                        viewModel.variantSelectionDropdownVisibility.value = false
                                    }
                                ) {
                                    for (i in 1..monsterState.value.monster.entries.size) {
                                        DropdownMenuItem(
                                            onClick = {
                                                viewModel.onVariantChange(i)
                                            },
                                            text = {
                                                Text(
                                                    text = stringResource(id = R.string.MonsterDetailsVariantDropdownLabel, i)
                                                )
                                            }
                                        )
                                    }
                                }
                            }
                        }
                    }
                    EntryDetails(
                        entry = monsterState.value.currentEntry.value,
                        viewModel = viewModel,
                        state = monsterState,
                    )
                }
            }

            if (monsterState.value.monster.tips != null) {
                Text(
                    text = stringResource(id = R.string.MonsterDetailsTipsHeader),
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier.padding(start = 8.dp)
                )
                Card(
                    modifier = Modifier
                        .padding(8.dp),
                    colors = CardDefaults.cardColors(containerColor = SecondaryBg),
                    border = BorderStroke(2.dp, BorderColor),
                ) {
                    LazyColumn (
                        modifier = Modifier.heightIn(min = 256.dp, max = 512.dp)
                    ) {
                        items(
                            items = monsterState.value.monster.tips!!.values.toList(),
                            itemContent = { tip ->
                                Column {
                                    tip.images.forEach { image ->
                                        val url =
                                            "https://api.ambr.top/assets/UI/tutorial/$image.png"
                                        Box(modifier = Modifier.fillMaxWidth()) {
                                            Image(
                                                painter = rememberAsyncImagePainter(
                                                    ImageRequest.Builder(
                                                        LocalContext.current
                                                    ).data(url)
                                                        .apply(block = fun ImageRequest.Builder.() {
                                                            size(Size.ORIGINAL)
                                                        }).build()
                                                ),
                                                contentScale = ContentScale.FillWidth,
                                                contentDescription = null,
                                                modifier = Modifier.fillMaxWidth()
                                            )
                                        }
                                    }
                                    Text(
                                        text = tip.description
                                    )
                                }
                            }
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun EntryDetails(
    entry: EntryEntity?,
    viewModel: MonsterDetailsViewModel,
    state: State<MonsterScreenState>,
) {
    if (entry != null) {
        Column{
            if (entry.resistance.isNotEmpty()) {
                Text(
                    text = stringResource(id = R.string.MonsterDetailsResistancesHeader),
                    modifier = Modifier
                        .padding(vertical = 8.dp),
                    fontWeight = FontWeight.Bold
                )
                LazyVerticalGrid(
                    userScrollEnabled = false,
                    modifier = Modifier
                        .background(BorderColor)
                        .clip(RoundedCornerShape(4.dp))
                        .height(160.dp),
                    columns = GridCells.Adaptive(128.dp),
                    content = {
                        items(entry.resistance.size) {  index ->
                            Row (
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(0.dp)
                                    .border(BorderStroke(2.dp, SecondaryBg))
                                    .padding(8.dp),
                                horizontalArrangement = Arrangement.SpaceBetween,
                                    ) {
                                Image(
                                    painter = painterResource(
                                        id = viewModel.getIcon(entry.resistance.keys.toList()[index])
                                    ),
                                    contentDescription = "Element",
                                    modifier = Modifier
                                        .padding(end = 4.dp)
                                        .size(24.dp),
                                )
                                Text((entry.resistance[entry.resistance.keys.toList()[index]]?.times(100))?.toInt().toString()+"%")
                            }
                        }
                    }
                )
            }
            if (entry.hpDrops != null) {
                var hpDropString = ""
                entry.hpDrops.forEach{
                    hpDropString += it.hpPercent.toString()+"/"
                }
                hpDropString = hpDropString.substringBeforeLast("/")
                Row{
                    if (hpDropString != "") {
                        Text(
                            text = stringResource(R.string.MonsterDetailsEnergyLabel),
                            fontWeight = FontWeight.Bold,
                            modifier = Modifier.padding(end = 8.dp)
                        )
                        Text(text = "$hpDropString%")
                    }
                }
                Row (
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier.padding(top = 8.dp)
                ) {
                    Text(
                        text = stringResource(id = R.string.MonsterDetailsLevelLabel, state.value.monsterLevel.value),
                        fontSize = 36.sp,
                        fontWeight = FontWeight.Bold,
                    )
                    LevelSlider(
                        value = state.value.selectedValue.value.toFloat(),
                    ) { value ->
                        viewModel.onSliderLevelChange(value)
                    }
                }
                Row (
                    modifier = Modifier.padding(bottom = 8.dp),
                    verticalAlignment = Alignment.CenterVertically
                )
                {
                    Text(
                        text = stringResource(id = R.string.MonsterDetailsPlayerCountLabel),
                        modifier = Modifier.padding(end = 8.dp)
                    )
                    Card(
                        modifier = Modifier.clickable {
                            state.value.playerDropdownVisibility.value = true
                        },
                        shape = RoundedCornerShape(2.dp)
                    ) {
                        Row (
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                text = state.value.playerCount.value.toString(),
                                modifier = Modifier.padding(
                                    horizontal = 4.dp,
                                    vertical = 2.dp
                                )
                            )
                            Icon(
                                imageVector = Icons.Filled.ArrowDropDown,
                                contentDescription = "",
                                tint = TextIconColor
                            )
                        }
                        DropdownMenu(
                            expanded = state.value.playerDropdownVisibility.value,
                            onDismissRequest = { state.value.playerDropdownVisibility.value = false }) {
                            for (i in 1..4) {
                                DropdownMenuItem(
                                    onClick = {
                                        viewModel.onPlayerDropdownClick(i)
                                    },
                                    text = {
                                        Text(i.toString())
                                    }
                                )
                            }
                        }
                    }
                }
                Row(
                    modifier = Modifier
                        .padding(horizontal = 0.dp)
                        .fillMaxWidth()
                        .background(RowPrimary),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(text = stringResource(id = R.string.FIGHT_PROP_BASE_HP))
                    Text(state.value.hp.value.toString())
                }
                Row(
                    modifier = Modifier
                        .padding(horizontal = 0.dp)
                        .fillMaxWidth()
                        .background(RowSecondary),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(text = stringResource(id = R.string.FIGHT_PROP_BASE_ATTACK))
                    Text(state.value.atk.value.toString())
                }
                Row(
                    modifier = Modifier
                        .padding(horizontal = 0.dp)
                        .fillMaxWidth()
                        .background(RowPrimary),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(text = stringResource(id = R.string.FIGHT_PROP_BASE_DEFENSE))
                    Text(state.value.def.value.toString())
                }
            }
        }
    }
}

@Composable
fun LevelSlider(value: Float, onValueChange: (Int) -> Unit) {
    val (sliderValue, setSliderValue) = remember { mutableStateOf(value) }

    Row (
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp),
        verticalAlignment = Alignment.CenterVertically
    ){
        Slider(
            modifier = Modifier
                .fillMaxWidth(),
            value = sliderValue,
            valueRange = 0f..10f,
            steps = 9,
            colors = CustomSliderColors(),
            onValueChange = {
                setSliderValue(it)
                onValueChange(it.toInt())
            }
        )
    }
}

@OptIn(ExperimentalLayoutApi::class)
@Composable
private fun Rewards(
    entry: EntryEntity?,
    navController: NavController,
    width: Int,
    ) {
    if (entry != null) {
        if (entry.reward.isNotEmpty()) {
            Column {
                Text(
                    text = stringResource(id = R.string.MonsterDetailsRewardsHeader),
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier.padding(start = 8.dp)
                )
                Card(
                    modifier = Modifier
                        .padding(8.dp),
                    colors = CardDefaults.cardColors(containerColor = SecondaryBg),
                    border = BorderStroke(2.dp, BorderColor),
                ) {
                    FlowRow(
                        modifier = Modifier
                            .background(BorderColor)
                            .fillMaxWidth()
                            .clip(RoundedCornerShape(4.dp)),
                        horizontalArrangement = Arrangement.Center
                    ) {
                        entry.reward.forEach { (id, rewardEntity) ->
                            RewardCard(
                                id = id,
                                reward = rewardEntity,
                                navController = navController,
                                width = width,
                            )
                        }
                    }
                }
            }
        }
    }
}

@Composable
private fun RewardCard(reward: RewardEntity, navController: NavController, id: String, width: Int) {
    val url = "https://api.ambr.top/assets/UI/"+reward.icon+".png"
    Card (
        modifier = Modifier
            .padding(4.dp)
            .width(width.dp)
            .clickable {
                navController.navigate(
                    "materialDetails/{id}"
                        .replace(
                            oldValue = "{id}",
                            newValue = id
                        )
                )
            },
        colors = CardDefaults.cardColors(containerColor = NavBackGround),
        shape = RoundedCornerShape(4.dp)
    ){
        Image(
            painter = rememberAsyncImagePainter(
                ImageRequest.Builder(
                    LocalContext.current
                )
                    .data(url)
                    .apply(
                        block = fun ImageRequest.Builder.() {
                            size(Size.ORIGINAL)
                        }
                    )
                    .build()
            ),
            contentScale = ContentScale.FillWidth,
            contentDescription = null,
            modifier = Modifier.fillMaxSize()
        )
    }
}