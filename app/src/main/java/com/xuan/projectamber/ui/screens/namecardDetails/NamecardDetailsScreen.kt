package com.xuan.projectamber.ui.screens.namecardDetails

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.LocationOn
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.size.Size
import com.xuan.projectamber.R
import com.xuan.projectamber.ui.theme.*
import com.xuan.projectamber.viewmodel.NamecardDetailsViewModel

@Composable
fun NamecardDetailsScreen(
    id: String,
    navController: NavController,
    viewModel: NamecardDetailsViewModel = hiltViewModel()
) {
    val namecard = viewModel.namecard
    val namecardIcon = namecard.icon.replace(
        oldValue = "Icon",
        newValue = "Pic"
    )
    val namecardIconUrl = "https://api.ambr.top/assets/UI/namecard/${namecardIcon}_P.png"

    viewModel.getNamecard(id)

    Surface(
        color = ScreenBackGround,
        modifier = Modifier
            .fillMaxSize()
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .verticalScroll(
                    rememberScrollState(),
                ),

            ) {
            Text(
                text = namecard.name,
                color = TextIconColor,
                fontWeight = FontWeight.Bold,
                fontSize = 24.sp,
                modifier = Modifier.padding(horizontal = 8.dp, vertical = 4.dp)
            )
            Card(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp),
                colors = CardDefaults.cardColors(containerColor = TextIconColor),
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                ) {
                    Box(modifier = Modifier.fillMaxWidth()) {
                        Image(
                            painter = rememberAsyncImagePainter(
                                ImageRequest.Builder(
                                    LocalContext.current
                                )
                                    .data(namecardIconUrl)
                                    .apply(
                                        block = fun ImageRequest.Builder.() {
                                            size(Size.ORIGINAL)
                                        }
                                    )
                                    .build()
                            ),
                            contentScale = ContentScale.FillWidth,
                            contentDescription = null,
                            modifier = Modifier.fillMaxSize()
                        )
                    }
                    Column (
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(8.dp)
                            ) {
                        Text(
                            modifier = Modifier.padding(8.dp),
                            text = namecard.description.replace("\\n","\n"),
                            fontWeight = FontWeight.SemiBold,
                            color = NavBackGround
                        )
                        Card(
                            modifier = Modifier.fillMaxWidth(),
                            colors = CardDefaults.cardColors(containerColor = TextIconColor),
                            border = BorderStroke(2.dp, BorderColor),
                        ) {
                            Column (modifier = Modifier.padding(8.dp)) {
                                Row(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(4.dp),
                                    horizontalArrangement = Arrangement.SpaceBetween,
                                ) {
                                    Row {
                                        Icon(
                                            imageVector = Icons.Filled.LocationOn,
                                            contentDescription = "",
                                            tint = NavBackGround
                                        )

                                        Text(
                                            text = stringResource(id = R.string.NamecardDetailsSourceLabel),
                                            color = NavBackGround,
                                            fontWeight = FontWeight.Bold,
                                            modifier = Modifier.padding(horizontal = 8.dp)
                                        )
                                    }
                                }
                                Card (
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(4.dp),
                                    colors = CardDefaults.cardColors(containerColor = SecondaryBg),
                                ) {
                                    Text(
                                        text = namecard.source,
                                        modifier = Modifier.padding(8.dp),
                                    )
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}