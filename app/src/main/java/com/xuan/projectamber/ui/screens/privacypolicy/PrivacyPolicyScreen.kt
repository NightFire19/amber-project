package com.xuan.projectamber.ui.screens.privacypolicy

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.xuan.projectamber.R
import com.xuan.projectamber.ui.theme.ScreenBackGround
import com.xuan.projectamber.ui.theme.TextIconColor

@Composable

fun PrivacyPolicyScreen(navController: NavController) {
    Surface(
        modifier = Modifier.fillMaxSize(),
        color = ScreenBackGround
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(8.dp)
                .verticalScroll(rememberScrollState()),
        ) {
            Row(
                modifier = Modifier.padding(bottom = 8.dp),
                verticalAlignment = Alignment.CenterVertically,
            ) {

                IconButton(
                    onClick = { navController.popBackStack() },
                    modifier = Modifier.padding(end = 8.dp)
                ) {
                    Icon(
                        imageVector = Icons.Filled.ArrowBack,
                        contentDescription = "",
                        tint = TextIconColor
                    )
                }

                Text(
                    text = stringResource(R.string.PrivacyPolicyHeader),
                    fontWeight = FontWeight.Bold,
                    fontSize = 24.sp
                )
            }

            Text(
                fontSize = 12.sp,
                lineHeight = 20.sp,
                text = stringResource(R.string.PrivacyPolicyIntroduction)
            )
            Text(
                text = stringResource(R.string.PrivacyPolicyInformationCollectionHeader),
                fontWeight = FontWeight.SemiBold,
                fontSize = 18.sp,
                modifier = Modifier.padding(vertical = 8.dp)
            )
            Text(
                fontSize = 12.sp,
                lineHeight = 20.sp,
                text = stringResource(R.string.PrivacyPolicyInformationCollectionBody)
            )
            Text(
                text = stringResource(R.string.PrivacyPolicyInformationDisclosureHeader),
                fontWeight = FontWeight.SemiBold,
                fontSize = 18.sp,
                modifier = Modifier.padding(vertical = 8.dp)
            )
            Text(
                fontSize = 12.sp,
                lineHeight = 20.sp,
                text = stringResource(R.string.PrivacyPolicyInformationDisclosureBody)
            )
            Text(
                text = stringResource(R.string.PrivacyPolicyChoicesHeader),
                fontWeight = FontWeight.SemiBold,
                fontSize = 18.sp,
                modifier = Modifier.padding(vertical = 8.dp)
            )
            Text(
                fontSize = 12.sp,
                lineHeight = 20.sp,
                text = stringResource(R.string.PrivacyPolicyChoicesBody)
            )
            Text(
                text = stringResource(R.string.PrivacyPolicyChildHeader),
                fontWeight = FontWeight.SemiBold,
                fontSize = 18.sp,
                modifier = Modifier.padding(vertical = 8.dp)
            )
            Text(
                fontSize = 12.sp,
                lineHeight = 20.sp,
                text = stringResource(R.string.PrivacyPolicyChildBody)
            )
            Text(
                text = stringResource(R.string.PrivacyPolicyChangesHeader),
                fontWeight = FontWeight.SemiBold,
                fontSize = 18.sp,
                modifier = Modifier.padding(vertical = 8.dp)
            )
            Text(
                fontSize = 12.sp,
                lineHeight = 20.sp,
                text = stringResource(R.string.PrivacyPolicyChangesBody)
            )
        }
    }

}