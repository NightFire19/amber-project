package com.xuan.projectamber.ui.screens.questDetails

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.AsyncImagePainter
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.size.Size
import com.xuan.projectamber.R
import com.xuan.projectamber.domain.models.QuestRewardEntity
import com.xuan.projectamber.domain.models.QuestStoryEntity
import com.xuan.projectamber.ui.common.AutoResizedText
import com.xuan.projectamber.ui.theme.*
import com.xuan.projectamber.util.Constants
import com.xuan.projectamber.viewmodel.QuestDetailsViewModel
import de.charlex.compose.HtmlText

@OptIn(ExperimentalLayoutApi::class)
@Composable
fun QuestDetailsScreen(
    id: String,
    viewModel: QuestDetailsViewModel = hiltViewModel(),
    navController: NavController,
) {
    viewModel.getQuest(id)
    val quest = viewModel.quest
    val dropdownVisibility = viewModel.dropDownVisibility
    val chapterNames = viewModel.chapterNames
    val currentChapterName = viewModel.currentChapterName
    val currentChapter = viewModel.currentChapter
    val configuration = LocalConfiguration.current


    Surface(
        color = ScreenBackGround,
        modifier = Modifier
            .fillMaxSize()
    ) {
        if (quest.info != null) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .verticalScroll(rememberScrollState())
                    .padding(8.dp)
            ) {
                Text(
                    text = quest.info.chapterTitle,
                    fontWeight = FontWeight.Bold,
                    fontSize = 24.sp,
                    modifier = Modifier.padding(vertical = 8.dp)
                )
                quest.info.chapterNum?.let { chapterNum ->
                    Text(
                        text = chapterNum,
                        fontSize = 18.sp,
                    )
                }
                if (chapterNames.size > 1) {
                    Box(
                        modifier = Modifier
                            .clip(RoundedCornerShape(8.dp))
                            .padding(vertical = 8.dp)
                            .background(DarkDropdownBackground)
                            .clip(
                                RoundedCornerShape(4.dp)
                            )
                            .clickable {
                                viewModel.onDropdownMenuClicked()
                            },
                    ) {
                        Row (
                            verticalAlignment = Alignment.CenterVertically,
                                ) {
                            Text(
                                text = currentChapterName,
                                color = DarkDropdownText,
                                textAlign = TextAlign.Center,
                                fontWeight = FontWeight.Bold,
                                modifier = Modifier
                                    .padding(4.dp),
                            )
                            if (dropdownVisibility) {
                                Icon(
                                    imageVector = Icons.Filled.ArrowDropUp,
                                    contentDescription = "",
                                    tint = DarkDropdownText
                                )
                            } else {
                                Icon(
                                    imageVector = Icons.Filled.ArrowDropDown,
                                    contentDescription = "",
                                    tint = DarkDropdownText
                                )
                            }
                        }
                        DropdownMenu(
                            expanded = dropdownVisibility,
                            onDismissRequest = {
                                viewModel.onDropdownDismissRequest()
                            },
                            modifier = Modifier
                                .background(
                                    DarkDropdownBackground
                                )
                        ) {
                            chapterNames.forEachIndexed { index, chapterName ->
                                DropdownMenuItem(
                                    text = {
                                        Text(
                                            text = chapterName,
                                            color = DarkDropdownText
                                        )
                                    },
                                    onClick = {
                                        viewModel.onDropdownMenuItemClick(index)
                                    }
                                )
                            }
                        }
                    }
                }
                Card(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 8.dp),
                    colors = CardDefaults.cardColors(containerColor = SecondaryBg),
                    border = BorderStroke(2.dp, BorderColor),
                    shape = RoundedCornerShape(8.dp)
                ) {
                    Text(
                        modifier = Modifier.padding(8.dp),
                        text = currentChapter.info.description
                    )
                }

                Text(
                    text = stringResource(id = R.string.QuestDetailsRewardsHeader),
                    fontWeight = FontWeight.Bold
                )

                Card(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 8.dp),
                    colors = CardDefaults.cardColors(containerColor = SecondaryBg),
                    border = BorderStroke(2.dp, BorderColor),
                    shape = RoundedCornerShape(8.dp)
                ) {
                    FlowRow(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(8.dp),
                        horizontalArrangement = Arrangement.Center
                    ) {
                        currentChapter.reward.forEach { (_, questRewardEntity) ->
                            Box(
                                modifier = Modifier.width((configuration.screenWidthDp / 5).dp)
                            ) {
                                RewardCard(
                                    reward = questRewardEntity,
                                    navController = navController
                                )
                            }
                        }
                    }
                }

                Text(
                    text = stringResource(id = R.string.QuestDetailsObjectivesLabel),
                    fontWeight = FontWeight.Bold
                )

                Column(
                    modifier = Modifier.fillMaxWidth()
                ) {
                    currentChapter.story.values.toList().forEach { questStory ->
                        ObjectiveCard(
                            viewModel = viewModel,
                            questStory = questStory
                        )
                    }
                }
            }
        }
    }
}

@Composable
private fun RewardCard(
    reward: QuestRewardEntity,
    navController: NavController,
) {
    val iconUrl = "https://api.ambr.top/assets/UI/${reward.icon}.png"

    Card(
        modifier = Modifier
            .padding(4.dp)
            .clickable {
                navController.navigate("materialDetails/${reward.id}")
            },
        colors = CardDefaults.cardColors(containerColor = NavBackGround),
        shape = RoundedCornerShape(4.dp)
    ) {
        Column {
            Box {
                Spacer(modifier = Modifier.matchParentSize())
                val painter = rememberAsyncImagePainter(
                    ImageRequest.Builder(
                        LocalContext.current
                    )
                        .data(iconUrl)
                        .apply(
                            block = fun ImageRequest.Builder.() {
                                size(Size.ORIGINAL)
                            }
                        )
                        .build()
                )
                val imageState = painter.state

                if (imageState is AsyncImagePainter.State.Loading) {
                    CircularProgressIndicator(
                        modifier = Modifier
                            .padding(32.dp)
                            .fillMaxWidth(),
                        color = TextIconColor
                    )
                }

                if (imageState is AsyncImagePainter.State.Error) {
                    val errorPainter = rememberAsyncImagePainter(
                        ImageRequest.Builder(
                            LocalContext.current
                        )
                            .data(Constants.ERROR_IMG_URL)
                            .apply(
                                block = fun ImageRequest.Builder.() {
                                    size(Size.ORIGINAL)
                                }
                            )
                            .build()
                    )
                    Image(
                        painter = errorPainter,
                        contentScale = ContentScale.FillWidth,
                        contentDescription = null,
                        modifier = Modifier
                            .fillMaxSize()
                            .background(
                                color = NavBackGround
                            )
                    )
                }

                Image(
                    painter = painter,
                    contentScale = ContentScale.FillWidth,
                    contentDescription = null,
                    modifier = Modifier
                        .fillMaxSize()
                        .background(
                            color = NavBackGround
                        )
                )
                Column(
                    verticalArrangement = Arrangement.SpaceBetween,
                    modifier = Modifier.matchParentSize()
                ) {
                    Spacer(modifier = Modifier.size(2.dp))

                    LazyRow(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.Center
                    ) {
                        items(reward.rank) {
                            Image(
                                painter = painterResource(
                                    id = R.drawable.star
                                ),
                                contentDescription = "Star",
                                modifier = Modifier
                                    .size(12.dp),
                            )
                        }
                    }
                }
            }
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(24.dp)
                    .background(color = TextIconColor),
                verticalArrangement = Arrangement.Center,
            ) {
                AutoResizedText(
                    text = "${reward.count}",
                    modifier = Modifier
                        .padding(1.dp)
                        .fillMaxWidth(),
                    textAlign = TextAlign.Center,
                    fontWeight = FontWeight.SemiBold,
                    color = ScreenBackGround,
                )
            }
        }
    }
}

@Composable
private fun ObjectiveCard(
    viewModel: QuestDetailsViewModel,
    questStory: QuestStoryEntity
) {
    var dialogueVisibility by remember { mutableStateOf(false) }

    val hasDialogue = questStory.taskData != null || questStory.narratorData != null
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(4.dp),
        colors = CardDefaults.cardColors(containerColor = SecondaryBg),
        border = BorderStroke(2.dp, BorderColor),
        shape = RoundedCornerShape(8.dp)
    ) {

        var modifier = Modifier.padding(4.dp)

        if (hasDialogue) {
            modifier = Modifier
                .padding(4.dp)
                .fillMaxWidth()
                .clickable {
                    dialogueVisibility = !dialogueVisibility
                }
        }

        Column(
            modifier = Modifier
                .padding(4.dp)
                .fillMaxWidth()
        ) {
            Row(
                modifier = modifier,
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Row(
                    modifier = Modifier.weight(10f),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    if (questStory.title != null) {
                        Icon(
                            imageVector = Icons.Default.Task,
                            contentDescription = "",
                            tint = TextIconColor
                        )
                        Text(
                            text = questStory.title,
                            modifier = Modifier.padding(horizontal = 8.dp)
                        )
                    } else {
                        Icon(
                            imageVector = Icons.Default.VisibilityOff,
                            contentDescription = "",
                            tint = TextIconColor
                        )
                        Text(
                            text = stringResource(id = R.string.QuestDetailsHiddenLabel),
                            modifier = Modifier.padding(horizontal = 8.dp)
                        )
                    }
                }
                if (hasDialogue) {
                    if (dialogueVisibility) {
                        Icon(
                            modifier = Modifier.weight(1f),
                            imageVector = Icons.Default.ArrowDropUp,
                            contentDescription = "",
                            tint = TextIconColor
                        )
                    } else {
                        Icon(
                            modifier = Modifier.weight(1f),
                            imageVector = Icons.Default.ArrowDropDown,
                            contentDescription = "",
                            tint = TextIconColor
                        )
                    }
                } else {
                    Spacer(modifier = Modifier.weight(1f))
                }
            }

            if (dialogueVisibility) {
                Divider(
                    color = BorderColor,
                    thickness = 4.dp
                )
                if (questStory.narratorData != null) {
                    questStory.narratorData.forEach { narratorData ->
                        narratorData.items.forEach { narratorItem ->
                            Text(
                                text = narratorItem.role,
                                fontWeight = FontWeight.Bold
                            )
                            Card(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(8.dp),
                                colors = CardDefaults.cardColors(containerColor = ScreenBackGround),
                                shape = RoundedCornerShape(4.dp)
                            ) {
                                HtmlText(
                                    text = viewModel.toHtmlText(narratorItem.text),
                                    modifier = Modifier.padding(8.dp),
                                    color = TextIconColor,
                                    fontFamily = FontFamily.Default
                                )
                            }
                        }
                    }
                }
                if (questStory.taskData != null) {
                    questStory.taskData.forEach { taskData ->
                        taskData.items.forEach { (_, taskItem) ->
                            if (taskItem.type == "SingleDialog") {
                                if (taskItem.role == "Traveler") {
                                    Row(
                                        modifier = Modifier
                                            .padding(4.dp)
                                            .background(color = RowPrimary),
                                        verticalAlignment = Alignment.CenterVertically
                                    ) {
                                        Icon(
                                            modifier = Modifier.weight(1f),
                                            imageVector = Icons.Default.Comment,
                                            contentDescription = "",
                                            tint = TextIconColor
                                        )
                                        taskItem.text.forEach { taskText ->
                                            Card(
                                                modifier = Modifier
                                                    .weight(9f)
                                                    .padding(4.dp),
                                                colors = CardDefaults.cardColors(containerColor = RowPrimary),
                                                shape = RoundedCornerShape(4.dp)
                                            ) {
                                                HtmlText(
                                                    text = viewModel.toHtmlText(taskText.text),
                                                    modifier = Modifier.padding(8.dp),
                                                    color = TextIconColor,
                                                    fontFamily = FontFamily.Default
                                                )
                                            }
                                        }
                                    }
                                } else {
                                    Text(
                                        text = taskItem.role,
                                        fontWeight = FontWeight.Bold
                                    )
                                    taskItem.text.forEach { taskText ->
                                        Card(
                                            modifier = Modifier
                                                .fillMaxWidth()
                                                .padding(8.dp),
                                            colors = CardDefaults.cardColors(containerColor = ScreenBackGround),
                                            shape = RoundedCornerShape(4.dp)
                                        ) {
                                            HtmlText(
                                                text = viewModel.toHtmlText(taskText.text),
                                                modifier = Modifier.padding(8.dp),
                                                color = TextIconColor,
                                                fontFamily = FontFamily.Default
                                            )
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}