package com.xuan.projectamber.ui.screens.questList

import android.util.Log
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.filled.ArrowDropUp
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.AsyncImagePainter
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.size.Size
import com.xuan.projectamber.R
import com.xuan.projectamber.domain.models.QuestEntity
import com.xuan.projectamber.ui.common.AmberTextField
import com.xuan.projectamber.ui.theme.*
import com.xuan.projectamber.viewmodel.QuestListViewModel

@Composable
fun QuestListScreen(
    navController: NavController,
    viewModel: QuestListViewModel = hiltViewModel(),
) {
    val questList = viewModel.questList
    val query = viewModel.query
    val type = viewModel.type
    val dropDownVisibility = viewModel.dropdownVisibliity
    val typeList = viewModel.typeList

    Surface(
        color = ScreenBackGround,
        modifier = Modifier
            .fillMaxSize()
    ) {
        Column {
            Column(
                modifier = Modifier.fillMaxWidth(),
            ) {
                AmberTextField(
                    value = query,
                ) { newEntry ->
                    viewModel.onSearchEntryValueChange(newEntry)
                }
                Box(
                    modifier = Modifier
                        .padding(8.dp)
                        .clip(RoundedCornerShape(8.dp))
                        .background(DarkDropdownBackground)
                        .clickable {
                            viewModel.onDropdownMenuClicked()
                        }
                ) {
                    Row(
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Text(
                            text = type,
                            color = DarkDropdownText,
                            textAlign = TextAlign.Center,
                            fontWeight = FontWeight.Bold,
                            modifier = Modifier
                                .padding(4.dp),
                        )
                        if (dropDownVisibility) {
                            Icon(
                                imageVector = Icons.Filled.ArrowDropUp,
                                contentDescription = "",
                                tint = DarkDropdownText
                            )
                        } else {
                            Icon(
                                imageVector = Icons.Filled.ArrowDropDown,
                                contentDescription = "",
                                tint = DarkDropdownText
                            )
                        }
                    }
                    DropdownMenu(
                        expanded = dropDownVisibility,
                        onDismissRequest = { viewModel.onDropdownDismissRequest() },
                        modifier = Modifier
                            .background(
                                DarkDropdownBackground
                            )
                    ) {
                        typeList.forEachIndexed { index, _type ->
                            DropdownMenuItem(
                                text = {
                                    Text(
                                        text = _type,
                                        color = DarkDropdownText
                                    )
                                },
                                onClick = {
                                    viewModel.onDropdownMenuItemClick(index)
                                }
                            )
                        }
                    }
                }
                Divider(color = BorderColor)
            }
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .verticalScroll(rememberScrollState()),
            ) {
                questList.forEach { questEntity ->
                    QuestCard(
                        navController = navController,
                        questEntity = questEntity
                    )
                }
            }
        }
    }
}

@Composable
fun QuestCard(
    navController: NavController,
    questEntity: QuestEntity,) {
    val iconUrl = "https://api.ambr.top/assets/UI/chapter/${questEntity.chapterIcon}.png"
    if (questEntity.chapterIcon == null) {
        Log.v("test2", "No icon")
    }
    val painter = rememberAsyncImagePainter(
        ImageRequest.Builder(
            LocalContext.current
        )
            .data(iconUrl)
            .apply(
                block = fun ImageRequest.Builder.() {
                    size(Size.ORIGINAL)
                }
            )
            .build()
    )
    val imageState = painter.state
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
            .heightIn(min = 64.dp)
            .clickable {
                navController.navigate(
                    "questDetails/{id}"
                        .replace(
                            oldValue = "{id}",
                            newValue = questEntity.id.toString()
                        )
                )
            },
        colors = CardDefaults.cardColors(containerColor = SecondaryBg),
        border = BorderStroke(2.dp, BorderColor),
        shape = RoundedCornerShape(8.dp)
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .height(IntrinsicSize.Max)
        ) {
            if (questEntity.chapterIcon != null) {
                if (imageState is AsyncImagePainter.State.Loading) {
                    CircularProgressIndicator(
                        modifier = Modifier
                            .weight(1f)
                            .padding(8.dp),
                        color = TextIconColor
                    )
                } else {
                    Image(
                        painter = painter,
                        contentScale = ContentScale.FillWidth,
                        contentDescription = null,
                        modifier = Modifier
                            .weight(1f)
                    )
                }
            }
            Column(
                modifier = Modifier
                    .weight(4f)
                    .fillMaxHeight()
                    .padding(4.dp),
                verticalArrangement = Arrangement.SpaceBetween
            ) {
                Column {
                    questEntity.chapterNum?.let { Text(it) }
                    Text(
                        text = questEntity.chapterTitle,
                        fontWeight = FontWeight.Bold
                    )
                }
                questEntity.chapterImageTitle?.let { Text(it) }
            }
        }
    }
}