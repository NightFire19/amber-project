package com.xuan.projectamber.ui.screens.reliquaryDetails

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.size.Size
import com.xuan.projectamber.R
import com.xuan.projectamber.domain.models.ReliquaryEntity
import com.xuan.projectamber.ui.theme.*
import com.xuan.projectamber.util.Constants
import com.xuan.projectamber.util.Constants.ARTIFACT_PIECES
import com.xuan.projectamber.viewmodel.ReliquaryDetailsViewModel

@Composable
fun ReliquaryDetailsScreen(
    id: String,
    navController: NavHostController,
    viewModel: ReliquaryDetailsViewModel = hiltViewModel()
) {
    viewModel.getReliquary(id)
    val reliquary = viewModel.reliquary
    val reliquaryTabIndex = viewModel.reliquaryTabIndex
    val artifactUrl = viewModel.artifactUrl
    val artifactDescription = viewModel.artifactDescription
    val artifactTitle = viewModel.artifactTitle

    Surface(
        color = ScreenBackGround,
        modifier = Modifier
            .fillMaxSize()
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
        ) {
            Text(
                text = reliquary.name,
                color = TextIconColor,
                fontWeight = FontWeight.Bold,
                fontSize = 24.sp,
                modifier = Modifier.padding(horizontal = 8.dp, vertical = 4.dp)
            )
            Card(
                colors = CardDefaults.cardColors(containerColor = SecondaryBg),
                border = BorderStroke(2.dp, BorderColor),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            ) {
                Column {
                    if (reliquary.suit.size == 5) {
                        TabRow(
                            selectedTabIndex = reliquaryTabIndex,
                        ) {
                            ARTIFACT_PIECES.forEachIndexed { index, _ ->
                                Tab(
                                    selected = reliquaryTabIndex == index,
                                    onClick = {
                                        viewModel.onArtifactTabClicked(index)
                                    },
                                    icon = {
                                        when (index) {
                                            0 -> Icon(
                                                imageVector = Icons.Default.LocalFlorist,
                                                contentDescription = null,
                                                tint = TextIconColor
                                            )
                                            1 -> Icon(
                                                imageVector = Icons.Default.HistoryEdu,
                                                contentDescription = null,
                                                tint = TextIconColor
                                            )
                                            2 -> Icon(
                                                imageVector = Icons.Default.HourglassEmpty,
                                                contentDescription = null,
                                                tint = TextIconColor
                                            )
                                            3 -> Icon(
                                                imageVector = Icons.Default.WineBar,
                                                contentDescription = null,
                                                tint = TextIconColor
                                            )
                                            4 -> Icon(
                                                imageVector = Icons.Default.School,
                                                contentDescription = null,
                                                tint = TextIconColor
                                            )
                                        }
                                    },
                                    modifier = Modifier
                                        .background(
                                            if (reliquaryTabIndex == index) {
                                                BorderColor
                                            } else {
                                                SecondaryBg
                                            }
                                        )
                                )
                            }
                        }
                    }
                }
            }

            if (reliquary.suit.isNotEmpty()) {
                ArtifactCard(
                    reliquaryEntity = reliquary,
                    artifactTitle = artifactTitle,
                    artifactDescription = artifactDescription,
                    artifactUrl = artifactUrl
                )
            }
        }
    }
}

@Composable
fun ArtifactCard(
    reliquaryEntity: ReliquaryEntity,
    artifactDescription: String,
    artifactUrl: String,
    artifactTitle: String,
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp),
        colors = CardDefaults.cardColors(containerColor = SecondaryBg),
    ) {
        Column (
            modifier = Modifier.background(
                brush = Brush.horizontalGradient(
                    colors = Constants.RANK_COLORS[reliquaryEntity.levelList.last().toString()] ?: listOf(
                        SecondaryBg,
                        SecondaryBg
                    )
                )
            )
                ) {
            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier
                    .padding(8.dp)
                    .height(128.dp)
            ) {
                Column(
                    modifier = Modifier
                        .weight(2f)
                        .fillMaxHeight(),
                    verticalArrangement = Arrangement.SpaceBetween,
                ) {
                    Text(artifactTitle)
                    LazyRow {
                        reliquaryEntity.levelList.last().let { count ->
                            items(count) {
                                Image(
                                    painter = painterResource(
                                        id = R.drawable.star
                                    ),
                                    contentDescription = "Star",
                                    modifier = Modifier
                                        .padding(
                                            end = 4.dp,
                                            top = 4.dp,
                                            bottom = 8.dp
                                        )
                                        .size(24.dp),
                                )
                            }
                        }
                    }
                }

                Image(
                    painter = rememberAsyncImagePainter(
                        ImageRequest.Builder(
                            LocalContext.current
                        )
                            .data(artifactUrl)
                            .apply(
                                block = fun ImageRequest.Builder.() {
                                    size(Size.ORIGINAL)
                                }
                            )
                            .build()
                    ),
                    contentScale = ContentScale.FillWidth,
                    contentDescription = null,
                    modifier = Modifier
                        .weight(1f)
                        .fillMaxSize()
                )
            }


            Column(
                modifier = Modifier
                    .background(TextIconColor)
                    .fillMaxWidth()
                    .padding(8.dp)
            ) {
                if (reliquaryEntity.affixList.size == 1) {
                    reliquaryEntity.affixList.forEach { (_, description) ->
                        Text(
                            text = description,
                            color = NavBackGround
                        )
                    }
                } else {
                    reliquaryEntity.affixList.values.toList().forEachIndexed { index, description ->
                        Text(
                            text = stringResource(id = R.string.ReliquaryDetailsBonusPieceLabel,(index + 1) * 2),
                            color = NavBackGround,
                            fontWeight = FontWeight.Bold
                        )
                        Text(
                            text = description,
                            color = NavBackGround
                        )
                    }
                }
                Divider(
                    color = BorderColor,
                    thickness = 4.dp,
                    modifier = Modifier.padding(bottom = 8.dp)
                )
                Text(
                    text = artifactDescription,
                    color = NavBackGround
                )
            }
        }
    }
}