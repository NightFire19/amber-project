package com.xuan.projectamber.ui.screens.search

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.size.Size
import com.xuan.projectamber.R
import com.xuan.projectamber.domain.models.SearchResultEntity
import com.xuan.projectamber.ui.common.AmberTextField
import com.xuan.projectamber.ui.theme.BorderColor
import com.xuan.projectamber.ui.theme.NavBackGround
import com.xuan.projectamber.ui.theme.SecondaryBg
import com.xuan.projectamber.ui.theme.TextIconColor
import com.xuan.projectamber.viewmodel.SearchViewModel

@Composable
fun SearchScreen(
    navController: NavController,
    viewModel: SearchViewModel = hiltViewModel()
) {
    val searchResults = viewModel.searchResults.collectAsState()
    Surface(
        color = NavBackGround,
        modifier = Modifier
            .fillMaxSize()
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .verticalScroll(rememberScrollState()),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(NavBackGround)
                    .padding(8.dp),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically

            ) {
                Text(
                    text = stringResource(id = R.string.SearchHeader),
                    fontWeight = FontWeight.Bold,
                    fontSize = 24.sp,
                )
                OutlinedButton(
                    onClick = {
                        navController.popBackStack()
                    },
                    modifier = Modifier
                        .size(48.dp),
                    shape = CircleShape,
                    border = BorderStroke(2.dp, Color.Gray),
                    contentPadding = PaddingValues(8.dp),
                    colors = ButtonDefaults.outlinedButtonColors(
                        containerColor = TextIconColor,
                        contentColor = Color.Black
                    )
                ) {
                    Icon(
                        imageVector = Icons.Filled.Close,
                        contentDescription = "close",
                    )
                }
            }
            Divider(
                color = BorderColor,
                thickness = 2.dp,
                modifier = Modifier.padding(vertical = 2.dp)
            )
            AmberTextField(
                value = viewModel.searchEntry,
            ) { newEntry ->
                viewModel.onSearchEntryValueChange(entry = newEntry)
            }
            Card(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 4.dp, horizontal = 32.dp),
                colors = CardDefaults.cardColors(containerColor = SecondaryBg),
                border = BorderStroke(2.dp, BorderColor),
            ) {
                Column(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    if (searchResults.value.isNotEmpty()) {
                        searchResults.value.forEach { result ->
                            SearchResultCard(
                                result = result,
                                navController = navController,
                                viewModel = viewModel
                            )
                        }
                    } else if (viewModel.searchEntry != "") {
                        Text(
                            text = stringResource(id = R.string.SearchNoResultsLabel),
                            modifier = Modifier.padding(8.dp)
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun SearchResultCard(
    result: SearchResultEntity,
    navController: NavController,
    viewModel: SearchViewModel
) {
    Column {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(4.dp)
                .clickable {
                    if (viewModel.getRoute(result) != "") {
                        navController.navigate(viewModel.getRoute(result))
                    }
                }
        ) {
            if (result.icon != null) {
                var searchUrl = "https://api.ambr.top/assets/UI/" + result.icon + ".png"

                when (result.type) {
                    "quest" -> searchUrl =
                        "https://api.ambr.top/assets/UI/chapter/" + result.icon + ".png"
                    "namecard" -> searchUrl =
                        "https://api.ambr.top/assets/UI/namecard/" + result.icon + ".png"
                    "monster" -> searchUrl =
                        "https://api.ambr.top/assets/UI/monster/" + result.icon + ".png"
                }

                Image(
                    painter = rememberAsyncImagePainter(
                        ImageRequest.Builder(
                            LocalContext.current
                        )
                            .data(searchUrl)
                            .apply(
                                block = fun ImageRequest.Builder.() {
                                    size(Size.ORIGINAL)
                                }
                            )
                            .build()
                    ),
                    contentDescription = null,
                    modifier = Modifier
                        .size(48.dp)
                )
            }
            Spacer(modifier = Modifier.size(8.dp))
            Column {
                Text(
                    text = result.name,
                )
                Text(
                    text = viewModel.getType(result.type),
                    fontWeight = FontWeight.Light
                )
            }
        }
        Divider(
            color = BorderColor,
            thickness = 2.dp,
            modifier = Modifier.padding(vertical = 2.dp)
        )
    }
}