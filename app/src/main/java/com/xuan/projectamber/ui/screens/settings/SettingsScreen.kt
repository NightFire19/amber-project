package com.xuan.projectamber.ui.screens.settings

import android.widget.Toast
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.filled.ArrowDropUp
import androidx.compose.material.icons.filled.Web
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.xuan.projectamber.R
import com.xuan.projectamber.navigation.Screens
import com.xuan.projectamber.ui.theme.DarkGrayTextColor
import com.xuan.projectamber.ui.theme.ScreenBackGround
import com.xuan.projectamber.ui.theme.SecondaryBg
import com.xuan.projectamber.ui.theme.TextIconColor
import com.xuan.projectamber.util.Constants.LANGUAGE_MAP
import com.xuan.projectamber.viewmodel.SettingsViewModel


@Composable
fun SettingsScreen(
    navController: NavController,
    viewModel: SettingsViewModel = hiltViewModel()
) {
    val configuration = LocalConfiguration.current
    val screenHeight = configuration.screenHeightDp.dp
    val uriHandler = LocalUriHandler.current
    val context = LocalContext.current
    val versionName = viewModel.versionNumber
    val currentLanguage = viewModel.currentLanguage

    Column(
        modifier = Modifier
            .height(screenHeight)
            .fillMaxWidth()
            .background(color = ScreenBackGround)
            .padding(8.dp)
            .verticalScroll(rememberScrollState()),
    ) {

        Text(
            text = stringResource(R.string.SettingsHeader),
            fontWeight = FontWeight.ExtraBold,
            fontSize = 36.sp
        )

        Text(
            text = stringResource(R.string.SettingsLanguageDropdownHeader),
            fontWeight = FontWeight.SemiBold,
            fontSize = 18.sp,
            modifier = Modifier.padding(top = 8.dp)
        )

        Box (
            modifier = Modifier
                .padding(vertical = 8.dp)
                .clip(RoundedCornerShape(8.dp))
                .background(TextIconColor)
                .clickable {
                    viewModel.onLanguageDropdownMenuClicked()
                }
        ) {
            Row (
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = currentLanguage,
                    color = DarkGrayTextColor,
                    textAlign = TextAlign.Center,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier
                        .padding(4.dp),
                )
                if (viewModel.languageDropdownVisibility) {
                    Icon(
                        imageVector = Icons.Filled.ArrowDropUp,
                        contentDescription = "",
                        tint = SecondaryBg
                    )
                } else {
                    Icon(
                        imageVector = Icons.Filled.ArrowDropDown,
                        contentDescription = "",
                        tint = SecondaryBg
                    )
                }
            }
            DropdownMenu(
                expanded = viewModel.languageDropdownVisibility,
                onDismissRequest = { viewModel.onLanguageDropdownDismissRequest() },
                modifier = Modifier
                    .background(
                        TextIconColor
                    )
            ) {
                LANGUAGE_MAP.keys.toList().forEach { languageName ->
                    DropdownMenuItem(
                        text = {
                            Text(
                                text = languageName,
                                color = DarkGrayTextColor
                            )
                        },
                        onClick = {
                            viewModel.onLanguageDropdownMenuItemClick(languageName)
                        }
                    )
                }
            }
        }

        Text(
            text = stringResource(R.string.SettingsOfflineModeHeader),
            fontWeight = FontWeight.SemiBold,
            fontSize = 18.sp,
            modifier = Modifier.padding(top = 8.dp)
        )

        Text(
            text = stringResource(id = R.string.SettingsComingSoonLabel),
            fontStyle = FontStyle.Italic,
            modifier = Modifier.padding(bottom = 8.dp)
        )

        Button(
            colors = ButtonDefaults.buttonColors(
                containerColor = SecondaryBg
            ),
            onClick = {
                viewModel.onClearCacheButtonClick()
                Toast.makeText(
                    context,
                    context.getString(R.string.SettingsCacheToastText),
                    Toast.LENGTH_SHORT
                )
                    .show()
            }
        ) {
            Text(
                text = stringResource(R.string.SettingsCacheButtonText)
            )
        }

        Text(
            text = stringResource(R.string.PrivacyPolicyContentHeader),
            fontWeight = FontWeight.SemiBold,
            fontSize = 24.sp,
            modifier = Modifier.padding(vertical = 8.dp)
        )

        Text(
            fontSize = 12.sp,
            modifier = Modifier.clickable {
                uriHandler.openUri("https://icon54.com/", )
            },
            text = stringResource(id = R.string.SettingsSocialMediaIconCredit, "Pixel Perfect Filled")
        )
        Text(
            fontSize = 12.sp,
            modifier = Modifier.clickable {
                uriHandler.openUri("https://www.flaticon.com/free-icons/explosion")
            },
            text = stringResource(id = R.string.SettingsExplosionIconCredit, " Andrejs Kirma")
        )
        Text(
            fontSize = 12.sp,
            modifier = Modifier.clickable {
                uriHandler.openUri("https://www.icons8.com")
            },
            text = stringResource(id = R.string.SettingsTeapotIconCredit, "Icons8")
        )
        Text(
            fontSize = 12.sp,
            modifier = Modifier.clickable {
                uriHandler.openUri("https://www.freepik.com/free-photos-vectors/icons")
            },
            text = stringResource(id = R.string.SettingsAdditionalIconsCredit, "Freepik Fill & Lineal and Freepik Circular")
        )
        Button(
            colors = ButtonDefaults.buttonColors(
                containerColor = SecondaryBg
            ),
            onClick = {
                navController.navigate(Screens.PrivacyPolicy.route)
            },
            modifier = Modifier.padding(vertical = 8.dp)
        ) {
            Text(
                text = stringResource(id = R.string.PrivacyPolicyButtonLabel)
            )
        }
        Text(
            text = stringResource(R.string.PrivacyPolicyDisclaimerHeader),
            fontWeight = FontWeight.SemiBold,
            fontSize = 24.sp,
            modifier = Modifier.padding(vertical = 8.dp)
        )
        Text(
            fontSize = 12.sp,
            lineHeight = 20.sp,
            text = stringResource(
                R.string.PrivacyPolicyDisclaimerBody,
                R.string.app_name,
                R.string.genshin_name,
                R.string.official_developer_name
            )
        )

        Text(
            fontSize = 12.sp,
            fontWeight = FontWeight.Bold,
            text = stringResource(id = R.string.SettingsDeveloperSNSHeader),
            modifier = Modifier.padding(top = 8.dp)

        )
        Row() {
            IconButton(
                content = {
                    Icon(
                        painter = painterResource(
                            id = R.drawable.gitlab
                        ),
                        contentDescription = "",
                        tint = TextIconColor,
                        modifier = Modifier.size(24.dp)
                    )
                },
                onClick = {
                    uriHandler.openUri("https://gitlab.com/NightFire19/amber-project")
                }
            )
            IconButton(
                content = {
                    Icon(
                        imageVector = Icons.Default.Web,
                        contentDescription = "",
                        tint = TextIconColor,
                        modifier = Modifier.size(24.dp)
                    )
                },
                onClick = {
                    uriHandler.openUri("https://ambr.top/")
                }
            )

            IconButton(
                content = {
                    Icon(
                        painter = painterResource(
                            id = R.drawable.twitter
                        ),
                        contentDescription = "",
                        tint = TextIconColor,
                        modifier = Modifier.size(24.dp)
                    )
                },
                onClick = {
                    uriHandler.openUri("https://twitter.com/anonsbelle")
                }
            )
            IconButton(
                content = {
                    Icon(
                        painter = painterResource(
                            id = R.drawable.discord
                        ),
                        contentDescription = "",
                        tint = TextIconColor,
                        modifier = Modifier.size(24.dp)
                    )
                },
                onClick = {
                    uriHandler.openUri("https://discord.gg/AVPV8hCVUA")
                }
            )
        }
        Text(
            fontSize = 12.sp,
            fontWeight = FontWeight.Bold,
            text = stringResource(id = R.string.SettingsOfficialSNSHeader)
        )
        Row() {
            IconButton(
                content = {
                    Icon(
                        painter = painterResource(
                            id = R.drawable.twitter
                        ),
                        contentDescription = "",
                        tint = TextIconColor,
                        modifier = Modifier.size(24.dp)
                    )
                },
                onClick = {
                    uriHandler.openUri("https://twitter.com/GenshinImpact")
                }
            )
            IconButton(
                content = {
                    Icon(
                        painter = painterResource(
                            id = R.drawable.discord
                        ),
                        contentDescription = "",
                        tint = TextIconColor,
                        modifier = Modifier.size(24.dp)
                    )
                },
                onClick = {
                    uriHandler.openUri("https://discord.com/invite/genshinimpact")
                }
            )
            IconButton(
                content = {
                    Icon(
                        painter = painterResource(
                            id = R.drawable.reddit
                        ),
                        contentDescription = "",
                        tint = TextIconColor,
                        modifier = Modifier.size(24.dp)
                    )
                },
                onClick = {
                    uriHandler.openUri("https://www.reddit.com/r/Genshin_Impact")
                }
            )
            IconButton(
                content = {
                    Icon(
                        painter = painterResource(
                            id = R.drawable.youtube
                        ),
                        contentDescription = "",
                        tint = TextIconColor,
                        modifier = Modifier.size(24.dp)
                    )
                },
                onClick = {
                    uriHandler.openUri("https://www.youtube.com/c/GenshinImpact")
                }
            )
            IconButton(
                content = {
                    Icon(
                        painter = painterResource(
                            id = R.drawable.facebook
                        ),
                        contentDescription = "",
                        tint = TextIconColor,
                        modifier = Modifier.size(24.dp)
                    )
                },
                onClick = {
                    uriHandler.openUri("https://www.facebook.com/Genshinimpact/")
                }
            )
        }
        Text(
            modifier = Modifier.padding(
                vertical = 4.dp
            ),
            fontSize = 12.sp,
            fontWeight = FontWeight.Bold,
            text = stringResource(id = R.string.SettingsTechnologiesHeader)
        )
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            Image(
                painter = painterResource(
                    id = R.drawable.jetpack_compose_icon
                ),
                contentDescription = "Jetpack Compose Icon",
                modifier = Modifier
                    .size(32.dp)
                    .clickable(
                        onClick = {
                            uriHandler.openUri("https://developer.android.com/jetpack/compose")
                        }
                    )
            )
            Image(
                painter = painterResource(
                    id = R.drawable.kotlin_icon
                ),
                contentDescription = "Jetpack Compose Icon",
                modifier = Modifier
                    .size(24.dp)
                    .clickable(
                        onClick = {
                            uriHandler.openUri("https://kotlinlang.org/")
                        }
                    )
            )
        }
        Text(
            text = stringResource(id = R.string.SettingsAppVersion, versionName),
            modifier = Modifier.padding(
                vertical = 4.dp,
            ),
            fontSize = 12.sp,
            fontWeight = FontWeight.Bold,
        )
    }
}