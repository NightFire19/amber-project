package com.xuan.projectamber.ui.screens.tcgDetails

import android.util.Log
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.size.Size
import com.xuan.projectamber.R
import com.xuan.projectamber.domain.models.DictionaryEntity
import com.xuan.projectamber.domain.models.TCGRelatedEntity
import com.xuan.projectamber.domain.models.TCGTalentEntity
import com.xuan.projectamber.ui.theme.*
import com.xuan.projectamber.util.Constants
import com.xuan.projectamber.viewmodel.TCGDetailsViewModel
import de.charlex.compose.HtmlText

@OptIn(ExperimentalLayoutApi::class)
@Composable
fun TCGDetailsScreen(
    id: String,
    navController: NavController,
    viewModel: TCGDetailsViewModel = hiltViewModel()
) {

    val tcg = viewModel.tcg

    LaunchedEffect(Unit) {
        viewModel.getTCG(id)
    }
    Surface(
        color = ScreenBackGround,
        modifier = Modifier
            .fillMaxSize()
    ) {
        if (tcg != null) {
            val cardUrl = "https://api.ambr.top/assets/UI/gcg/${tcg.icon}.png"
            val painter = rememberAsyncImagePainter(
                ImageRequest.Builder(
                    LocalContext.current
                )
                    .data(cardUrl)
                    .apply(
                        block = fun ImageRequest.Builder.() {
                            size(Size.ORIGINAL)
                        }
                    )
                    .build()
            )
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .verticalScroll(rememberScrollState())
                    .padding(8.dp)
            ) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 8.dp)
                ) {
                    Image(
                        painter = painter,
                        contentScale = ContentScale.FillWidth,
                        contentDescription = null,
                        modifier = Modifier
                            .weight(1f)
                    )
                    Column(
                        modifier = Modifier
                            .weight(3f)
                            .padding(horizontal = 8.dp)
                    ) {
                        Row(
                            modifier = Modifier.padding(8.dp)
                        ) {
                            Text(
                                text = tcg.name,
                                fontWeight = FontWeight.Bold,
                                fontSize = 24.sp
                            )
                        }
                        Divider(
                            modifier = Modifier.fillMaxWidth(),
                            color = BorderColor
                        )
                        FlowRow(
                            modifier = Modifier.padding(vertical = 8.dp)
                        ) {
                            tcg.tags.forEach { (key, name) ->
                                TagCard(key = key, name = name)
                            }
                        }
                    }
                }

                Card(
                    colors = CardDefaults.cardColors(containerColor = SecondaryBg),
                    border = BorderStroke(2.dp, BorderColor),
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 8.dp)
                ) {
                    Column(
                        modifier = Modifier.padding(8.dp)
                    ) {
                        if (!(tcg.storyTitle == "" && tcg.storyDetail == "")) {
                            Column(
                                modifier = Modifier.padding(vertical = 8.dp)
                            ) {
                                Text(
                                    text = tcg.storyTitle,
                                    fontWeight = FontWeight.Bold,
                                    fontSize = 20.sp

                                )
                                Text(
                                    text = tcg.storyDetail
                                )
                            }
                        }
                        if (tcg.source != "") {
                            Column(
                                modifier = Modifier.padding(vertical = 8.dp)
                            ) {
                                Text(
                                    text = stringResource(R.string.TCGDetailsSourceLabel),
                                    fontWeight = FontWeight.Bold,
                                    fontSize = 20.sp
                                )
                                Text(
                                    text = tcg.source.toString()
                                )
                            }
                        }
                    }
                }

                Text(
                    text = stringResource(id = R.string.TCGDetailsTalentHeader),
                    fontWeight = FontWeight.Bold,
                    fontSize = 24.sp
                )
                tcg.talent.forEach { (s, tcgTalentEntity) ->
                    TCGTalentCard(
                        tcgTalentEntity = tcgTalentEntity,
                    ) { string, map ->
                        viewModel.processTalentDescription(string, map)
                    }
                }
                if (!tcg.dictionary.isNullOrEmpty()) {
                    Text(
                        text = stringResource(id = R.string.TCGDetailsDictionaryHeader),
                        fontWeight = FontWeight.Bold,
                        fontSize = 24.sp
                    )
                    Card(
                        colors = CardDefaults.cardColors(containerColor = SecondaryBg),
                        border = BorderStroke(2.dp, BorderColor),
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(vertical = 8.dp)
                    ) {
                        Column(
                            modifier = Modifier.padding(8.dp)
                        ) {
                            tcg.dictionary.forEach { (s, dictionaryEntity) ->
                                DictionaryEntry(
                                    dictionaryEntity = dictionaryEntity,
                                    dictionaryMap = tcg.dictionary
                                ) { string, map, param ->
                                    viewModel.processDictionaryString(string, map, param)
                                }
                            }
                        }
                    }

                }
                if (!tcg.relatedEntries.isNullOrEmpty()) {
                    Text(
                        text = stringResource(id = R.string.TCGDetailsRelatedEntriesHeader),
                        fontWeight = FontWeight.Bold,
                        fontSize = 24.sp,
                    )
                    FlowRow {
                        tcg.relatedEntries.forEach { tcgRelatedEntity ->
                            TCGRelatedEntryCard(
                                tcgRelatedEntity = tcgRelatedEntity,
                                navController = navController,
                            )
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun TCGRelatedEntryCard(tcgRelatedEntity: TCGRelatedEntity, navController: NavController) {
    val cardUrl = "https://api.ambr.top/assets/UI/gcg/${tcgRelatedEntity.icon}.png"
    val painter = rememberAsyncImagePainter(
        ImageRequest.Builder(
            LocalContext.current
        )
            .data(cardUrl)
            .apply(
                block = fun ImageRequest.Builder.() {
                    size(Size.ORIGINAL)
                }
            )
            .build()
    )

    Card(
        modifier = Modifier
            .padding(vertical = 8.dp)
            .width(128.dp)
            .clickable { navController.navigate("tcgDetails/${tcgRelatedEntity.id}") },
        shape = RoundedCornerShape(8.dp)
    ) {
        Box(
            modifier = Modifier.fillMaxSize()
        ) {
            Image(
                painter = painter,
                contentScale = ContentScale.FillWidth,
                contentDescription = null,
                modifier = Modifier
                    .fillMaxSize()
            )
            Column(
                modifier = Modifier.matchParentSize(),
                verticalArrangement = Arrangement.Bottom
            ) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(color = NavBackGround.copy(alpha = .5f)),
                    horizontalArrangement = Arrangement.Center,
                ) {
                    Text(
                        text = tcgRelatedEntity.name,
                        textAlign = TextAlign.Center,
                        fontWeight = FontWeight.SemiBold,
                        modifier = Modifier.padding(vertical = 2.dp)
                    )
                }
            }
        }
    }
}

@Composable
fun DictionaryEntry(
    dictionaryEntity: DictionaryEntity,
    dictionaryMap: Map<String, DictionaryEntity>,
    replaceTokens: (String, Map<String, DictionaryEntity>, Map<String, String>?) -> String,
) {
    Column(modifier = Modifier.padding(vertical = 4.dp)) {
        HtmlText(
            text = "\u2022 ${
                replaceTokens(
                    dictionaryEntity.name,
                    dictionaryMap,
                    dictionaryEntity.params
                )
            }",
            fontWeight = FontWeight.Bold,
            color = TextIconColor,
        )
        val columnStrings = replaceTokens(
            dictionaryEntity.description,
            dictionaryMap,
            dictionaryEntity.params
        ).split("\n")
        Column (
            modifier = Modifier.padding(8.dp),
        ){
            for (string in columnStrings) {
                HtmlText(
                    text = string,
                    color = TextIconColor,
                    fontFamily = FontFamily.Default
                )
            }
        }
    }
}

@Composable
fun TagCard(key: String, name: String) {
    Card(
        colors = CardDefaults.cardColors(containerColor = BorderColor),
        modifier = Modifier.padding(end = 8.dp, bottom = 8.dp)
    ) {
        Row() {
            Text(
                text = name,
                modifier = Modifier.padding(8.dp)
            )
        }
    }
}

@Composable
fun TCGTalentCard(
    tcgTalentEntity: TCGTalentEntity,
    replaceTokens: (String, Map<String, String>?) -> String,
) {
    Card(
        colors = CardDefaults.cardColors(containerColor = SecondaryBg),
        border = BorderStroke(2.dp, BorderColor),
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 8.dp)
    ) {
        Column {
            Row(
                modifier = Modifier
                    .padding(8.dp)
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Row {
                    Column {
                        Text(
                            text = tcgTalentEntity.name,
                            fontWeight = FontWeight.Bold,
                            modifier = Modifier.padding(vertical = 8.dp)
                        )
                        if (!tcgTalentEntity.tags.isNullOrEmpty()) {
                            Text(
                                text = tcgTalentEntity.tags.values.joinToString("/"),
                                color = AlternateTextColor,
                            )
                        }
                    }
                }
                Row {
                    if (tcgTalentEntity.cost != null) {
                        tcgTalentEntity.cost.forEach { (key, count) ->
                            Box(
                                modifier = Modifier.padding(2.dp)
                            ) {
                                Constants.TCG_PROPS[key]?.let {
                                    painterResource(
                                        id = it
                                    )
                                }?.let {
                                    Image(
                                        painter = it,
                                        contentDescription = key,
                                        modifier = Modifier
                                            .size(36.dp),
                                    )
                                }
                                Column(
                                    modifier = Modifier.matchParentSize(),
                                    horizontalAlignment = Alignment.CenterHorizontally,
                                    verticalArrangement = Arrangement.Center,
                                ) {
                                    Box(
                                        modifier = Modifier
                                            .background(
                                                NavBackGround.copy(alpha = 0.5f),
                                                RoundedCornerShape(8.dp)
                                            ),
                                    ) {
                                        Text(
                                            text = count.toString(),
                                            textAlign = TextAlign.Center,
                                        )
                                    }
                                }
                            }
                        }
                    } else {
                        Spacer(modifier = Modifier)
                    }
                }
            }
            Divider(
                color = BorderColor,
                modifier = Modifier.fillMaxWidth()
            )
            HtmlText(
                modifier = Modifier.padding(8.dp),
                color = TextIconColor,
                text = replaceTokens(
                    tcgTalentEntity.description,
                    tcgTalentEntity.params
                )
            )
        }
    }
}