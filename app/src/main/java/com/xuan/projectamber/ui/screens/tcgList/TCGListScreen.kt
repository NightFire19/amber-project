package com.xuan.projectamber.ui.screens.tcgList

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Close
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.*
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.rememberAsyncImagePainter
import coil.imageLoader
import coil.request.ImageRequest
import coil.size.Size
import com.xuan.projectamber.R
import com.xuan.projectamber.domain.models.TCGSummaryEntity
import com.xuan.projectamber.ui.common.AmberTextField
import com.xuan.projectamber.ui.common.AutoResizedText
import com.xuan.projectamber.ui.theme.BorderColor
import com.xuan.projectamber.ui.theme.NavBackGround
import com.xuan.projectamber.ui.theme.SecondaryBg
import com.xuan.projectamber.ui.theme.TextIconColor
import com.xuan.projectamber.util.Constants.TCG_PROPS
import com.xuan.projectamber.viewmodel.TCGListViewModel

@Composable
fun TCGListScreen(
    navController: NavController,
    viewModel: TCGListViewModel = hiltViewModel()
) {
    val context = LocalContext.current
    val tcgList = viewModel.tcgList
    val query = viewModel.query
    val needImagesQueued = viewModel.needImagesQueued

    LaunchedEffect(Unit) {
        if (tcgList.isNotEmpty() && needImagesQueued) {
            tcgList.forEach { tcgSummaryEntity ->
                val request = ImageRequest.Builder(context)
                    .data("https://api.ambr.top/assets/UI/gcg/${tcgSummaryEntity.icon}.png")
                    .size(10)
                    .build()

                context.imageLoader.enqueue(request)
            }
            viewModel.onImagesQueued()
        }
    }

    Surface(
        color = NavBackGround,
        modifier = Modifier
            .fillMaxSize()
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize(),
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(NavBackGround),
                verticalAlignment = Alignment.CenterVertically
            ) {
                IconButton(
                    onClick = { navController.popBackStack() },
                    modifier = Modifier.padding(end = 8.dp)
                ) {
                    Icon(
                        imageVector = Icons.Filled.ArrowBack,
                        contentDescription = "",
                        tint = TextIconColor
                    )
                }
                Text(
                    text = stringResource(id = R.string.TCGListHeader),
                    fontWeight = FontWeight.Bold,
                )
            }
            AmberTextField(
                value = query,
            ) { newEntry ->
                viewModel.onSearchEntryValueChange(newEntry)
            }
            LazyVerticalGrid(
                modifier = Modifier
                    .background(NavBackGround)
                    .weight(1f)
                    .padding(top = 8.dp)
                    .clip(RoundedCornerShape(4.dp)),
                columns = GridCells.Adaptive(128.dp),
            ) {
                items(
                    count = tcgList.size,
                    key = { key ->
                        tcgList[key].id
                    }
                ) { index ->
                    TCGCard(
                        tcgSummaryEntity = tcgList[index],
                        navController = navController,
                    )
                }
            }
        }
    }
}

@Composable
fun TCGCard(
    tcgSummaryEntity: TCGSummaryEntity,
    navController: NavController
) {
    val cardUrl = "https://api.ambr.top/assets/UI/gcg/${tcgSummaryEntity.icon}.png"
    val painter = rememberAsyncImagePainter(
        ImageRequest.Builder(
            LocalContext.current
        )
            .data(cardUrl)
            .apply(
                block = fun ImageRequest.Builder.() {
                    size(Size.ORIGINAL)
                }
            )
            .build()
    )
    
    val state = painter.state

    Card(
        modifier = Modifier
            .padding(4.dp)
            .width(128.dp)
            .clickable { navController.navigate("tcgDetails/${tcgSummaryEntity.id}") },
        shape = RoundedCornerShape(8.dp),
        colors = CardDefaults.cardColors(containerColor = NavBackGround),
        ) {
        Box(
            modifier = Modifier.fillMaxSize()
        ) {
            Image(
                painter = painter,
                contentScale = ContentScale.FillWidth,
                contentDescription = null,
                modifier = Modifier
                    .width(128.dp)
                    .height(200.dp)
            )
            Column(
                modifier = Modifier.matchParentSize(),
                verticalArrangement = Arrangement.SpaceBetween,
            ) {
                if (!tcgSummaryEntity.props.isNullOrEmpty()) {
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.SpaceBetween
                    ) {
                        if (tcgSummaryEntity.props.keys.contains("GCG_PROP_HP")) {
                            Box(
                                modifier = Modifier.padding(2.dp)
                            ) {
                                Image(
                                    painter = painterResource(
                                        id = R.drawable.tcg_health
                                    ),
                                    contentDescription = "GCG_PROP_HP",
                                    modifier = Modifier
                                        .size(36.dp),
                                )
                                Column(
                                    modifier = Modifier.matchParentSize(),
                                    horizontalAlignment = Alignment.CenterHorizontally,
                                    verticalArrangement = Arrangement.Center,
                                ) {
                                    Text(
                                        text = tcgSummaryEntity.props["GCG_PROP_HP"].toString(),
                                        textAlign = TextAlign.Center,
                                        fontSize = 14.sp,
                                    )
                                }

                            }
                        } else {
                            Spacer(modifier = Modifier)
                        }
                        Column {
                            tcgSummaryEntity.props.forEach { (s, i) ->
                                if (s != "GCG_PROP_HP") {
                                    Box(
                                        modifier = Modifier.padding(2.dp)
                                    ) {
                                        TCG_PROPS[s]?.let {
                                            painterResource(
                                                id = it
                                            )
                                        }?.let {
                                            Image(
                                                painter = it,
                                                contentDescription = s,
                                                modifier = Modifier
                                                    .size(36.dp),
                                            )
                                        }
                                        Column(
                                            modifier = Modifier.matchParentSize(),
                                            horizontalAlignment = Alignment.CenterHorizontally,
                                            verticalArrangement = Arrangement.Center,
                                        ) {
                                            Box(
                                                modifier = Modifier
                                                    .background(NavBackGround.copy(alpha = 0.5f), RoundedCornerShape(8.dp)),
                                            ) {
                                                Text(
                                                    text = i.toString(),
                                                    textAlign = TextAlign.Center,
                                                )
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    Spacer(modifier = Modifier.fillMaxWidth())
                }
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(36.dp)
                        .background(color = NavBackGround.copy(alpha = .7f)),
                    horizontalArrangement = Arrangement.Center,
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    AutoResizedText(
                        text = tcgSummaryEntity.name,
                        textAlign = TextAlign.Center,
                        color = TextIconColor,
                        fontWeight = FontWeight.SemiBold,
                        modifier = Modifier
                            .padding(vertical = 2.dp)
                            .fillMaxWidth()
                    )
                }
            }
        }
    }
}
