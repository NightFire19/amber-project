package com.xuan.projectamber.ui.screens.weaponDetails

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.size.Size
import com.xuan.projectamber.R
import com.xuan.projectamber.ui.common.AscensionMaterialCard
import com.xuan.projectamber.ui.common.AutoResizedText
import com.xuan.projectamber.ui.common.CustomSliderColors
import com.xuan.projectamber.ui.theme.*
import com.xuan.projectamber.util.Constants
import com.xuan.projectamber.util.Constants.RANK_COLORS
import com.xuan.projectamber.viewmodel.WeaponDetailsViewModel
import de.charlex.compose.HtmlText

@Composable
fun WeaponDetailsScreen(
    upgradeId: String,
    navController: NavController,
    viewModel: WeaponDetailsViewModel = hiltViewModel(),
) {

    viewModel.getWeapon(upgradeId)
    viewModel.getWeaponStory(upgradeId)

    val weapon = viewModel.weapon.collectAsState()
    val selectedValue = viewModel.selected.collectAsState()
    val weaponLevel = viewModel.weaponLevel.collectAsState()
    val mainStatName = viewModel.mainStatName.collectAsState()
    val mainStatValue = viewModel.mainStatValue.collectAsState()
    val subStatName = viewModel.subStatName.collectAsState()
    val subStatValue = viewModel.subStatValue.collectAsState()
    val refinementLevel = viewModel.refinementLevel.collectAsState()
    val refinementTitle = viewModel.refinementTitle.collectAsState()
    val refinementDescription = viewModel.refinementDescription.collectAsState()
    val upperLevelBoundValue = viewModel.upperLevelBoundValue.collectAsState()
    val lowerLevelBoundValue = viewModel.lowerLevelBoundValue.collectAsState()
    val maxLevelIndex = viewModel.maxLevelIndex.collectAsState()
    val promoteMap = viewModel.promoteMap.collectAsState()
    val weaponStoryBody = viewModel.weaponStoryBody.collectAsState()
    val sliderSize = viewModel.sliderSize

    val weaponUrl = "https://api.ambr.top/assets/UI/" + weapon.value.icon + ".png"

    Surface(
        color = ScreenBackGround,
        modifier = Modifier
            .fillMaxSize()
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .verticalScroll(
                    rememberScrollState(),
                )
        ) {
            Text(
                modifier = Modifier.padding(8.dp),
                fontWeight = FontWeight.Bold,
                fontSize = 24.sp,
                text = weapon.value.name!!,
            )
            Card(
                colors = CardDefaults.cardColors(containerColor = SecondaryBg),
                border = BorderStroke(2.dp, BorderColor),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 8.dp, horizontal = 4.dp)
            ) {
                Column(
                    modifier = Modifier.background(
                        brush = Brush.horizontalGradient(
                            colors = RANK_COLORS[weapon.value.rank.toString()] ?: listOf(SecondaryBg, SecondaryBg)
                        )
                    )
                ) {
                    Row(
                        modifier = Modifier
                            .fillMaxSize(),
                        horizontalArrangement = Arrangement.SpaceBetween,
                        verticalAlignment = Alignment.CenterVertically,
                    ) {
                        Column(
                            modifier = Modifier
                                .weight(1f)
                                .padding(8.dp)
                                .fillMaxHeight(),
                            verticalArrangement = Arrangement.SpaceBetween,
                        ) {
                            Text(
                                fontWeight = FontWeight.Bold,
                                text = weapon.value.type!!,
                                fontSize = 18.sp,
                            )
                            Spacer(modifier = Modifier.size(4.dp))
                            if (mainStatName.value != 0 ) {
                                Text(
                                    fontWeight = FontWeight.Bold,
                                    text = stringResource(mainStatName.value),
                                    color = TextIconColor.copy(alpha = 0.7f)
                                )
                            }
                            Text(
                                fontWeight = FontWeight.Bold,
                                text = mainStatValue.value
                            )
                            Spacer(modifier = Modifier.size(4.dp))
                            if (subStatName.value != 0) {
                                Text(
                                    fontWeight = FontWeight.Bold,
                                    text = stringResource(subStatName.value),
                                    color = TextIconColor.copy(alpha = 0.7f)
                                )
                                Text(
                                    fontWeight = FontWeight.Bold,
                                    text = subStatValue.value
                                )
                            } else {
                                Spacer(modifier = Modifier.size(16.dp))
                            }

                            Spacer(modifier = Modifier.size(8.dp))
                            LazyRow {
                                weapon.value.rank?.let { it1 ->
                                    items(it1) {
                                        Image(
                                            painter = painterResource(
                                                id = R.drawable.star
                                            ),
                                            contentDescription = "Star",
                                            modifier = Modifier
                                                .padding(
                                                    end = 4.dp,
                                                    top = 4.dp,
                                                    bottom = 8.dp
                                                )
                                                .size(24.dp),
                                        )
                                    }
                                }
                            }
                        }
                        Image(
                            painter = rememberAsyncImagePainter(
                                ImageRequest.Builder(
                                    LocalContext.current
                                )
                                    .data(weaponUrl)
                                    .apply(
                                        block = fun ImageRequest.Builder.() {
                                            size(Size.ORIGINAL)
                                        }
                                    )
                                    .build()
                            ),
                            contentScale = ContentScale.FillWidth,
                            contentDescription = null,
                            modifier = Modifier.weight(1f)
                        )
                    }
                    Surface(
                        color = LightCardBackground,
                        modifier = Modifier.fillMaxWidth()
                    ) {
                        Column(
                            modifier = Modifier.padding(8.dp)
                        ) {
                            Row(
                                modifier = Modifier.fillMaxWidth(),
                                verticalAlignment = Alignment.CenterVertically,
                            ) {
                                Card(
                                    modifier = Modifier.weight(1f),
                                    shape = RoundedCornerShape(4.dp),
                                    colors = CardDefaults.cardColors(containerColor = DarkGrayTextColor),
                                ) {
                                    Row (modifier = Modifier
                                        .padding(horizontal = 8.dp, vertical = 4.dp)
                                        .fillMaxWidth(),
                                        horizontalArrangement = Arrangement.Center
                                    ) {
                                        AutoResizedText(
                                            text = stringResource(id = R.string.WeaponDetailsLvLabel,weaponLevel.value),
                                            fontWeight = FontWeight.Bold,
                                            color = LightGrayTextColor
                                        )
                                    }
                                }
                                Box(
                                    modifier = Modifier.weight(4f)
                                ) {
                                    LevelSlider(
                                        sliderSize = sliderSize,
                                        value = selectedValue.value.toFloat(),
                                    ) { value ->
                                        viewModel.onValueChanged(value)
                                    }
                                }
                            }
                            if (refinementTitle.value != "null") {
                                Row(
                                    verticalAlignment = Alignment.CenterVertically
                                ) {
                                    Text(
                                        text = stringResource(id = R.string.WeaponDetailsRefinementLabel),
                                        color = BrownTextColor,
                                        fontWeight = FontWeight.Bold,
                                        modifier = Modifier.padding(end = 4.dp)
                                    )
                                    Card(
                                        modifier = Modifier.clickable {
                                            viewModel.refinementVisibility.value = true
                                        },
                                        shape = RoundedCornerShape(4.dp),
                                        colors = CardDefaults.cardColors(containerColor = DarkDropdownBackground),
                                    ) {
                                        Row(
                                            modifier = Modifier.padding(vertical = 2.dp, horizontal = 8.dp),
                                            verticalAlignment = Alignment.CenterVertically
                                        ) {
                                            Text(
                                                text = refinementLevel.value,
                                                modifier = Modifier.padding(
                                                    horizontal = 2.dp,
                                                ),
                                                color = DarkDropdownText,
                                                fontWeight = FontWeight.Bold,
                                            )
                                            Icon(
                                                imageVector = Icons.Filled.ArrowDropDown,
                                                contentDescription = "",
                                                tint = DarkDropdownText
                                            )
                                        }

                                        DropdownMenu(
                                            expanded = viewModel.refinementVisibility.value,
                                            onDismissRequest = {
                                                viewModel.refinementVisibility.value = false
                                            },
                                            modifier = Modifier
                                                .background(
                                                    DarkDropdownBackground
                                                ),
                                        ) {
                                            for (i in 1..5) {
                                                DropdownMenuItem(
                                                    onClick = {
                                                        viewModel.onRefinementLevelClick(i)
                                                    },
                                                    text = {
                                                        Text(
                                                            text = i.toString(),
                                                            color = DarkDropdownText
                                                        )
                                                    }
                                                )
                                            }
                                        }
                                    }
                                }
                                Text(
                                    text = refinementTitle.value,
                                    color = DarkGrayTextColor,
                                    fontWeight = FontWeight.ExtraBold
                                )
                                HtmlText(
                                    text = viewModel.toHtmlText(refinementDescription.value),
                                    color = DarkGrayTextColor,
                                    fontSize = 16.sp,
                                    fontWeight = FontWeight.SemiBold,
                                )
                            }
                            Divider(
                                color = BorderColor.copy(alpha = 0.5f),
                                thickness = 4.dp,
                                modifier = Modifier.padding(vertical = 8.dp)
                            )
                            Text(
                                text = weapon.value.description!!,
                                fontSize = 14.sp,
                                fontWeight = FontWeight.Bold,
                                letterSpacing = 0.sp,
                                color = GrayTextColor,
                            )
                        }
                    }
                }
            }
            Text(
                modifier = Modifier.padding(8.dp),
                fontWeight = FontWeight.Bold,
                fontSize = 24.sp,
                text = stringResource(id = R.string.WeaponDetailsAscensionHeader),
            )
            Card(
                colors = CardDefaults.cardColors(containerColor = SecondaryBg),
                border = BorderStroke(2.dp, BorderColor),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 8.dp, horizontal = 4.dp)
            ) {
                Column {
                    var multiplier = promoteMap.value.keys.size / 6
                    if (promoteMap.value.keys.size % 6 == 0) multiplier -= 1

                    Card(
                        colors = CardDefaults.cardColors(containerColor = SecondaryBg),
                        border = BorderStroke(2.dp, BorderColor),
                        modifier = Modifier
                            .fillMaxWidth(),
                    ) {
                        if (promoteMap.value.isEmpty()) {
                            Text("Loading...")
                        } else {
                            LazyVerticalGrid(
                                columns = GridCells.Fixed(6),
                                modifier = Modifier
                                    .padding(4.dp)
                                    .fillMaxWidth()
                                    .height(88.dp + (88 * multiplier).dp),
                                userScrollEnabled = false,
                                content = {
                                    items(promoteMap.value.keys.size) { index ->
                                        AscensionMaterialCard(
                                            itemId = promoteMap.value.keys.sortedDescending()[index],
                                            quantity = promoteMap.value[promoteMap.value.keys.sortedDescending()[index]]!!,
                                            navController = navController,
                                        )
                                    }
                                }
                            )
                        }
                    }
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 8.dp, vertical = 16.dp),
                        horizontalArrangement = Arrangement.SpaceBetween,
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Text(
                            text = stringResource(id = R.string.WeaponDetailsLevelLabel),
                            fontWeight = FontWeight.Bold,
                            fontSize = 18.sp,
                        )
                        Row {
                            Box(
                                modifier = Modifier
                                    .clip(RoundedCornerShape(16.dp, 0.dp, 0.dp, 16.dp))
                                    .background(TextIconColor)
                                    .width(48.dp)
                                    .clickable {
                                        viewModel.lowerLevelBoundVisibility.value = true
                                    }
                            ) {
                                Text(
                                    text = lowerLevelBoundValue.value,
                                    color = DarkGrayTextColor,
                                    textAlign = TextAlign.Center,
                                    fontWeight = FontWeight.Bold,
                                    modifier = Modifier
                                        .fillMaxSize()
                                        .padding(4.dp),
                                )
                                DropdownMenu(
                                    expanded = viewModel.lowerLevelBoundVisibility.value,
                                    onDismissRequest = {
                                        viewModel.lowerLevelBoundVisibility.value = false
                                    },
                                    modifier = Modifier
                                        .background(
                                            TextIconColor
                                        )
                                ) {
                                    Constants.ASCENSION_LEVELS.subList(0, maxLevelIndex.value)
                                        .forEachIndexed { index, level ->
                                            DropdownMenuItem(
                                                onClick = {
                                                    viewModel.onLowerLevelBoundChange(index)
                                                },
                                                text = {
                                                    Text(
                                                        text = level,
                                                        color = DarkGrayTextColor
                                                    )
                                                }
                                            )
                                        }
                                }
                            }
                            Spacer(
                                modifier = Modifier.padding(8.dp)
                            )
                            Box(
                                modifier = Modifier
                                    .clip(RoundedCornerShape(0.dp, 16.dp, 16.dp, 0.dp))
                                    .background(TextIconColor)
                                    .width(48.dp)
                                    .clickable {
                                        viewModel.upperLevelBoundVisibility.value = true
                                    }
                            ) {
                                Text(
                                    text = upperLevelBoundValue.value,
                                    color = DarkGrayTextColor,
                                    textAlign = TextAlign.Center,
                                    fontWeight = FontWeight.Bold,
                                    modifier = Modifier
                                        .fillMaxSize()
                                        .padding(4.dp),
                                )
                                DropdownMenu(
                                    expanded = viewModel.upperLevelBoundVisibility.value,
                                    onDismissRequest = {
                                        viewModel.upperLevelBoundVisibility.value = false
                                    },
                                    modifier = Modifier
                                        .background(
                                            TextIconColor
                                        )
                                ) {
                                    Constants.ASCENSION_LEVELS.subList(
                                        1,
                                        Constants.ASCENSION_LEVELS.size
                                    ).forEachIndexed { index, level ->
                                        DropdownMenuItem(
                                            onClick = {
                                                viewModel.onUpperLevelBoundChange(index + 1)
                                            },
                                            text = {
                                                Text(
                                                    text = level,
                                                    color = DarkGrayTextColor
                                                )
                                            }
                                        )
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Text(
                modifier = Modifier.padding(8.dp),
                fontWeight = FontWeight.Bold,
                fontSize = 24.sp,
                text = stringResource(id = R.string.WeaponDetailsStoryHeader),
            )
            Card(
                colors = CardDefaults.cardColors(containerColor = SecondaryBg),
                border = BorderStroke(2.dp, BorderColor),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 8.dp, horizontal = 4.dp)
            ) {
                Text(
                    text = weaponStoryBody.value,
                    modifier = Modifier.padding(8.dp)
                )
            }
        }
    }
}

@Composable
fun LevelSlider(sliderSize: Float, value: Float, onValueChange: (Int) -> Unit) {
    val (sliderValue, setSliderValue) = remember { mutableStateOf(value) }

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Slider(
            modifier = Modifier
                .fillMaxWidth(),
            value = sliderValue,
            valueRange = 0f..sliderSize,
            steps = 8,
            colors = CustomSliderColors(),
            onValueChange = {
                setSliderValue(it)
                onValueChange(it.toInt())
            }
        )
    }
}