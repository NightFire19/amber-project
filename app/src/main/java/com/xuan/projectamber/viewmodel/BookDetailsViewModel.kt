package com.xuan.projectamber.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.xuan.projectamber.domain.models.BookEntity
import com.xuan.projectamber.domain.models.ReadableEntity
import com.xuan.projectamber.domain.models.VolumeEntity
import com.xuan.projectamber.domain.repository.RepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BookDetailsViewModel @Inject constructor(
    private val repository: RepositoryImpl
) : ViewModel() {
    var book: BookEntity by mutableStateOf(BookEntity())
        private set
    var volumes: List<ReadableEntity> by mutableStateOf(listOf())
        private set
    var dropDownVisibility: Boolean by mutableStateOf(false)
        private set
    var currentReadableTitle: String by mutableStateOf("")
    var currentReadableDescription: String by mutableStateOf("")
    var currentReadableBody: String by mutableStateOf("")

    fun getBook(id: String) = viewModelScope.launch {
        repository.getBook(id).collectLatest { bookEntity ->
            if (bookEntity != null) {
                book = bookEntity
                if (book.volume != null) {
                    if (volumes.size != book.volume!!.size) {
                        currentReadableTitle = book.volume!![0].name
                        currentReadableDescription = book.volume!![0].description
                        val ids: MutableList<String> = mutableListOf()
                        book.volume!!.forEach { volumeEntity ->
                            ids.add(volumeEntity.storyId)
                        }
                        repository.getReadables(ids).collectLatest { readableEntityList ->
                            volumes = readableEntityList
                            if (currentReadableBody == "") {
                                currentReadableBody = volumes[0].data
                            }
                        }
                    }
                }
            }
        }
    }

    fun onDropdownMenuClicked() {
        dropDownVisibility = true
    }

    fun onDropdownDismissRequest() {
        dropDownVisibility = false
    }

    fun onDropdownMenuItemClick(
        index:Int
    ) {
        currentReadableTitle = book.volume?.get(index)?.name ?: "No Title Found."
        currentReadableDescription = book.volume?.get(index)?.description ?: "No Description Found."
        currentReadableBody = volumes[index].data
        dropDownVisibility = false
    }
}