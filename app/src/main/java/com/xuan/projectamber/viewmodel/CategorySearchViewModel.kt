package com.xuan.projectamber.viewmodel

import androidx.compose.runtime.*
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.xuan.projectamber.domain.models.CategorySearchResultEntity
import com.xuan.projectamber.domain.repository.RepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CategorySearchViewModel @Inject constructor(
    private val repository: RepositoryImpl,
) : ViewModel() {
    var searchResults: List<CategorySearchResultEntity> by mutableStateOf(listOf())
        private set

    var query: String by mutableStateOf("")
    private set
    private var _category: String = ""

    var showExpandCard: Boolean by mutableStateOf(false)

    var listLimit = 51
    var increment = 52

    fun fetchResults(category: String) = viewModelScope.launch {
        getResults(category = category)
    }

    fun onExpandClicked() = viewModelScope.launch {
        listLimit += increment
        getResults(_category)
    }

    private fun getResults(category: String,) = viewModelScope.launch {
        _category = category
        repository.getResultList(
            query = query,
            category = category,
            limit = listLimit
        ).collectLatest { results ->
            searchResults = results
            showExpandCard = searchResults.size >= listLimit
        }
    }

    fun getRoute(categorySearchResultEntity: CategorySearchResultEntity): String {
        when (_category) {
            "avatar" -> return "characterDetails/{id}"
                .replace(
                    oldValue = "{id}",
                    newValue = categorySearchResultEntity.id
                )
            "weapon" -> return "weaponDetails/{id}"
                .replace(
                    oldValue = "{id}",
                    newValue = categorySearchResultEntity.id
                )
            "monster" -> return "monsterDetails/{id}"
                .replace(
                    oldValue = "{id}",
                    newValue = categorySearchResultEntity.id
                )
            "material" -> return "materialDetails/{id}"
                .replace(
                    oldValue = "{id}",
                    newValue = categorySearchResultEntity.id
                )
            "food" -> return "foodDetails/{id}"
                .replace(
                    oldValue = "{id}",
                    newValue = categorySearchResultEntity.id
                )
            "furniture" -> return "furnitureDetails/{id}"
                .replace(
                    oldValue = "{id}",
                    newValue = categorySearchResultEntity.id
                )
            "reliquary" -> return "artifactDetails/{id}"
                .replace(
                    oldValue = "{id}",
                    newValue = categorySearchResultEntity.id
                    )
            "namecard" -> return "namecardDetails/{id}"
                .replace(
                    oldValue = "{id}",
                    newValue = categorySearchResultEntity.id
                )
            "book" -> return "bookDetails/{id}"
                .replace(
                    oldValue = "{id}",
                    newValue = categorySearchResultEntity.id
                )
        }
        return ""
    }

    fun onSearchEntryValueChange(newQuery: String) {
        query = newQuery
        getResults(_category)
    }
}