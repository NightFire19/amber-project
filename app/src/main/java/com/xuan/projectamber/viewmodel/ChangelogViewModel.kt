package com.xuan.projectamber.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.xuan.projectamber.domain.models.CategorySearchResultEntity
import com.xuan.projectamber.domain.models.ChangelogEntity
import com.xuan.projectamber.domain.models.TCGSummaryEntity
import com.xuan.projectamber.domain.repository.RepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ChangelogViewModel @Inject constructor(
    private val repository: RepositoryImpl
) : ViewModel()  {

    var changeLogs: List<ChangelogEntity> by mutableStateOf(listOf())
    var currentChangelog: ChangelogEntity by mutableStateOf(ChangelogEntity())

    var avatar: List<CategorySearchResultEntity> by mutableStateOf(listOf())
        private set
    var weapon: List<CategorySearchResultEntity> by mutableStateOf(listOf())
        private set
    var material: List<CategorySearchResultEntity> by mutableStateOf(listOf())
        private set
    var food: List<CategorySearchResultEntity> by mutableStateOf(listOf())
        private set
    var namecard: List<CategorySearchResultEntity> by mutableStateOf(listOf())
        private set
    var furnishing: List<CategorySearchResultEntity> by mutableStateOf(listOf())
        private set
    var reliquary: List<CategorySearchResultEntity> by mutableStateOf(listOf())
        private set
    var monster: List<CategorySearchResultEntity> by mutableStateOf(listOf())
        private set
    var dropDownVisibility: Boolean by mutableStateOf(false)
        private set
    var gcg: List<TCGSummaryEntity> by mutableStateOf(listOf())
        private set

    init {
        getChangelogs()
    }
    private fun getChangelogs() = viewModelScope.launch {
        changeLogs = repository.getChangelog().values.toList().reversed()
        updateChangelog(changeLogs.toList().first())
    }

    private fun updateChangelog(changelogEntity: ChangelogEntity) = viewModelScope.launch {
        currentChangelog = changelogEntity
        changelogEntity.items.forEach { (category, ids) ->
            if (category == "gcg") {
                getTCG(ids = ids)
            } else {
                getResults(
                    ids = ids,
                    category = category
                )
            }
        }
    }

    private fun getResults(ids: List<String>, category: String) = viewModelScope.launch {
        repository.getResultList(
            ids = ids,
            category = category,
        ).collectLatest { resultList ->
            when (category) {
                "avatar" ->  avatar = resultList
                "weapon" ->  weapon = resultList
                "material" ->  material = resultList
                "food" ->  food = resultList
                "namecard" ->  namecard = resultList
                "furniture" ->  furnishing = resultList
                "reliquary" ->  reliquary = resultList
                "monster" ->  monster = resultList
            }
        }
    }

    private fun getTCG(ids: List<String>) = viewModelScope.launch {
        repository.getTCGList(ids).collectLatest { tcgSummaryEntities->
            gcg = tcgSummaryEntities
        }
    }

    fun getRoute(categorySearchResultEntity: CategorySearchResultEntity): String {
        when (categorySearchResultEntity.category) {
            "avatar" -> return "characterDetails/{id}"
                .replace(
                    oldValue = "{id}",
                    newValue = categorySearchResultEntity.id
                )
            "weapon" -> return "weaponDetails/{id}"
                .replace(
                    oldValue = "{id}",
                    newValue = categorySearchResultEntity.id
                )
            "monster" -> return "monsterDetails/{id}"
                .replace(
                    oldValue = "{id}",
                    newValue = categorySearchResultEntity.id
                )
            "material" -> return "materialDetails/{id}"
                .replace(
                    oldValue = "{id}",
                    newValue = categorySearchResultEntity.id
                )
            "food" -> return "foodDetails/{id}"
                .replace(
                    oldValue = "{id}",
                    newValue = categorySearchResultEntity.id
                )
            "furniture" -> return "furnitureDetails/{id}"
                .replace(
                    oldValue = "{id}",
                    newValue = categorySearchResultEntity.id
                )
            "reliquary" -> return "artifactDetails/{id}"
                .replace(
                    oldValue = "{id}",
                    newValue = categorySearchResultEntity.id
                )
            "namecard" -> return "namecardDetails/{id}"
                .replace(
                    oldValue = "{id}",
                    newValue = categorySearchResultEntity.id
                )
            "book" -> return "bookDetails/{id}"
                .replace(
                    oldValue = "{id}",
                    newValue = categorySearchResultEntity.id
                )
        }
        return ""
    }

    fun onDropdownDismissRequest() = viewModelScope.launch {
        dropDownVisibility = false
    }

    fun onDropdownMenuItemClick(index :Int) = viewModelScope.launch {
        dropDownVisibility = false
        updateChangelog(changeLogs[index])
    }

    fun onDropdownMenuClicked() = viewModelScope.launch {
        dropDownVisibility = true
    }

}