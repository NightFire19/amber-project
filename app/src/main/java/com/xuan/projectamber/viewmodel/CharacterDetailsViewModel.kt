package com.xuan.projectamber.viewmodel

import android.util.Log
import androidx.compose.runtime.*
import androidx.core.text.HtmlCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.xuan.projectamber.R
import com.xuan.projectamber.domain.models.CharacterEntity
import com.xuan.projectamber.domain.models.CharacterStoryEntity
import com.xuan.projectamber.domain.models.TalentEntity
import com.xuan.projectamber.domain.repository.RepositoryImpl
import com.xuan.projectamber.util.Constants.ASCENSION_LEVELS
import com.xuan.projectamber.util.Constants.BASE_CRIT_DMG
import com.xuan.projectamber.util.Constants.BASE_CRIT_RATE
import com.xuan.projectamber.util.Constants.GROW_CURVE_HP_S4
import com.xuan.projectamber.util.Constants.GROW_CURVE_HP_S5
import com.xuan.projectamber.util.Constants.MANUAL_WEAPON
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject
import kotlin.math.roundToInt

@HiltViewModel
class CharacterDetailsViewModel @Inject constructor(
    private val repository: RepositoryImpl
) : ViewModel() {

    private var _character = MutableStateFlow(CharacterEntity())
    val character get() = _character.asStateFlow()

    private var _charStory = MutableStateFlow(CharacterStoryEntity())
    val charStory get() = _charStory.asStateFlow()

    var elementPng by mutableStateOf(0)
    var weaponTypePng by mutableStateOf(0)

    var storySelector by mutableStateOf(true)

    private var hpCurve = listOf(0f)
    private var atkCurve = listOf(0f)
    private var defCurve = listOf(0f)

    private val _level = MutableLiveData(1)
    val level: LiveData<Int> = _level

    private val _selected = MutableLiveData(0)
    val selected: LiveData<Int> = _selected

    private val _health = MutableLiveData(1)
    val health: LiveData<Int> = _health

    private val _attack = MutableLiveData(1)
    val attack: LiveData<Int> = _attack

    private val _defense = MutableLiveData(1)
    val defense: LiveData<Int> = _defense

    private var _ascensionFloat = MutableLiveData(0f)
    val ascensionStat: LiveData<Float> = _ascensionFloat

    private val _ascensionName = MutableLiveData(0)
    val ascensionName: LiveData<Int> = _ascensionName

    private val _selectedNormalAttack = MutableLiveData(0)
    val selectedNormalAttack: LiveData<Int> = _selectedNormalAttack

    private val _selectedSkill = MutableLiveData(0)
    val selectedSkill: LiveData<Int> = _selectedSkill

    private val _selectedBurst = MutableLiveData(0)
    val selectedBurst: LiveData<Int> = _selectedBurst

    val hasOther = mutableStateOf(false)
    val levelCheckedState = mutableStateOf(true)
    val normalAttackCheckedState = mutableStateOf(true)
    val skillCheckedState = mutableStateOf(true)
    val burstCheckedState = mutableStateOf(true)

    val lowerLevelBoundVisibility = mutableStateOf(false)
    val upperLevelBoundVisibility = mutableStateOf(false)

    private val _upperLevelBoundValue = MutableStateFlow("90")
    val upperLevelBoundValue get() = _upperLevelBoundValue.asStateFlow()

    private val _lowerLevelBoundValue = MutableStateFlow("1")
    val lowerLevelBoundValue get() = _lowerLevelBoundValue.asStateFlow()

    private val _maxLevelIndex = MutableStateFlow(ASCENSION_LEVELS.size)
    val maxLevelIndex get() = _maxLevelIndex

    private val _promoteMap = MutableStateFlow(mutableMapOf<String, Int>())
    val promoteMap get() = _promoteMap

    val normalAttackLevelBounds = mutableStateOf(TalentLevelSelector(type = 0))

    val skillLevelBounds = mutableStateOf(TalentLevelSelector(type = 1))

    val burstLevelBounds = mutableStateOf(TalentLevelSelector(type = 2))

    private val _coinCost = MutableStateFlow(0)
    val coinCost get() = _coinCost.asStateFlow()

    private var lowerLevelIndex = 0
    private var upperLevelIndex = 7

    private var baseValue = 0f

    var isAscensionStatPercentage: Boolean by mutableStateOf(true)

    fun getCharacter(id: String) = viewModelScope.launch {
        repository.getCharacter(id).collectLatest { char ->
            if (char != null) {
                _character.value = char
                if (_character.value.element != null) {
                    when (_character.value.element!!.lowercase(Locale.ROOT)) {
                        "fire" -> elementPng = R.drawable.pyro
                        "ice" -> elementPng = R.drawable.cryo
                        "rock" -> elementPng = R.drawable.geo
                        "grass" -> elementPng = R.drawable.dendro
                        "electric" -> elementPng = R.drawable.electro
                        "water" -> elementPng = R.drawable.hydro
                        "wind" -> elementPng = R.drawable.anemo
                    }
                }

                if (_character.value.weaponType != null) {
                    when (_character.value.weaponType!!) {
                        "WEAPON_POLE" -> weaponTypePng = R.drawable.polearm
                        "WEAPON_CLAYMORE" -> weaponTypePng = R.drawable.claymore
                        "WEAPON_CATALYST" -> weaponTypePng = R.drawable.catalyst
                        "WEAPON_SWORD_ONE_HAND" -> weaponTypePng = R.drawable.sword
                        "WEAPON_BOW" -> weaponTypePng = R.drawable.bow
                    }
                }

                hasOther.value = _character.value.other != null

                _health.value = _character.value.upgradeEntity!!.prop[0].initValue.roundToInt()

                if (_character.value.upgradeEntity!!.prop[0].type.contains("S4")) {
                    hpCurve = GROW_CURVE_HP_S4
                } else if (_character.value.upgradeEntity!!.prop[0].type.contains("S5")) {
                    hpCurve = GROW_CURVE_HP_S5
                }

                _attack.value = _character.value.upgradeEntity!!.prop[1].initValue.roundToInt()

                if (_character.value.upgradeEntity!!.prop[1].type.contains("S4")) {
                    atkCurve = GROW_CURVE_HP_S4
                } else if (_character.value.upgradeEntity!!.prop[1].type.contains("S5")) {
                    atkCurve = GROW_CURVE_HP_S5
                }

                _defense.value = _character.value.upgradeEntity!!.prop[2].initValue.roundToInt()

                if (_character.value.upgradeEntity!!.prop[2].type.contains("S4")) {
                    defCurve = GROW_CURVE_HP_S4
                } else if (_character.value.upgradeEntity!!.prop[2].type.contains("S5")) {
                    defCurve = GROW_CURVE_HP_S5
                }

                _character.value.upgradeEntity!!.promote[1].addProps.forEach {
                    if (it.key != "FIGHT_PROP_BASE_HP"
                        && it.key != "FIGHT_PROP_BASE_DEFENSE"
                        && it.key != "FIGHT_PROP_BASE_ATTACK"
                    ) {

                        _ascensionName.value = MANUAL_WEAPON.getValue(it.key)

                        if (it.key == "FIGHT_PROP_ELEMENT_MASTERY") {
                            isAscensionStatPercentage = false
                        }

                        if (_ascensionName.value == R.string.FIGHT_PROP_CRITICAL) baseValue = BASE_CRIT_RATE
                        if (_ascensionName.value == R.string.FIGHT_PROP_CRITICAL_HURT) baseValue = BASE_CRIT_DMG

                    }
                }
                onNormalAttackValueChanged(0)
                onSkillValueChanged(0)
                onBurstValueChanged(0)
                getMaterials()
            }
        }
    }

    fun getCharacterStory(id: String) = viewModelScope.launch {
        val result = id.filter { it.isDigit() }
        repository.getCharacterStory(result).collectLatest { cStory ->
            _charStory.value = cStory
        }
    }

    fun onValueChanged(selected: Int) {
        _selected.value = selected
        if (selected == 0) {
            _level.value = 1
            _health.value = _character.value.upgradeEntity!!.prop[0].initValue.roundToInt()
            _attack.value = _character.value.upgradeEntity!!.prop[1].initValue.roundToInt()
            _defense.value = _character.value.upgradeEntity!!.prop[2].initValue.roundToInt()
        } else {
            _level.value = selected * 10
            _health.value = (_character.value.upgradeEntity!!.prop[0].initValue *
                    hpCurve[_level.value!! - 1]).roundToInt()
            _attack.value = (_character.value.upgradeEntity!!.prop[1].initValue.roundToInt() *
                    atkCurve[_level.value!! - 1]).roundToInt()
            _defense.value = (_character.value.upgradeEntity!!.prop[2].initValue.roundToInt() *
                    defCurve[_level.value!! - 1]).roundToInt()
            when (_level.value) {
                30, 40 -> {
                    getCurves(index = 1)
                }
                50 -> {
                    getCurves(index = 2)
                }
                60 -> {
                    getCurves(index = 3)
                }
                70 -> {
                    getCurves(index = 4)
                }
                80 -> {
                    getCurves(index = 5)
                }
                90 -> {
                    getCurves(index = 6)
                }
            }
        }
    }

    fun convertToPercentage(stat: Float): String {
        return "%.1f".format(stat * 100) + "%"
    }

    fun onNormalAttackValueChanged(selected: Int) {
        _selectedNormalAttack.value = selected
    }

    fun onSkillValueChanged(selected: Int) {
        _selectedSkill.value = selected
    }

    fun onBurstValueChanged(selected: Int) {
        _selectedBurst.value = selected
    }

    fun toHtmlText(input: String): String {
        val regex = "<color=#(\\p{XDigit}{6})\\p{XDigit}*?>".toRegex()
        val closingTagRegex = Regex("</color>")
        val closingTagReplacement = "</font></b>"

        val output = regex.replace(input) { matchResult ->
            val colorHex = matchResult.groups[1]!!.value
            "<b><font color=\"#$colorHex\">"
        }
        return output.replace(closingTagRegex, closingTagReplacement).replace("\\n", "\n")
            .replace("{LAYOUT_MOBILE#Tap}{LAYOUT_PC#Press}{LAYOUT_PS#Press}", "Press")
            .replace("{NICKNAME}", "Traveler")
    }

    fun getDescriptionText(fullString: String): String {
        return fullString.substringBefore("|").replace("#{LAYOUT_MOBILE#Tapping}{LAYOUT_PC#Press}{LAYOUT_PS#Press}", "Press")
    }

    fun getParams(fullString: String, selectedValue: Int, talentEntity: TalentEntity): String {
        var stringHolder = fullString
        var formattedNum = ""
        val params = Regex("\\{(.*?)\\}").findAll(fullString)
        params.forEach { result ->
            if (result.value.contains("[0-9]".toRegex())) {
                val paramNum = result.value.substringBefore(":").filter { it.isDigit() }
                val paramValue =
                    talentEntity.promote[(selectedValue + 1).toString()]?.params?.get(paramNum.toInt() - 1)


                if (result.value.contains("P")) {
                    if (paramValue != null) {
                        formattedNum = String.format("%.1f", paramValue * 100)
                    }
                    formattedNum = "$formattedNum%"
                } else {
                    if (paramValue != null) {
                        formattedNum = paramValue.toInt().toString()
                    }
                }
                stringHolder = stringHolder.replace(result.value, formattedNum)
            }
        }
        return stringHolder.substringAfter("|")
    }

    private fun getCurves(index: Int) {
        _health.value =
            (_health.value!! + _character.value.upgradeEntity!!.promote[index].addProps["FIGHT_PROP_BASE_HP"]!!)
                .toInt()

        _attack.value =
            (_attack.value!! + _character.value.upgradeEntity!!.promote[index].addProps["FIGHT_PROP_BASE_ATTACK"]!!)
                .toInt()

        _defense.value =
            (_defense.value!! + _character.value.upgradeEntity!!.promote[index].addProps["FIGHT_PROP_BASE_DEFENSE"]!!)
                .toInt()

        _character.value.upgradeEntity!!.promote[index].addProps.forEach {
            if (it.key != "FIGHT_PROP_BASE_HP"
                && it.key != "FIGHT_PROP_BASE_DEFENSE"
                && it.key != "FIGHT_PROP_BASE_ATTACK"
            ) {

                _ascensionFloat.value = baseValue + (it.value)

            }
        }
    }

    fun onUpperLevelBoundChange(index: Int) {
        upperLevelBoundVisibility.value = false
        _maxLevelIndex.value = index
        upperLevelIndex = index
        _upperLevelBoundValue.value = ASCENSION_LEVELS[index]
        if (_upperLevelBoundValue.value.toInt() <= _lowerLevelBoundValue.value.toInt()) {
            _lowerLevelBoundValue.value = ASCENSION_LEVELS[index - 1]
        }
        getMaterials()
    }

    fun onLowerLevelBoundChange(index: Int) {
        lowerLevelBoundVisibility.value = false
        lowerLevelIndex = index
        _lowerLevelBoundValue.value = ASCENSION_LEVELS[index]
        getMaterials()
    }

    fun getMaterials() {
        _promoteMap.value = mutableMapOf()
        _coinCost.value = 0

        if (levelCheckedState.value) {
            _character.value.upgradeEntity?.promote?.forEachIndexed { index, promote ->
                if (index in lowerLevelIndex until upperLevelIndex) {
                    _coinCost.value = _coinCost.value + promote.coinCost
                    _promoteMap.value.putAll(mapOf(Pair("202", _coinCost.value)))
                    if (promote.costItems != null) {
                        _promoteMap.value = _promoteMap.value
                            .apply {
                                promote.costItems.forEach { (k, v) ->
                                    merge(
                                        k,
                                        v
                                    ) { oldVal, newVal -> oldVal + newVal }
                                }
                            }
                    }
                }
            }
        }

        if (normalAttackCheckedState.value) {
            _character.value.talent?.get("0")?.promote?.forEach {
                if (it.key.toInt() <= normalAttackLevelBounds.value.upperLevelBound.value
                    && it.key.toInt() >= normalAttackLevelBounds.value.lowerLevelBound.value
                ) {
                    _coinCost.value = _coinCost.value + it.value.coinCost
                    _promoteMap.value.putAll(mapOf(Pair("202", _coinCost.value)))
                    if (it.value.costItems != null) {
                        _promoteMap.value = _promoteMap.value
                            .apply {
                                it.value.costItems!!.forEach { (k, v) ->
                                    merge(
                                        k,
                                        v
                                    ) { oldVal, newVal -> oldVal + newVal }
                                }
                            }
                    }
                }
            }

        }
        if (skillCheckedState.value) {
            _character.value.talent?.get("1")?.promote?.forEach {
                if (it.key.toInt() <= skillLevelBounds.value.upperLevelBound.value
                    && it.key.toInt() >= skillLevelBounds.value.lowerLevelBound.value
                ) {
                    _coinCost.value = _coinCost.value + it.value.coinCost
                    _promoteMap.value.putAll(mapOf(Pair("202", _coinCost.value)))
                    if (it.value.costItems != null) {
                        _promoteMap.value = _promoteMap.value
                            .apply {
                                it.value.costItems!!.forEach { (k, v) ->
                                    merge(
                                        k,
                                        v
                                    ) { oldVal, newVal -> oldVal + newVal }
                                }
                            }
                    }
                }
            }
        }

        if (burstCheckedState.value) {
            if (_character.value.talent?.get("2") != null) {
                _character.value.talent?.get("4")?.promote?.forEach {
                    if (it.key.toInt() <= burstLevelBounds.value.upperLevelBound.value
                        && it.key.toInt() >= burstLevelBounds.value.lowerLevelBound.value
                    ) {
                        _coinCost.value = _coinCost.value + it.value.coinCost
                        _promoteMap.value.putAll(mapOf(Pair("202", _coinCost.value)))
                        if (it.value.costItems != null) {
                            _promoteMap.value = _promoteMap.value
                                .apply {
                                    it.value.costItems!!.forEach { (k, v) ->
                                        merge(
                                            k,
                                            v
                                        ) { oldVal, newVal -> oldVal + newVal }
                                    }
                                }
                        }
                    }
                }
            } else {
                _character.value.talent?.get("3")?.promote?.forEach {
                    if (it.key.toInt() <= burstLevelBounds.value.upperLevelBound.value
                        && it.key.toInt() >= burstLevelBounds.value.lowerLevelBound.value
                    ) {
                        _coinCost.value = _coinCost.value + it.value.coinCost
                        _promoteMap.value.putAll(mapOf(Pair("202", _coinCost.value)))
                        if (it.value.costItems != null) {
                            _promoteMap.value = _promoteMap.value
                                .apply {
                                    it.value.costItems!!.forEach { (k, v) ->
                                        merge(
                                            k,
                                            v
                                        ) { oldVal, newVal -> oldVal + newVal }
                                    }
                                }
                        }
                    }
                }
            }
        }
    }

    fun onUpperTalentLevelChange(state: State<TalentLevelSelector>) {
        when (state.value.type) {
            0 -> {
                levelChange(normalAttackLevelBounds)
            }
            1 -> {
                levelChange(skillLevelBounds)
            }
            2 -> {
                levelChange(burstLevelBounds)
            }
        }
    }

    private fun levelChange(state: MutableState<TalentLevelSelector>) {
        state.value.upperLevelBound.value = state.value.index.value + 2
        state.value.upperVisibility.value = false
        if (state.value.upperLevelBound.value <= state.value.lowerLevelBound.value) {
            state.value.lowerLevelBound.value = state.value.upperLevelBound.value - 1
        }
        getMaterials()
    }

    fun onLowerTalentLevelChange(type: Int, index: Int) {
        when (type) {
            0 -> {
                normalAttackLevelBounds.value.lowerLevelBound.value = index + 1
                normalAttackLevelBounds.value.lowerVisibility.value = false
            }
            1 -> {
                skillLevelBounds.value.lowerLevelBound.value = index + 1
                skillLevelBounds.value.lowerVisibility.value = false
            }
            2 -> {
                burstLevelBounds.value.lowerLevelBound.value = index + 1
                burstLevelBounds.value.lowerVisibility.value = false
            }
        }
        getMaterials()
    }
}

data class TalentLevelSelector(
    val lowerLevelBound: MutableState<Int> = mutableStateOf(1),
    val upperLevelBound: MutableState<Int> = mutableStateOf(10),
    var index: MutableState<Int> = mutableStateOf(10),
    val upperVisibility: MutableState<Boolean> = mutableStateOf(false),
    val lowerVisibility: MutableState<Boolean> = mutableStateOf(false),
    val type: Int,
)


