package com.xuan.projectamber.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.xuan.projectamber.domain.models.ElementEntity
import com.xuan.projectamber.domain.repository.RepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ElementsViewModel @Inject constructor(
    private val repository: RepositoryImpl
) : ViewModel() {

    var elementsEntity: ElementEntity by mutableStateOf(ElementEntity())
        private set

    init {
        getElements()
    }

    private fun getElements() = viewModelScope.launch {
        if (repository.getElements()!!.body() != null) {
            elementsEntity = repository.getElements()?.body()!!.data
        }
    }

    fun toHtmlText(input: String): String {
        val regex = "<color=#(\\p{XDigit}{6})\\p{XDigit}*?>".toRegex()
        val closingTagRegex = Regex("</color>")
        val closingTagReplacement = "</font></b>"

        val output = regex.replace(input) { matchResult ->
            val colorHex = matchResult.groups[1]!!.value
            "<b><font color=\"#$colorHex\">"
        }
        return output.replace(closingTagRegex, closingTagReplacement).replace("\\n", "\n")
            .replace("{LAYOUT_MOBILE#Tap}{LAYOUT_PC#Press}{LAYOUT_PS#Press}", "Press")
            .replace("{NICKNAME}", "Traveler")
    }
}