package com.xuan.projectamber.viewmodel

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.xuan.projectamber.domain.models.MaterialEntity
import com.xuan.projectamber.domain.repository.RepositoryImpl
import com.xuan.projectamber.viewmodel.MaterialScreenState
import com.xuan.projectamber.domain.models.FoodEntity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FoodDetailsViewModel @Inject constructor(
    private val repository: RepositoryImpl
): ViewModel() {

    private val _foodState = MutableStateFlow(FoodScreenState())
    val foodState get() = _foodState.asStateFlow()
    
    fun getFood(id: String) = viewModelScope.launch {
        repository.getFood(id).collectLatest {
            _foodState.value.food.value = it
        }
    }
}

data class FoodScreenState(
    val food: MutableState<FoodEntity> = mutableStateOf(FoodEntity()),
)