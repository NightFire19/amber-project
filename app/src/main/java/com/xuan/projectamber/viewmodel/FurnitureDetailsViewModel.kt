package com.xuan.projectamber.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.xuan.projectamber.domain.models.CategorySearchResultEntity
import com.xuan.projectamber.domain.repository.RepositoryImpl
import com.xuan.projectamber.domain.models.FurnitureEntity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FurnitureDetailsViewModel @Inject constructor(
    private val repository: RepositoryImpl
): ViewModel() {
    var furniture: FurnitureEntity by mutableStateOf(FurnitureEntity())
        private set

    fun getFurniture(id: String) = viewModelScope.launch {
        repository.getFurniture(id).collectLatest {
            furniture = it
        }
    }
}