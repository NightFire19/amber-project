package com.xuan.projectamber.viewmodel

import android.os.CountDownTimer
import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.core.text.HtmlCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.xuan.projectamber.domain.models.*
import com.xuan.projectamber.domain.repository.RepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.time.Duration
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneOffset
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val repository: RepositoryImpl
) : ViewModel() {

    var secondsUntilEuropeReset = mutableStateOf(0)
    var secondsUntilAmericaReset = mutableStateOf(0)
    var secondsUntilAsiaReset = mutableStateOf(0)

    var isException: Pair<Boolean, String> by mutableStateOf(Pair(false, ""))
        private set


    private val _timeUntilAmericaReset = MutableLiveData("00h 00m 00s")
    val timeUntilAmericaReset: LiveData<String> = _timeUntilAmericaReset

    private val _timeUntilEuropeReset = MutableLiveData("00h 00m 00s")
    val timeUntilEuropeReset: LiveData<String> = _timeUntilEuropeReset

    private val _timeUntilAsiaReset = MutableLiveData("00h 00m 00s")
    val timeUntilAsiaReset: LiveData<String> = _timeUntilAsiaReset

    var eventList: List<EventEntity?> by mutableStateOf(listOf())
        private set

    private val _dailyDungeons = MutableStateFlow(
        DailyDungeonEntity(
            response = 0,
            daysOfWeek = null
        )
    )

    val dailyDungeons get() = _dailyDungeons.asStateFlow()

    private val _upgrades = MutableStateFlow<Map<String, UpgradeDetail>>(mapOf())
    val upgrades get() = _upgrades.asStateFlow()

    var noticeCategory = mutableStateOf(0)
    var noticeDialogVisibility = mutableStateOf(false)
    var isSunday = mutableStateOf(false)


    var noticeDialogEvent = mutableStateOf(
        EventEntity(
            id = 0,
            name = Name(""),
            nameFull = NameFull(""),
            description = Description(""),
            banner = Banner(""),
            endAt = ""
        )
    )

    private var dailyFarmingDungeonList: List<List<SimpleDungeonEntity>> =
        List(size = 3, init = { emptyList() })
    var currentDungeonList: List<SimpleDungeonEntity> by mutableStateOf(listOf())
        private set

    init {
        initializeTimers()
        fetchEvents()
        fetchDailyDungeon()
        fetchUpgrades()
    }

    private fun fetchDailyDungeon() = viewModelScope.launch {
        try {
            repository.fetchDailyDungeons()
            repository.getDailyDungeons().collect { dailyDungeonEntity ->
                _dailyDungeons.value = dailyDungeonEntity
            }
        } catch (e: Exception) {
            Log.v("test2", "exception caught: $e")
        }
    }

    private fun fetchUpgrades() = viewModelScope.launch {
        try {
            repository.fetchUpgrades()
            repository.getUpgrades().collect { upgradeEntity ->
                if (upgradeEntity != null) {
                    upgradeEntity.data.let { dataUpgrade ->
                        if (dataUpgrade != null) {
                            _upgrades.value = (dataUpgrade.avatar + dataUpgrade.weapon)
                        }
                    }
                }
            }
        } catch (e: Exception) {
            Log.v("test2", "exception caught: $e")
        }
    }

    fun createUpgradeMap() = viewModelScope.launch {
        val dungeons: MutableList<MutableList<SimpleDungeonEntity>> = mutableListOf()

        fun createDungeonList(
            dayDungeonList: Map<String, Dungeon>
        ): MutableList<SimpleDungeonEntity> {
            val simpleDungeonList: MutableList<SimpleDungeonEntity> = mutableListOf()
            dayDungeonList.forEach { (_, dungeon) ->
                val simpleUpgrade: MutableList<SimpleUpgradeEntity> = mutableListOf()
                dungeon.reward.forEach { reward ->
                    _upgrades.value.forEach { (s, upgradeDetail) ->
                        if (upgradeDetail.items.keys.contains(reward.toString())) {

                            val upgradeIdInt = try {
                                s.toInt()
                            } catch (exception: Exception) {
                                val regex = Regex("\\d+")
                                regex.find(s)!!.value.toInt()
                            }

                            val upgradeHolder = SimpleUpgradeEntity(
                                id = s,
                                isCharacter = upgradeIdInt >= 10000000,
                                imageUrl = "https://api.ambr.top/assets/UI/${upgradeDetail.icon}.png",
                                rank = upgradeDetail.rank,
                            )

                            if (!simpleUpgrade.contains(upgradeHolder)) {
                                simpleUpgrade.add(
                                    upgradeHolder
                                )
                            }
                        }
                    }
                }

                simpleDungeonList.add(
                    SimpleDungeonEntity(
                        name = dungeon.name,
                        city = dungeon.city,
                        upgrades = simpleUpgrade,
                        icon = dungeon.reward.last()
                    )
                )
            }
            return simpleDungeonList
        }

        Log.v("test2", _dailyDungeons.value.daysOfWeek.toString())
        _dailyDungeons.value.daysOfWeek?.let { daysOfWeek ->
            dungeons.add(createDungeonList(daysOfWeek.monday))
            dungeons.add(createDungeonList(daysOfWeek.tuesday))
            dungeons.add(createDungeonList(daysOfWeek.wednesday))
        }
        Log.v("test2", dungeons[0].toString())

        dailyFarmingDungeonList = dungeons
        if (LocalDate.now().dayOfWeek.value == 7) {
            getDailyDungeon(1)
        } else {
            getDailyDungeon(LocalDate.now().dayOfWeek.value + 1)
        }
    }

    private fun initializeTimers() {
        val currentDateTime = LocalDateTime.now(ZoneOffset.UTC)

        var europeRestTime = LocalDateTime.of(
            currentDateTime.year, currentDateTime.month, currentDateTime.dayOfMonth,
            3, 0
        )

        if (europeRestTime < currentDateTime) {
            europeRestTime = europeRestTime.plusDays(1)
        }
        secondsUntilEuropeReset.value =
            Duration.between(currentDateTime, europeRestTime).seconds.toInt()

        object : CountDownTimer((secondsUntilEuropeReset.value * 1000).toLong(), 1000) {
            override fun onTick(millisUntilFinished: Long) {
                if (secondsUntilEuropeReset.value < 1) {
                    secondsUntilEuropeReset.value += 86400
                }
                secondsUntilEuropeReset.value -= 1
                _timeUntilEuropeReset.value = convertToTimeFormat(secondsUntilEuropeReset.value)
            }

            override fun onFinish() {
            }
        }.start()

        var americaResetTime = LocalDateTime.of(
            currentDateTime.year, currentDateTime.month, currentDateTime.dayOfMonth,
            10 - 1, 0
        )

        if (americaResetTime < currentDateTime) {
            americaResetTime = americaResetTime.plusDays(1)
        }
        secondsUntilAmericaReset.value =
            Duration.between(currentDateTime, americaResetTime).seconds.toInt()

        object : CountDownTimer((secondsUntilAmericaReset.value * 1000).toLong(), 1000) {
            override fun onTick(millisUntilFinished: Long) {
                if (secondsUntilAmericaReset.value < 1) {
                    secondsUntilAmericaReset.value += 86400
                }
                secondsUntilAmericaReset.value -= 1
                _timeUntilAmericaReset.value = convertToTimeFormat(secondsUntilAmericaReset.value)
            }

            override fun onFinish() {
            }
        }.start()

        var resetTime = LocalDateTime.of(
            currentDateTime.year, currentDateTime.month, currentDateTime.dayOfMonth,
            21 - 1, 0
        )
        if (resetTime < currentDateTime) {
            resetTime = resetTime.plusDays(1)
        }
        secondsUntilAsiaReset.value = Duration.between(currentDateTime, resetTime).seconds.toInt()

        object : CountDownTimer((secondsUntilAsiaReset.value * 1000).toLong(), 1000) {
            override fun onTick(millisUntilFinished: Long) {
                if (secondsUntilAsiaReset.value < 1) {
                    secondsUntilAsiaReset.value += 86400
                }
                secondsUntilAsiaReset.value -= 1
                _timeUntilAsiaReset.value = convertToTimeFormat(secondsUntilAsiaReset.value)
            }

            override fun onFinish() {
            }
        }.start()
    }

    private fun fetchEvents() = viewModelScope.launch {
        try {
            repository.fetchEvents()
        } catch (e: Exception) {
            isException = Pair(true, e.message.toString())
        }
        getBanners()
    }

    fun getDailyDungeon(dayOfWeek: Int) = viewModelScope.launch {
        if (_dailyDungeons.value.daysOfWeek != null) {
            when (dayOfWeek) {
                2 -> {
                    currentDungeonList = dailyFarmingDungeonList[0]
                }

                3 -> {
                    currentDungeonList = dailyFarmingDungeonList[1]
                }

                4 -> {
                    currentDungeonList = dailyFarmingDungeonList[2]
                }

                5 -> {
                    currentDungeonList = dailyFarmingDungeonList[0]
                }

                6 -> {
                    currentDungeonList = dailyFarmingDungeonList[1]
                }

                7 -> {
                    currentDungeonList = dailyFarmingDungeonList[2]
                }

                1 -> isSunday.value = true
            }
        }
    }

    fun getBanners() = viewModelScope.launch {
        repository.getLikeEvents("Event Wish").collectLatest { _eventList ->
            eventList = _eventList
        }
    }

    fun getEvents() = viewModelScope.launch {
        repository.getLikeEvents("Event Details").collectLatest { _eventList ->
            eventList = _eventList
        }
    }

    fun getOther() = viewModelScope.launch {
        repository.getNotLikeEvents("Event").collectLatest { _eventList ->
            eventList = _eventList
        }
    }

    fun htmlToText(html: String): String {
        return HtmlCompat.fromHtml(html, HtmlCompat.FROM_HTML_MODE_LEGACY).toString()
    }

    private fun convertToTimeFormat(inputSeconds: Int): String {
        val hours = inputSeconds / 3600
        val minutes = (inputSeconds % 3600) / 60
        val seconds = inputSeconds % 60

        return String.format("%02dh %02dm %02ds", hours, minutes, seconds)
    }

    fun errorCaught() = viewModelScope.launch {
        isException = Pair(false, "")
    }

}

data class SimpleDungeonEntity(
    val name: String,
    val city: Int,
    val icon: Int,
    val upgrades: List<SimpleUpgradeEntity>
)

data class SimpleUpgradeEntity(
    val id: String,
    val isCharacter: Boolean,
    val imageUrl: String,
    val rank: Int,
)