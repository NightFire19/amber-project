package com.xuan.projectamber.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.xuan.projectamber.domain.repository.RepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
        private val repository: RepositoryImpl
): ViewModel()
{
    init {
        clearAllTables()
    }

    private fun clearAllTables() = viewModelScope.launch {
        repository.clearAllTables()
    }
}