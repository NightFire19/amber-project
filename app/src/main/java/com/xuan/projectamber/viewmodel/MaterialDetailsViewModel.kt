package com.xuan.projectamber.viewmodel

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.xuan.projectamber.domain.models.MaterialEntity
import com.xuan.projectamber.domain.models.TipEntity
import com.xuan.projectamber.domain.repository.RepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MaterialDetailsViewModel @Inject constructor(
    private val repository: RepositoryImpl
): ViewModel() {

    private val _materialState = MutableStateFlow(MaterialScreenState())
    val materialState get() = _materialState.asStateFlow()
    fun getMaterial(id: String) = viewModelScope.launch {
        repository.getMaterial(id).collectLatest { materialEntity ->
            _materialState.value.material.value = materialEntity
            if (materialEntity.recipe != null) {
                if (materialEntity.recipe.toString() == "false") {
                    Log.v("test2", "no recipe")
                } else {
                    _materialState.value.recipeMap.value = fromRecipeString(materialEntity.recipe.toString())
                }
            }
        }
    }

    fun onSourceClicked() {
        _materialState.value.sourceVisibility.value = !_materialState.value.sourceVisibility.value
    }

    private fun fromRecipeString(value: String?): Map<String, Map<String,MaterialRecipeEntity>> {
        val mapType = object : TypeToken<Map<String, Map<String,MaterialRecipeEntity>>>() {}.type
        return Gson().fromJson(value, mapType)
    }
}

data class MaterialScreenState(
    val material: MutableState<MaterialEntity> = mutableStateOf(MaterialEntity()),
    val sourceVisibility: MutableState<Boolean> = mutableStateOf(true),
    val recipeMap: MutableState<Map<String, Map<String,MaterialRecipeEntity>>> = mutableStateOf(emptyMap())
)

data class MaterialRecipeEntity(
    val icon: String,
    val count: Int,
)