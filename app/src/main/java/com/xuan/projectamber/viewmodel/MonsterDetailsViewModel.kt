package com.xuan.projectamber.viewmodel

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.xuan.projectamber.domain.models.EntryEntity
import com.xuan.projectamber.domain.models.MonsterEntity
import com.xuan.projectamber.domain.repository.RepositoryImpl
import com.xuan.projectamber.util.Constants.ATK_MULTIPLIER
import com.xuan.projectamber.util.Constants.HP_MULTIPLIER
import com.xuan.projectamber.util.Constants.MONSTER_CURVE
import com.xuan.projectamber.util.Constants.RESISTANCES
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MonsterDetailsViewModel  @Inject constructor(
    private val repository: RepositoryImpl
): ViewModel() {

    private val _monsterState = MutableStateFlow(MonsterScreenState())
    val monsterState get() = _monsterState.asStateFlow()

    val variantSelectionDropdownVisibility = mutableStateOf(false)

    fun getMonster(id : String) = viewModelScope.launch {
        repository.getMonster(id).collectLatest { monster ->
            _monsterState.value.monster = monster
            _monsterState.value.currentEntry.value = monster.entries[monster.id.toString()]
            calculateStats(_monsterState.value.monsterLevel.value.toInt())
        }
    }

    fun onVariantChange(i:Int){
        variantSelectionDropdownVisibility.value = false
        _monsterState.value.variant=i
        _monsterState.value.currentEntry.value = _monsterState.value.monster.entries[_monsterState.value.monster.entries.keys.sorted()[i-1]]
        onSliderLevelChange(_monsterState.value.selectedValue.value)
    }

    fun getIcon(string: String): Int {
        return RESISTANCES[string]!!
    }

    fun onPlayerDropdownClick(i: Int) {
        _monsterState.value.playerDropdownVisibility.value = false
        _monsterState.value.playerCount.value = i
        calculateStats(_monsterState.value.monsterLevel.value.toInt())
    }

    fun onSliderLevelChange(i: Int) {
        _monsterState.value.selectedValue.value = i
        if (i == 0) {
            _monsterState.value.monsterLevel.value = "1"
        } else {
            _monsterState.value.monsterLevel.value = (i*10).toString()
        }
        calculateStats(_monsterState.value.monsterLevel.value.toInt())
    }

    private fun calculateStats(level: Int) {
        _monsterState.value.currentEntry.value?.prop?.get(0).let {
            _monsterState.value.hp.value = (it!!.initValue * MONSTER_CURVE[it.type]?.get(level-1)!! * HP_MULTIPLIER[_monsterState.value.playerCount.value-1]).toInt()
        }

        _monsterState.value.currentEntry.value?.prop?.get(1).let {
            _monsterState.value.atk.value = (it!!.initValue * MONSTER_CURVE[it.type]?.get(level-1)!! * ATK_MULTIPLIER[_monsterState.value.playerCount.value-1]).toInt()
        }
        _monsterState.value.currentEntry.value?.prop?.get(2).let {
            _monsterState.value.def.value = (it!!.initValue * MONSTER_CURVE[it.type]?.get(level-1)!!).toInt()
        }
    }
}

data class MonsterScreenState(
    var monster: MonsterEntity = MonsterEntity(),
    var variant: Int = 1,
    val currentEntry: MutableState<EntryEntity?> = mutableStateOf(EntryEntity()),
    val playerCount: MutableState<Int> = mutableStateOf(1),
    val playerDropdownVisibility: MutableState<Boolean> = mutableStateOf(false),
    val selectedValue: MutableState<Int> = mutableStateOf(0),
    val monsterLevel: MutableState<String> = mutableStateOf("1"),
    val hp: MutableState<Int> = mutableStateOf(0),
    val atk: MutableState<Int> = mutableStateOf(0),
    val def: MutableState<Int> = mutableStateOf(0),
)