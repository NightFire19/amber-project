package com.xuan.projectamber.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.xuan.projectamber.domain.models.FurnitureEntity
import com.xuan.projectamber.domain.models.NamecardEntity
import com.xuan.projectamber.domain.repository.RepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NamecardDetailsViewModel @Inject constructor(
    private val repository: RepositoryImpl
): ViewModel() {
    var namecard: NamecardEntity by mutableStateOf(NamecardEntity())
        private set

    fun getNamecard(id: String) = viewModelScope.launch {
        repository.getNamecard(id).collectLatest {
            namecard = it
        }
    }
}