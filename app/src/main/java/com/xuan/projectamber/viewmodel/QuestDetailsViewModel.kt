package com.xuan.projectamber.viewmodel

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.xuan.projectamber.domain.models.QuestDetailEntity
import com.xuan.projectamber.domain.models.StoryListEntity
import com.xuan.projectamber.domain.repository.RepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class QuestDetailsViewModel @Inject constructor(
    private val repository: RepositoryImpl
) : ViewModel() {
    var isStartUp = true
    var quest: QuestDetailEntity by mutableStateOf(QuestDetailEntity())
        private set
    var dropDownVisibility: Boolean by mutableStateOf(false)
        private set
    var chapterNames: List<String> by mutableStateOf(listOf())
    var currentChapterName: String by mutableStateOf("")
    var currentChapter: StoryListEntity by mutableStateOf(StoryListEntity())

    fun getQuest(id: String) = viewModelScope.launch {
        repository.getQuest(id).collectLatest { _quest ->
            quest = _quest
            if (isStartUp) {
                quest.storyList.forEach { (_, storyListEntity) ->
                    chapterNames = chapterNames + storyListEntity.info.title
                }
                currentChapterName = chapterNames.first()
                currentChapter = quest.storyList["0"]!!
                isStartUp = false
            }
        }
    }

    fun onDropdownDismissRequest() = viewModelScope.launch  {
        dropDownVisibility = false
    }

    fun onDropdownMenuClicked() = viewModelScope.launch {
        dropDownVisibility = true
    }

    fun onDropdownMenuItemClick(index : Int) = viewModelScope.launch {
        currentChapter = quest.storyList[index.toString()]!!
        currentChapterName = chapterNames[index]
        onDropdownDismissRequest()
    }

    fun toHtmlText(input: String): String {
        val regex = "<color=#(\\p{XDigit}{6})\\p{XDigit}*?>".toRegex()
        val closingTagRegex = Regex("</color>")
        val closingTagReplacement = "</font></b>"

        val output = regex.replace(input) { matchResult ->
            val colorHex = matchResult.groups[1]!!.value
            "<b><font color=\"#$colorHex\">"
        }
        Log.v("test2", output)
        return output.replace(closingTagRegex, closingTagReplacement).replace("\\n", "\n")
            .replace("{LAYOUT_MOBILE#Tap}{LAYOUT_PC#Press}{LAYOUT_PS#Press}", "Press")
            .replace("{NICKNAME}", "Traveler")
    }
}