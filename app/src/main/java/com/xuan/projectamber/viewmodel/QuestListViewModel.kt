package com.xuan.projectamber.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.xuan.projectamber.domain.models.QuestEntity
import com.xuan.projectamber.domain.repository.RepositoryImpl
import com.xuan.projectamber.util.Constants.QUEST_TYPES
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class QuestListViewModel@Inject constructor(
    private val repository: RepositoryImpl
): ViewModel()  {
    init {
        fetchQuests()
    }

    var questList: List<QuestEntity> by mutableStateOf(listOf())
    var query: String by mutableStateOf("")
    var typeList = QUEST_TYPES.keys.toList()
    var type: String by mutableStateOf(typeList[0])
    var dropdownVisibliity by mutableStateOf(false)

    private fun fetchQuests() = viewModelScope.launch {
        repository.fetchQuests()
        getQuests()
    }

    private fun getQuests() = viewModelScope.launch {
        repository.getQuestsByType(query = query, type = QUEST_TYPES[type]!!).collectLatest { _questList ->
            questList = _questList
        }
    }

    fun onSearchEntryValueChange(_query: String) = viewModelScope.launch {
        query = _query
        getQuests()
    }

    fun onDropdownMenuClicked() = viewModelScope.launch {
        dropdownVisibliity = true
    }

    fun onDropdownDismissRequest() = viewModelScope.launch {
        dropdownVisibliity = false
    }

    fun onDropdownMenuItemClick(index: Int) = viewModelScope.launch {
        type = typeList[index]
        getQuests()
        onDropdownDismissRequest()
    }
}