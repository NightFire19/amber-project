package com.xuan.projectamber.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.xuan.projectamber.domain.models.ReliquaryEntity
import com.xuan.projectamber.domain.repository.RepositoryImpl
import com.xuan.projectamber.util.Constants
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ReliquaryDetailsViewModel @Inject constructor(
    private val repository: RepositoryImpl
): ViewModel() {

    var reliquary: ReliquaryEntity by mutableStateOf(ReliquaryEntity())
    var reliquaryTabIndex: Int by mutableStateOf(0)
    private var suitKey: String = ""
    var artifactUrl: String by mutableStateOf("")
    var artifactDescription: String by mutableStateOf("")
    var artifactTitle: String by mutableStateOf("")
    private var isStartUp: Boolean by mutableStateOf(true)
    fun getReliquary(id: String) = viewModelScope.launch {
        repository.getArtifact(id).collectLatest {
            reliquary = it
            if (isStartUp) {
                if (reliquary.suit.size>1){
                    onArtifactTabClicked(0)
                } else {
                    artifactDescription = reliquary.suit["EQUIP_DRESS"]!!.description
                    artifactUrl = "https://api.ambr.top/assets/UI/reliquary/${reliquary.suit["EQUIP_DRESS"]!!.icon}.png"
                    artifactTitle = reliquary.suit["EQUIP_DRESS"]!!.name
                }
                isStartUp = false
            }
        }
    }

    fun onArtifactTabClicked(index : Int) = viewModelScope.launch {
        reliquaryTabIndex = index
        suitKey = Constants.ARTIFACT_PIECES[index]
        if (reliquary.suit[suitKey] != null) {
            artifactDescription = reliquary.suit[suitKey]!!.description
            artifactUrl = "https://api.ambr.top/assets/UI/reliquary/${reliquary.suit[suitKey]!!.icon}.png"
            artifactTitle = reliquary.suit[suitKey]!!.name
        }

    }
}