package com.xuan.projectamber.viewmodel

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.text.capitalize
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.xuan.projectamber.domain.models.SearchResultEntity
import com.xuan.projectamber.domain.repository.RepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val repository: RepositoryImpl
) : ViewModel() {

    var searchEntry: String by mutableStateOf("")
        private set
    private val _searchResults = MutableStateFlow<List<SearchResultEntity>>(listOf())
    val searchResults get() = _searchResults

    init {
        screenStartUp()
    }

    private fun screenStartUp() = viewModelScope.launch {
        repository.fetchSearchResults()
    }

    fun onSearchEntryValueChange(entry: String) = viewModelScope.launch {
        if (!entry.contains("\n")) {
            searchEntry = entry
            if (entry.isNotBlank()) {
                repository.getSearchResults(entry).collectLatest { results ->
                    _searchResults.value = results
                }
            } else {
                _searchResults.value = listOf()
            }
        }
    }

    fun getRoute(searchResultEntity: SearchResultEntity): String {
        when (searchResultEntity.type) {
            "avatar" -> return "characterDetails/{id}"
                .replace(
                    oldValue = "{id}",
                    newValue = searchResultEntity.id
                )

            "weapon" -> return "weaponDetails/{id}"
                .replace(
                    oldValue = "{id}",
                    newValue = searchResultEntity.id
                )

            "monster" -> return "monsterDetails/{id}"
                .replace(
                    oldValue = "{id}",
                    newValue = searchResultEntity.id
                )

            "material" -> return "materialDetails/{id}"
                .replace(
                    oldValue = "{id}",
                    newValue = searchResultEntity.id
                )

            "food" -> return "foodDetails/{id}"
                .replace(
                    oldValue = "{id}",
                    newValue = searchResultEntity.id
                )

            "furniture" -> return "furnitureDetails/{id}"
                .replace(
                    oldValue = "{id}",
                    newValue = searchResultEntity.id
                )

            "reliquary" -> return "artifactDetails/{id}"
                .replace(
                    oldValue = "{id}",
                    newValue = searchResultEntity.id
                )

            "namecard" -> return "namecardDetails/{id}"
                .replace(
                    oldValue = "{id}",
                    newValue = searchResultEntity.id
                )

            "book" -> return "bookDetails/{id}"
                .replace(
                    oldValue = "{id}",
                    newValue = searchResultEntity.id
                )
        }
        return ""
    }

    fun getType(type: String): String {
        when (type) {
            "avatar" -> return "Character"
        }
        return type.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.ROOT) else it.toString() }
    }
}