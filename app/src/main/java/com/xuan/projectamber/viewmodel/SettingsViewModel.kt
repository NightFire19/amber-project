package com.xuan.projectamber.viewmodel

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.xuan.projectamber.BuildConfig
import com.xuan.projectamber.data.ds.UserPreferencesRepository
import com.xuan.projectamber.domain.repository.RepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SettingsViewModel @Inject constructor(
        private val repository: RepositoryImpl,
        private val userPreferencesRepository: UserPreferencesRepository
): ViewModel(){

    var versionNumber: String by mutableStateOf("")
        private set

    var currentLanguage: String by mutableStateOf("")
        private set
    var languageDropdownVisibility: Boolean by mutableStateOf(false)
        private set

    init {
        getVersionNumber()
        currentLanguage = userPreferencesRepository.languageFlow
    }

    private fun getVersionNumber() = viewModelScope.launch {
        versionNumber = BuildConfig.VERSION_NAME
    }

    fun onClearCacheButtonClick() = viewModelScope.launch {
        repository.clearAllTables()
    }

    fun onLanguageDropdownMenuClicked() = viewModelScope.launch {
        languageDropdownVisibility = true
    }

    fun onLanguageDropdownDismissRequest() = viewModelScope.launch {
        languageDropdownVisibility = false
    }

    fun onLanguageDropdownMenuItemClick(languageName: String) = viewModelScope.launch {
        onLanguageDropdownDismissRequest()
        onClearCacheButtonClick()
        userPreferencesRepository.setLanguage(languageName)
        currentLanguage = languageName
        Log.v("test2",userPreferencesRepository.languageFlow)
    }
}