package com.xuan.projectamber.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.xuan.projectamber.domain.models.DictionaryEntity
import com.xuan.projectamber.domain.models.TCGEntity
import com.xuan.projectamber.domain.repository.RepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class TCGDetailsViewModel @Inject constructor(
    private val repository: RepositoryImpl
) : ViewModel(){

     var tcg : TCGEntity? by mutableStateOf(TCGEntity())

    suspend fun getTCG(id: String) =
        withContext(viewModelScope.coroutineContext) {
            repository.getTCG(id).collectLatest { tcgEntity ->
                tcg = tcgEntity
            }
        }

    fun processDictionaryString(
        input: String,
        map: Map<String,DictionaryEntity>,
        param: Map<String,String>?
    ) : String {
        return if (param.isNullOrEmpty()) {
            toHtmlText(deleteSprites(replaceDictionaryTokens(input, map)))
        } else {
            toHtmlText(deleteSprites(replaceTokens(replaceDictionaryTokens(input, map), param)))
        }

    }

    fun processTalentDescription(
        input: String,
        map: Map<String,String>?,
    ) : String {
        return if (!map.isNullOrEmpty()) {
            toHtmlText(deleteSprites(replaceTokens(input, map)))
        } else {
            toHtmlText(deleteSprites(input))
        }
    }

    private fun replaceTokens(input: String, map: Map<String,String>): String {
        val tokenRegex = Regex("""\$\[([^\]]+)\]""")
        return tokenRegex.replace(input) { result ->
            val token = result.groupValues[1]
            val value = map[token]
            value ?: result.value
        }
    }

    private fun replaceDictionaryTokens(input: String, map: Map<String,DictionaryEntity>): String {
        val tokenRegex = Regex("""\$\[([^\]]+)\]""")
        return tokenRegex.replace(input) { result ->
            val token = result.groupValues[1]
            val value = map[token]
            value?.name ?: result.value
        }
    }

    private fun deleteSprites(input: String): String {
        val tokenRegex = Regex("""\{[^\}]*\}""")
        return tokenRegex.replace(input, "")
    }

    private fun toHtmlText(input: String): String {
        val regex = "<color=#(\\p{XDigit}{6})\\p{XDigit}*?>".toRegex()
        val closingTagRegex = Regex("</color>")
        val closingTagReplacement = "</font></b>"

        val output = regex.replace(input) { matchResult ->
            val colorHex = matchResult.groups[1]!!.value
            "<b><font color=\"#$colorHex\">"
        }
        return output.replace(closingTagRegex, closingTagReplacement)
            .replace("{LAYOUT_MOBILE#Tap}{LAYOUT_PC#Press}{LAYOUT_PS#Press}", "Press")
            .replace("{NICKNAME}", "Traveler").replace("\\n", "\n")
    }

}