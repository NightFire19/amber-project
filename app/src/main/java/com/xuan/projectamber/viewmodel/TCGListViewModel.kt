package com.xuan.projectamber.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.xuan.projectamber.domain.models.TCGSummaryEntity
import com.xuan.projectamber.domain.repository.RepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TCGListViewModel @Inject constructor(
    private val repository: RepositoryImpl
) : ViewModel() {

    var tcgList: List<TCGSummaryEntity> by mutableStateOf(listOf())
        private set

    var query: String by mutableStateOf("")
        private set

    var needImagesQueued: Boolean by mutableStateOf(true)

    init {
        getTCGList()
    }

    private fun getTCGList() = viewModelScope.launch {
        repository.getTCGList(query).collectLatest { tcgs ->
            tcgList = tcgs
        }
    }

    fun onSearchEntryValueChange(newEntry: String) = viewModelScope.launch {
        query = newEntry
        getTCGList()
    }

    fun onImagesQueued() {
        needImagesQueued = false
    }
}