package com.xuan.projectamber.viewmodel

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.core.text.HtmlCompat
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.xuan.projectamber.domain.models.WeaponEntity
import com.xuan.projectamber.domain.models.WeaponStoryEntity
import com.xuan.projectamber.domain.repository.RepositoryImpl
import com.xuan.projectamber.util.Constants
import com.xuan.projectamber.util.Constants.MANUAL_WEAPON
import com.xuan.projectamber.util.Constants.WEAPON_CURVE
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.math.roundToInt

@HiltViewModel
class WeaponDetailsViewModel @Inject constructor(
    private val repository: RepositoryImpl
): ViewModel() {

    private var _weapon = MutableStateFlow(WeaponEntity())
    val weapon get() = _weapon.asStateFlow()

    private var _weaponStory = MutableStateFlow(WeaponStoryEntity())
    val weaponStory get() = _weapon.asStateFlow()

    private val _selected = MutableStateFlow(0)
    val selected get() = _selected

    private val _weaponLevel = MutableStateFlow("1")
    val weaponLevel get() = _weaponLevel
    
    private val _mainStatName = MutableStateFlow(0)
    val mainStatName get() = _mainStatName

    private val _mainStatValue = MutableStateFlow("main stat value")
    val mainStatValue get() = _mainStatValue

    private val _subStatName = MutableStateFlow(0)
    val subStatName get() = _subStatName

    private val _subStatValue = MutableStateFlow("sub value")
    val subStatValue get() = _subStatValue

    private val _refinementLevel = MutableStateFlow("1")
    val refinementLevel get() = _refinementLevel

    private val _refinementTitle = MutableStateFlow("null")
    val refinementTitle get() = _refinementTitle

    private val _refinementDescription = MutableStateFlow("refinement description")
    val refinementDescription get() = _refinementDescription

    private val _maxLevelIndex = MutableStateFlow(Constants.ASCENSION_LEVELS.size)
    val maxLevelIndex get() = _maxLevelIndex

    private val _upperLevelBoundValue = MutableStateFlow("90")
    val upperLevelBoundValue get() = _upperLevelBoundValue.asStateFlow()

    private val _lowerLevelBoundValue = MutableStateFlow("1")
    val lowerLevelBoundValue get() = _lowerLevelBoundValue.asStateFlow()

    private val _promoteMap = MutableStateFlow(mutableMapOf<String, Int>())
    val promoteMap get() = _promoteMap

    private val _coinCost = MutableStateFlow(0)
    val coinCost get() = _coinCost.asStateFlow()

    var sliderSize: Float by mutableStateOf(9f)
    private set

    private val _weaponStoryBody = MutableStateFlow("story")
    val weaponStoryBody get() = _weaponStoryBody

    val refinementVisibility = mutableStateOf(false)
    val lowerLevelBoundVisibility = mutableStateOf(false)
    val upperLevelBoundVisibility = mutableStateOf(false)

    private var mainStatCurve = listOf<Float>()
    private var subStatCurve = listOf<Float>()
    private var refinementDescriptionMap = mapOf<String, String>()
    private var lowerLevelIndex = 0
    private var upperLevelIndex = 7

    fun getWeapon(id: String) = viewModelScope.launch {
        repository.getWeapon(id).collectLatest { weapon ->
            _weapon.value = weapon

            sliderSize = (weapon.upgrade?.promote?.last()?.unlockMaxLevel?.div(10))?.toFloat() ?: 9f

            _mainStatName.value = MANUAL_WEAPON[weapon.upgrade!!.prop?.get(0)?.propType]!!
            _mainStatValue.value = weapon.upgrade.prop?.get(0)?.initValue?.roundToInt().toString()

            _subStatName.value = MANUAL_WEAPON[weapon.upgrade.prop?.get(1)?.propType]!!
            var subStat = weapon.upgrade.prop?.get(1)?.initValue

            if (subStat != null) {
                if (subStat < 1) {
                    subStat *= 100
                    _subStatValue.value = "%.${1}f".format(subStat)+"%"
                } else {
                    _subStatValue.value = subStat.roundToInt().toString()
                }
            }

            mainStatCurve = WEAPON_CURVE[weapon.upgrade.prop?.get(0)?.type]!!
            subStatCurve = WEAPON_CURVE[weapon.upgrade.prop?.get(1)?.type]!!

            weapon.affix?.forEach {
                _refinementTitle.value = it.value.name.toString()
                refinementDescriptionMap = it.value.upgrade!!
            }
            _refinementDescription.value = refinementDescriptionMap["0"].toString()
            getMaterials()
        }
    }

    fun getWeaponStory(id: String) = viewModelScope.launch {
        repository.getWeaponStory(id).collectLatest{
            _weaponStoryBody.value = it.story
        }
    }

    fun onValueChanged (selected : Int) = viewModelScope.launch{
        _selected.value = selected
        if (selected == 0) {
            _weaponLevel.value = "1"
        } else {
            _weaponLevel.value = (selected*10).toString()
        }
        _mainStatValue.value = (_weapon.value.upgrade?.prop?.get(0)?.initValue?.times(mainStatCurve[_weaponLevel.value.toInt() -1]))
            .toString()
        _subStatValue.value = (_weapon.value.upgrade?.prop?.get(1)?.initValue?.times(subStatCurve[_weaponLevel.value.toInt() -1])).toString()
        if (_subStatValue.value.toFloat() < 1f) {
            _subStatValue.value = "%.${1}f".format(_subStatValue.value.toFloat()*100f)+"%"
        } else {
            _subStatValue.value = _subStatValue.value.toFloat().roundToInt().toString()
        }

        when (weaponLevel.value) {
            "30","40" -> {
                _mainStatValue.value = (_mainStatValue.value.toFloat() + _weapon.value.upgrade?.promote?.get(1)?.addProps?.get(
                    _weapon.value.upgrade?.prop?.get(0)?.propType)!!).toString()
            }
            "50" -> {
                _mainStatValue.value = (_mainStatValue.value.toFloat() + _weapon.value.upgrade?.promote?.get(2)?.addProps?.get(
                    _weapon.value.upgrade?.prop?.get(0)?.propType)!!).toString()
            }
            "60" -> {
                _mainStatValue.value = (_mainStatValue.value.toFloat() + _weapon.value.upgrade?.promote?.get(3)?.addProps?.get(
                    _weapon.value.upgrade?.prop?.get(0)?.propType)!!).toString()
            }
            "70" -> {
                _mainStatValue.value = (_mainStatValue.value.toFloat() + _weapon.value.upgrade?.promote?.get(4)?.addProps?.get(
                    _weapon.value.upgrade?.prop?.get(0)?.propType)!!).toString()
            }
            "80" -> {
                _mainStatValue.value = (_mainStatValue.value.toFloat() + _weapon.value.upgrade?.promote?.get(5)?.addProps?.get(
                    _weapon.value.upgrade?.prop?.get(0)?.propType)!!).toString()
            }
            "90" -> {
                _mainStatValue.value = (_mainStatValue.value.toFloat() + _weapon.value.upgrade?.promote?.get(6)?.addProps?.get(
                    _weapon.value.upgrade?.prop?.get(0)?.propType)!!).toString()
            }
        }
        _mainStatValue.value = _mainStatValue.value.toFloat().roundToInt().toString()
    }

    fun onRefinementLevelClick(level: Int) {
        refinementVisibility.value = false
        _refinementLevel.value = level.toString()
        _refinementDescription.value = refinementDescriptionMap[(level - 1).toString()].toString()
    }

    fun onUpperLevelBoundChange(index: Int) {
        upperLevelBoundVisibility.value = false
        _maxLevelIndex.value = index
        upperLevelIndex = index
        _upperLevelBoundValue.value = Constants.ASCENSION_LEVELS[index]
        if (_upperLevelBoundValue.value.toInt() <= _lowerLevelBoundValue.value.toInt()) {
            _lowerLevelBoundValue.value = Constants.ASCENSION_LEVELS[index-1]
        }
        getMaterials()
    }
    fun onLowerLevelBoundChange(index: Int) {
        lowerLevelBoundVisibility.value = false
        lowerLevelIndex = index
        _lowerLevelBoundValue.value = Constants.ASCENSION_LEVELS[index]
        getMaterials()
    }

    private fun getMaterials() {
        _promoteMap.value = mutableMapOf()
        _coinCost.value = 0
        _weapon.value.upgrade?.promote?.forEachIndexed { index, promote ->
            if (index in lowerLevelIndex until upperLevelIndex) {
                if (promote.coinCost != null) {
                    _coinCost.value = _coinCost.value + promote.coinCost
                }
                _promoteMap.value.putAll(mapOf(Pair("202",_coinCost.value)))
                if (promote.costItems != null) {
                    _promoteMap.value = _promoteMap.value
                        .apply {
                            promote.costItems.forEach { (k, v) -> merge(k, v) { oldVal, newVal -> oldVal + newVal } }
                        }
                }
            }
        }
    }

    fun toHtmlText(input: String): String {
        val regex = "<color=#(\\p{XDigit}{6})\\p{XDigit}*?>".toRegex()
        val closingTagRegex = Regex("</color>")
        val closingTagReplacement = "</font></b>"

        val output = regex.replace(input) {
            val colorHex = "3299CC" //TODO:
            "<b><font color=\"#$colorHex\">"
        }
        Log.v("test2", output)
        return output.replace(closingTagRegex, closingTagReplacement).replace("\\n", "\n")
            .replace("{LAYOUT_MOBILE#Tap}{LAYOUT_PC#Press}{LAYOUT_PS#Press}", "Press")
            .replace("{NICKNAME}", "Traveler")
    }
}