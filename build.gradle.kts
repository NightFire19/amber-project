buildscript {
    dependencies {
        classpath("com.android.tools.build:gradle:8.0.0")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.8.10")
        classpath("com.google.gms:google-services:4.3.15")
        classpath ("com.google.firebase:firebase-crashlytics-gradle:2.9.4")

    }
    repositories {
        google()
        mavenCentral()
    }
}
plugins {
    id("com.android.application").version("7.2.1").apply(false)
    id("com.android.library").version("7.2.1").apply(false)
    id("com.google.dagger.hilt.android").version("2.45").apply(false)
}
tasks.register(name = "type", type = Delete::class){
    delete(rootProject.buildDir)
}